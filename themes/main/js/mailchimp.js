function AddSubcriber(email, name, link, elem, elemform){

    $.ajax({
        type: 'GET',
        dataType: 'json',
        url: link + 'MailchimpAddSubscriber',
        data: {email: email, name: name},
        success: function (data) {
            if (data['status'] != 400){
                $(elem + ' .mc-message').empty();
                $(elem + ' .mc-message').removeClass('mc-error');
                $(elemform).hide();
                switch(data['status']) {
                    case 'pending':
                        $(elem + ' .mc-message').append("Bedankt voor je aanmelding. Je ontvangt nu een e-mail met een link om je aanmelding te bevestigen.");
                        break;
                    case 'subscribed':
                        $(elem + ' .mc-message').append("Bedankt voor je aanmelding. Je bent nu geabonneerd op onze nieuwsbrief.");
                        break;
                    default:
                        $(elem + ' .mc-message').append("Bedankt voor je aanmelding.");
                }
            }else{
                $(elem + ' .mc-message').empty();
                $(elem + ' .mc-message').addClass('mc-error');
                switch(data['title']) {
                    case 'Invalid Resource':
                        $(elem + ' .mc-message').prepend("Controleer nog even je e-mailadres en naam.");
                        break;
                    case 'Member Exists':
                        $(elem + ' .mc-message').prepend("Dit e-mailadres is al eerder aangemeld.");
                        break;
                    default:
                        $(elem + ' .mc-message').prepend("Sorry, er is een onbekende fout opgetreden.");
                }
            }
        },
        error: function (data) {
            $('#results-mailchimpform').prepend("Sorry, er is een onbekende fout opgetreden.");

        }
    })

};

// possibly place this in an existing "document.ready"
$(document).ready(function() {

    $('#Form_MailchimpAddForm_action_SubmitMailchimpAddForm').click(function() {

        var email = $('#Form_MailchimpAddForm_Email').val();
        var name = $('#Form_MailchimpAddForm_Name').val();
        var link = $('#Form_MailchimpAddForm_Link').val();
        var elem = '#mc-subscribe';
        var elemform = '#mc-form';

        AddSubcriber(email, name, link, elem, elemform);
        return false;
    });

    $('#Form_ElementMailchimpAddForm_action_SubmitMailchimpAddForm').click(function() {

        var email = $('#Form_ElementMailchimpAddForm_Email').val();
        var name = $('#Form_ElementMailchimpAddForm_Name').val();
        var link = $('#Form_ElementMailchimpAddForm_Link').val();
        var elem = '#element-mc-subscribe';
        var elemform = '#element-mc-form';

        AddSubcriber(email, name, link, elem, elemform);
        return false;
    });

    $('#Form_ExtraMailchimpAddForm_action_SubmitMailchimpAddForm').click(function() {

        var email = $('#Form_ExtraMailchimpAddForm_Email').val();
        var name = $('#Form_ExtraMailchimpAddForm_Name').val();
        var link = $('#Form_ExtraMailchimpAddForm_Link').val();
        var elem = '#extra-mc-subscribe';
        var elemform = '#extra-mc-form';

        AddSubcriber(email, name, link, elem, elemform);
        return false;
    });

});
