<!doctype html>
<html class="no-js" lang="$ContentLocale">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    $MetaTags
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <%-- Facebook metatags used for sharing content, fill in the FB app_id see the Hestec OneNote wiki --%>
    <% if $SiteConfig.FacebookAppId %>
        <meta property="fb:app_id" content="$SiteConfig.FacebookAppId" />
    <% end_if %>
    <meta property="og:url" content="<% if $newAbsoluteLink %>$newAbsoluteLink<% else %>$AbsoluteLink<% end_if %>" />
    <meta property="og:title" content="<% if $newMetaTitle %>$newMetaTitle<% else %>$MetaTitle<% end_if %>" />
    <%--<meta property="og:description"           content="$Content.LimitSentences" />--%>
    <% if $ImpressionImage %>
        <meta property="og:image" content="$ImpressionImage.AbsoluteURL" />
    <% end_if %>
    <%-- set the CANONICAL_BASE_URL in _ss_environment.php --%>
    <% if $CanonicalUrl %>
        <link rel="canonical" href="$CanonicalUrl" />
    <% end_if %>
    <link rel="icon" type="image/png" href="$BaseHref\favicon.png" />
    <link rel="icon" sizes="192x192" href="$BaseHref\android-icon.png">
    <%-- stop the Skype Click To Call plugin, also a jQuery script in main.js --%>
    <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />

    <script src="/themes/main/js/tp/modernizr-3.7.1.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="/themes/main/js/tp/jquery-3.4.1.min.js"><\\/script>')</script>
    <% include SchemaData %>
    <%-- A jQuery plugin for browser detection. jQuery v1.9.1 dropped support for browser detection -> https://github.com/gabceb/jquery-browser-plugin --%>
    <%--script src="/themes/main/js/tp/jquery.browser.min.js"></script--%>

</head>
<body lang="$ContentLocale">

<!--[if IE]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
<![endif]-->

$Layout

<% if not $isDev %>
    <% if $SiteConfig.GATrackingCode && $SiteConfig.isNotOwnIp %>
    <!--Google Analytics-->
    <script async src="https://www.googletagmanager.com/gtag/js?id=$SiteConfig.GATrackingCode"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', '$SiteConfig.GATrackingCode', { 'anonymize_ip': true });
    </script>
    <script>
        jQuery( document ).ready(function($) {
            setTimeout("gtag('event', 'read', {'event_category': '30 seconds on page', 'event_label': '$URLSegment'});", 30000);
        });
    </script>
    <!--End of Google Analytics-->
    <% end_if %>
    <% if $SiteConfig.TawkSiteId %>
    <!--Start of Tawk.to Script-->
    <script type="text/javascript">
        var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
        (function(){
            var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
            s1.async=true;
            s1.src='https://embed.tawk.to/$SiteConfig.TawkSiteId/default';
            s1.charset='UTF-8';
            s1.setAttribute('crossorigin','*');
            s0.parentNode.insertBefore(s1,s0);
        })();
    </script>
    <!--End of Tawk.to Script-->
    <% end_if %>
<% end_if %>

    $BetterNavigator

</body>
</html>
