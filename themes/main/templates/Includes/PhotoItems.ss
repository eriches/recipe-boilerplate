       <% if PaginatedPhotos %>
          <ul class="row album-photos">
          <% loop PaginatedPhotos %>
            <li class="col-lg-2 col-md-2 col-sm-3 col-xs-4">
              <a class="colorbox" href="$PhotoSized(800,800).URL" title="$Caption"><img class="img-responsive" src="$PhotoCropped(125,125).URL" alt="$Caption" /></a>
            </li>
          <% end_loop %>
          </ul>
          
          <% if PaginatedPhotos.MoreThanOnePage %>
             <% if PaginatedPhotos.NotFirstPage %>
                 <a class="prev" href="$PaginatedPhotos.PrevLink">Prev</a>
             <% end_if %>
             <% loop PaginatedPhotos.Pages %>
                 <% if CurrentBool %>
                     $PageNum
                 <% else %>
                     <% if Link %>
                         <a href="$Link">$PageNum</a>
                     <% else %>
                         ...
                     <% end_if %>
                 <% end_if %>
                 <% end_loop %>
             <% if PaginatedPhotos.NotLastPage %>
                 <a class="next" href="$PaginatedPhotos.NextLink">Next</a>
             <% end_if %>
         <% end_if %>
          
         <% else %>
          <p>There are no photos in this album.</p> 
        <% end_if %>
