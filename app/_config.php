<?php

use SilverStripe\Control\Email\Email;
use SilverStripe\Core\Config\Config;
use SilverStripe\Control\Director;
use SilverStripe\Forms\HTMLEditor\TinyMCEConfig;

\SilverStripe\i18n\i18n::set_locale('nl_NL');

if(Director::isDev()) {
    Config::modify()->set(Email::class, 'send_all_emails_to', "dev-all-emails@hestec.xyz");
    Config::modify()->set(Email::class, 'send_all_emails_from', "dev-sender@hestec.xyz");
} else {
    // redirect to https when live
    // if rebuild by command line, don't force for localhost
    if( !in_array( $_SERVER['REMOTE_ADDR'], array( '127.0.0.1', '::1' ) ) ) {
        Director::forceSSL();
    }
}
// always add www before the domain
// if rebuild by command line, don't force for localhost
if( !in_array( $_SERVER['REMOTE_ADDR'], array( '127.0.0.1', '::1' ) ) ) {
    Director::forceWWW();
}

$formats = [
    [
        'title' => 'List styles', 'items' => [
        [
            'title' => 'arrow-right',
            'selector' => 'ul',
            'classes' => 'list-arrow-right',
            'wrapper' => true,
            'merge_siblings' => false,
        ],
        [
            'title' => 'check',
            'selector' => 'ul',
            'classes' => 'list-check',
            'wrapper' => true,
            'merge_siblings' => false,
        ],
        [
            'title' => 'hand',
            'selector' => 'ul',
            'classes' => 'list-hand',
            'wrapper' => true,
            'merge_siblings' => false,
        ],
        [
            'title' => 'dot',
            'selector' => 'ul',
            'classes' => 'list-dot',
            'wrapper' => true,
            'merge_siblings' => false,
        ],
        [
            'title' => 'dot-sm',
            'selector' => 'ul',
            'classes' => 'list-dot-sm',
            'wrapper' => true,
            'merge_siblings' => false,
        ],
        [
            'title' => 'angle-right',
            'selector' => 'ul',
            'classes' => 'list-angle-right',
            'wrapper' => true,
            'merge_siblings' => false,
        ],
        [
            'title' => 'thumbs-up',
            'selector' => 'ul',
            'classes' => 'list-thumbs-up',
            'wrapper' => true,
            'merge_siblings' => false,
        ],
        [
            'title' => 'plus-cirlce',
            'selector' => 'ul',
            'classes' => 'list-plus-cirlce',
            'wrapper' => true,
            'merge_siblings' => false,
        ],
        [
            'title' => 'number-1',
            'selector' => 'ol',
            'classes' => 'list-number-1',
            'wrapper' => true,
            'merge_siblings' => false,
        ]
    ]
    ]
];

TinyMCEConfig::get('cms')
    ->addButtonsToLine(1, 'styleselect')
    ->setOptions([
        'importcss_append' => true,
        'style_formats' => $formats,
        'style_formats_merge' => true
    ]);