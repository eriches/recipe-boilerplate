-- --------------------------------------------------------
-- Host:                         www.ss-boilerplate.hst1.nl
-- Server versie:                10.1.22-MariaDB - Source distribution
-- Server OS:                    Linux
-- HeidiSQL Versie:              9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Structuur van  tabel vagrant.Blog wordt geschreven
CREATE TABLE IF NOT EXISTS `Blog` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PostsPerPage` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.Blog: ~0 rows (ongeveer)
DELETE FROM `Blog`;
/*!40000 ALTER TABLE `Blog` DISABLE KEYS */;
/*!40000 ALTER TABLE `Blog` ENABLE KEYS */;


-- Structuur van  tabel vagrant.BlogCategory wordt geschreven
CREATE TABLE IF NOT EXISTS `BlogCategory` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('SilverStripe\\Blog\\Model\\BlogCategory') CHARACTER SET utf8 DEFAULT 'SilverStripe\\Blog\\Model\\BlogCategory',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `URLSegment` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `BlogID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ClassName` (`ClassName`),
  KEY `BlogID` (`BlogID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.BlogCategory: ~0 rows (ongeveer)
DELETE FROM `BlogCategory`;
/*!40000 ALTER TABLE `BlogCategory` DISABLE KEYS */;
/*!40000 ALTER TABLE `BlogCategory` ENABLE KEYS */;


-- Structuur van  tabel vagrant.BlogPost wordt geschreven
CREATE TABLE IF NOT EXISTS `BlogPost` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PublishDate` datetime DEFAULT NULL,
  `AuthorNames` varchar(1024) CHARACTER SET utf8 DEFAULT NULL,
  `Summary` mediumtext CHARACTER SET utf8,
  `FeaturedImageID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `PublishDate` (`PublishDate`),
  KEY `FeaturedImageID` (`FeaturedImageID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.BlogPost: ~0 rows (ongeveer)
DELETE FROM `BlogPost`;
/*!40000 ALTER TABLE `BlogPost` DISABLE KEYS */;
/*!40000 ALTER TABLE `BlogPost` ENABLE KEYS */;


-- Structuur van  tabel vagrant.BlogPost_Authors wordt geschreven
CREATE TABLE IF NOT EXISTS `BlogPost_Authors` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `BlogPostID` int(11) NOT NULL DEFAULT '0',
  `MemberID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `BlogPostID` (`BlogPostID`),
  KEY `MemberID` (`MemberID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.BlogPost_Authors: ~0 rows (ongeveer)
DELETE FROM `BlogPost_Authors`;
/*!40000 ALTER TABLE `BlogPost_Authors` DISABLE KEYS */;
/*!40000 ALTER TABLE `BlogPost_Authors` ENABLE KEYS */;


-- Structuur van  tabel vagrant.BlogPost_Categories wordt geschreven
CREATE TABLE IF NOT EXISTS `BlogPost_Categories` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `BlogPostID` int(11) NOT NULL DEFAULT '0',
  `BlogCategoryID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `BlogPostID` (`BlogPostID`),
  KEY `BlogCategoryID` (`BlogCategoryID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.BlogPost_Categories: ~0 rows (ongeveer)
DELETE FROM `BlogPost_Categories`;
/*!40000 ALTER TABLE `BlogPost_Categories` DISABLE KEYS */;
/*!40000 ALTER TABLE `BlogPost_Categories` ENABLE KEYS */;


-- Structuur van  tabel vagrant.BlogPost_Live wordt geschreven
CREATE TABLE IF NOT EXISTS `BlogPost_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PublishDate` datetime DEFAULT NULL,
  `AuthorNames` varchar(1024) CHARACTER SET utf8 DEFAULT NULL,
  `Summary` mediumtext CHARACTER SET utf8,
  `FeaturedImageID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `PublishDate` (`PublishDate`),
  KEY `FeaturedImageID` (`FeaturedImageID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.BlogPost_Live: ~0 rows (ongeveer)
DELETE FROM `BlogPost_Live`;
/*!40000 ALTER TABLE `BlogPost_Live` DISABLE KEYS */;
/*!40000 ALTER TABLE `BlogPost_Live` ENABLE KEYS */;


-- Structuur van  tabel vagrant.BlogPost_Tags wordt geschreven
CREATE TABLE IF NOT EXISTS `BlogPost_Tags` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `BlogPostID` int(11) NOT NULL DEFAULT '0',
  `BlogTagID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `BlogPostID` (`BlogPostID`),
  KEY `BlogTagID` (`BlogTagID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.BlogPost_Tags: ~0 rows (ongeveer)
DELETE FROM `BlogPost_Tags`;
/*!40000 ALTER TABLE `BlogPost_Tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `BlogPost_Tags` ENABLE KEYS */;


-- Structuur van  tabel vagrant.BlogPost_Versions wordt geschreven
CREATE TABLE IF NOT EXISTS `BlogPost_Versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `PublishDate` datetime DEFAULT NULL,
  `AuthorNames` varchar(1024) CHARACTER SET utf8 DEFAULT NULL,
  `Summary` mediumtext CHARACTER SET utf8,
  `FeaturedImageID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`),
  KEY `PublishDate` (`PublishDate`),
  KEY `FeaturedImageID` (`FeaturedImageID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.BlogPost_Versions: ~0 rows (ongeveer)
DELETE FROM `BlogPost_Versions`;
/*!40000 ALTER TABLE `BlogPost_Versions` DISABLE KEYS */;
/*!40000 ALTER TABLE `BlogPost_Versions` ENABLE KEYS */;


-- Structuur van  tabel vagrant.BlogTag wordt geschreven
CREATE TABLE IF NOT EXISTS `BlogTag` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('SilverStripe\\Blog\\Model\\BlogTag') CHARACTER SET utf8 DEFAULT 'SilverStripe\\Blog\\Model\\BlogTag',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `URLSegment` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `BlogID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ClassName` (`ClassName`),
  KEY `BlogID` (`BlogID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.BlogTag: ~0 rows (ongeveer)
DELETE FROM `BlogTag`;
/*!40000 ALTER TABLE `BlogTag` DISABLE KEYS */;
/*!40000 ALTER TABLE `BlogTag` ENABLE KEYS */;


-- Structuur van  tabel vagrant.Blog_Contributors wordt geschreven
CREATE TABLE IF NOT EXISTS `Blog_Contributors` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `BlogID` int(11) NOT NULL DEFAULT '0',
  `MemberID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `BlogID` (`BlogID`),
  KEY `MemberID` (`MemberID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.Blog_Contributors: ~0 rows (ongeveer)
DELETE FROM `Blog_Contributors`;
/*!40000 ALTER TABLE `Blog_Contributors` DISABLE KEYS */;
/*!40000 ALTER TABLE `Blog_Contributors` ENABLE KEYS */;


-- Structuur van  tabel vagrant.Blog_Editors wordt geschreven
CREATE TABLE IF NOT EXISTS `Blog_Editors` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `BlogID` int(11) NOT NULL DEFAULT '0',
  `MemberID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `BlogID` (`BlogID`),
  KEY `MemberID` (`MemberID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.Blog_Editors: ~0 rows (ongeveer)
DELETE FROM `Blog_Editors`;
/*!40000 ALTER TABLE `Blog_Editors` DISABLE KEYS */;
/*!40000 ALTER TABLE `Blog_Editors` ENABLE KEYS */;


-- Structuur van  tabel vagrant.Blog_Live wordt geschreven
CREATE TABLE IF NOT EXISTS `Blog_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PostsPerPage` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.Blog_Live: ~0 rows (ongeveer)
DELETE FROM `Blog_Live`;
/*!40000 ALTER TABLE `Blog_Live` DISABLE KEYS */;
/*!40000 ALTER TABLE `Blog_Live` ENABLE KEYS */;


-- Structuur van  tabel vagrant.Blog_Versions wordt geschreven
CREATE TABLE IF NOT EXISTS `Blog_Versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `PostsPerPage` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.Blog_Versions: ~2 rows (ongeveer)
DELETE FROM `Blog_Versions`;
/*!40000 ALTER TABLE `Blog_Versions` DISABLE KEYS */;
INSERT INTO `Blog_Versions` (`ID`, `RecordID`, `Version`, `PostsPerPage`) VALUES
	(1, 11, 1, 10),
	(2, 11, 2, 10);
/*!40000 ALTER TABLE `Blog_Versions` ENABLE KEYS */;


-- Structuur van  tabel vagrant.Blog_Writers wordt geschreven
CREATE TABLE IF NOT EXISTS `Blog_Writers` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `BlogID` int(11) NOT NULL DEFAULT '0',
  `MemberID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `BlogID` (`BlogID`),
  KEY `MemberID` (`MemberID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.Blog_Writers: ~0 rows (ongeveer)
DELETE FROM `Blog_Writers`;
/*!40000 ALTER TABLE `Blog_Writers` DISABLE KEYS */;
/*!40000 ALTER TABLE `Blog_Writers` ENABLE KEYS */;


-- Structuur van  tabel vagrant.BrandColor wordt geschreven
CREATE TABLE IF NOT EXISTS `BrandColor` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('BrandColor') CHARACTER SET utf8 DEFAULT 'BrandColor',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `NameInLesscss` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `NameInCMS` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `ColorHex` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `ColorRgb` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `SiteConfigID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ClassName` (`ClassName`),
  KEY `SiteConfigID` (`SiteConfigID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.BrandColor: ~0 rows (ongeveer)
DELETE FROM `BrandColor`;
/*!40000 ALTER TABLE `BrandColor` DISABLE KEYS */;
/*!40000 ALTER TABLE `BrandColor` ENABLE KEYS */;


-- Structuur van  tabel vagrant.BrandColorType wordt geschreven
CREATE TABLE IF NOT EXISTS `BrandColorType` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('BrandColorType') CHARACTER SET utf8 DEFAULT 'BrandColorType',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `ColorType` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `Description` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `SiteConfigID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ClassName` (`ClassName`),
  KEY `SiteConfigID` (`SiteConfigID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.BrandColorType: ~0 rows (ongeveer)
DELETE FROM `BrandColorType`;
/*!40000 ALTER TABLE `BrandColorType` DISABLE KEYS */;
/*!40000 ALTER TABLE `BrandColorType` ENABLE KEYS */;


-- Structuur van  tabel vagrant.BrandColor_BrandColorTypes wordt geschreven
CREATE TABLE IF NOT EXISTS `BrandColor_BrandColorTypes` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `BrandColorID` int(11) NOT NULL DEFAULT '0',
  `BrandColorTypeID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `BrandColorID` (`BrandColorID`),
  KEY `BrandColorTypeID` (`BrandColorTypeID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.BrandColor_BrandColorTypes: ~0 rows (ongeveer)
DELETE FROM `BrandColor_BrandColorTypes`;
/*!40000 ALTER TABLE `BrandColor_BrandColorTypes` DISABLE KEYS */;
/*!40000 ALTER TABLE `BrandColor_BrandColorTypes` ENABLE KEYS */;


-- Structuur van  tabel vagrant.ChangeSet wordt geschreven
CREATE TABLE IF NOT EXISTS `ChangeSet` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('SilverStripe\\Versioned\\ChangeSet') CHARACTER SET utf8 DEFAULT 'SilverStripe\\Versioned\\ChangeSet',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `State` enum('open','published','reverted') CHARACTER SET utf8 DEFAULT 'open',
  `IsInferred` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Description` mediumtext CHARACTER SET utf8,
  `PublishDate` datetime DEFAULT NULL,
  `LastSynced` datetime DEFAULT NULL,
  `OwnerID` int(11) NOT NULL DEFAULT '0',
  `PublisherID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `State` (`State`),
  KEY `ID` (`ID`),
  KEY `ClassName` (`ClassName`),
  KEY `OwnerID` (`OwnerID`),
  KEY `PublisherID` (`PublisherID`)
) ENGINE=InnoDB AUTO_INCREMENT=88 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.ChangeSet: ~74 rows (ongeveer)
DELETE FROM `ChangeSet`;
/*!40000 ALTER TABLE `ChangeSet` DISABLE KEYS */;
INSERT INTO `ChangeSet` (`ID`, `ClassName`, `LastEdited`, `Created`, `Name`, `State`, `IsInferred`, `Description`, `PublishDate`, `LastSynced`, `OwnerID`, `PublisherID`) VALUES
	(1, 'SilverStripe\\Versioned\\ChangeSet', '2018-03-26 20:15:00', '2018-03-26 20:15:00', 'Generated by publish of \'#1\' at Mar 26, 2018 8:15:00 PM', 'published', 1, NULL, '2018-03-26 20:15:00', '2018-03-26 20:15:00', 0, 1),
	(2, 'SilverStripe\\Versioned\\ChangeSet', '2018-03-26 22:28:55', '2018-03-26 22:28:55', 'Generated by publish of \'Home\' at Mar 26, 2018 10:28:55 PM', 'published', 1, NULL, '2018-03-26 22:28:55', '2018-03-26 22:28:55', 0, 1),
	(3, 'SilverStripe\\Versioned\\ChangeSet', '2018-03-26 22:34:42', '2018-03-26 22:34:42', 'Generated by publish of \'test\' at Mar 26, 2018 10:34:42 PM', 'published', 1, NULL, '2018-03-26 22:34:42', '2018-03-26 22:34:42', 0, 1),
	(4, 'SilverStripe\\Versioned\\ChangeSet', '2018-03-29 20:45:36', '2018-03-29 20:45:36', 'Generated by publish of \'Elements\' at Mar 29, 2018 8:45:36 PM', 'published', 1, NULL, '2018-03-29 20:45:36', '2018-03-29 20:45:36', 0, 1),
	(5, 'SilverStripe\\Versioned\\ChangeSet', '2018-03-29 20:46:21', '2018-03-29 20:46:21', 'Generated by publish of \'Dit is een faketekst\' at Mar 29, 2018 8:46:21 PM', 'published', 1, NULL, '2018-03-29 20:46:21', '2018-03-29 20:46:21', 0, 1),
	(6, 'SilverStripe\\Versioned\\ChangeSet', '2018-03-29 20:55:29', '2018-03-29 20:55:29', 'Generated by publish of \'Video\' at Mar 29, 2018 8:55:29 PM', 'published', 1, NULL, '2018-03-29 20:55:29', '2018-03-29 20:55:29', 0, 1),
	(7, 'SilverStripe\\Versioned\\ChangeSet', '2018-03-29 20:58:08', '2018-03-29 20:58:08', 'Generated by publish of \'Video\' at Mar 29, 2018 8:58:08 PM', 'published', 1, NULL, '2018-03-29 20:58:08', '2018-03-29 20:58:08', 0, 1),
	(8, 'SilverStripe\\Versioned\\ChangeSet', '2018-03-29 20:58:49', '2018-03-29 20:58:49', 'Generated by publish of \'Video\' at Mar 29, 2018 8:58:49 PM', 'published', 1, NULL, '2018-03-29 20:58:49', '2018-03-29 20:58:49', 0, 1),
	(9, 'SilverStripe\\Versioned\\ChangeSet', '2018-03-29 21:22:29', '2018-03-29 21:22:29', 'Generated by publish of \'Elements\' at Mar 29, 2018 9:22:29 PM', 'published', 1, NULL, '2018-03-29 21:22:29', '2018-03-29 21:22:29', 0, 1),
	(10, 'SilverStripe\\Versioned\\ChangeSet', '2018-03-29 21:23:08', '2018-03-29 21:23:08', 'Generated by publish of \'Video\' at Mar 29, 2018 9:23:08 PM', 'published', 1, NULL, '2018-03-29 21:23:08', '2018-03-29 21:23:08', 0, 1),
	(11, 'SilverStripe\\Versioned\\ChangeSet', '2018-03-29 22:13:17', '2018-03-29 22:13:17', 'Generated by publish of \'Default Admin\' at Mar 29, 2018 10:13:17 PM', 'published', 1, NULL, '2018-03-29 22:13:17', '2018-03-29 22:13:17', 0, 1),
	(12, 'SilverStripe\\Versioned\\ChangeSet', '2018-03-29 22:15:25', '2018-03-29 22:15:25', 'Generated by publish of \'Video\' at 29 mrt. 2018 22:15:25', 'published', 1, NULL, '2018-03-29 22:15:25', '2018-03-29 22:15:25', 0, 1),
	(13, 'SilverStripe\\Versioned\\ChangeSet', '2018-03-31 14:13:54', '2018-03-31 14:13:53', 'Generated by publish of \'Elements\' at 31 mrt. 2018 14:13:53', 'published', 1, NULL, '2018-03-31 14:13:54', '2018-03-31 14:13:53', 0, 1),
	(14, 'SilverStripe\\Versioned\\ChangeSet', '2018-03-31 14:15:54', '2018-03-31 14:15:54', 'Generated by publish of \'Video\' at 31 mrt. 2018 14:15:54', 'published', 1, NULL, '2018-03-31 14:15:54', '2018-03-31 14:15:54', 0, 1),
	(15, 'SilverStripe\\Versioned\\ChangeSet', '2018-03-31 15:13:38', '2018-03-31 15:13:38', 'Generated by publish of \'Panel\' at 31 mrt. 2018 15:13:38', 'published', 1, NULL, '2018-03-31 15:13:38', '2018-03-31 15:13:38', 0, 1),
	(16, 'SilverStripe\\Versioned\\ChangeSet', '2018-03-31 16:16:05', '2018-03-31 16:16:05', 'Generated by publish of \'Elements\' at 31 mrt. 2018 16:16:05', 'published', 1, NULL, '2018-03-31 16:16:05', '2018-03-31 16:16:05', 0, 1),
	(17, 'SilverStripe\\Versioned\\ChangeSet', '2018-03-31 16:16:17', '2018-03-31 16:16:16', 'Generated by publish of \'Elements\' at 31 mrt. 2018 16:16:16', 'published', 1, NULL, '2018-03-31 16:16:17', '2018-03-31 16:16:17', 0, 1),
	(18, 'SilverStripe\\Versioned\\ChangeSet', '2018-04-04 10:48:57', '2018-04-04 10:48:57', 'Generated by publish of \'Evenementen\' at 4 apr. 2018 10:48:57', 'published', 1, NULL, '2018-04-04 10:48:57', '2018-04-04 10:48:57', 0, 1),
	(19, 'SilverStripe\\Versioned\\ChangeSet', '2018-04-04 11:25:08', '2018-04-04 11:25:08', 'Generated by publish of \'Test\' at 4 apr. 2018 11:25:08', 'published', 1, NULL, '2018-04-04 11:25:08', '2018-04-04 11:25:08', 0, 1),
	(20, 'SilverStripe\\Versioned\\ChangeSet', '2018-04-05 10:16:58', '2018-04-05 10:16:58', 'Generated by publish of \'Dit is een test\' at 5 apr. 2018 10:16:58', 'published', 1, NULL, '2018-04-05 10:16:58', '2018-04-05 10:16:58', 0, 1),
	(21, 'SilverStripe\\Versioned\\ChangeSet', '2018-04-05 10:17:21', '2018-04-05 10:17:21', 'Generated by publish of \'Landing\' at 5 apr. 2018 10:17:21', 'published', 1, NULL, '2018-04-05 10:17:21', '2018-04-05 10:17:21', 0, 1),
	(22, 'SilverStripe\\Versioned\\ChangeSet', '2018-04-05 10:23:29', '2018-04-05 10:23:29', 'Generated by publish of \'Homepage\' at 5 apr. 2018 10:23:29', 'published', 1, NULL, '2018-04-05 10:23:29', '2018-04-05 10:23:29', 0, 1),
	(23, 'SilverStripe\\Versioned\\ChangeSet', '2018-04-05 10:23:35', '2018-04-05 10:23:35', 'Generated by publish of \'Homepage\' at 5 apr. 2018 10:23:35', 'published', 1, NULL, '2018-04-05 10:23:35', '2018-04-05 10:23:35', 0, 1),
	(24, 'SilverStripe\\Versioned\\ChangeSet', '2018-04-05 10:53:24', '2018-04-05 10:53:24', 'Generated by publish of \'Enquiry\' at 5 apr. 2018 10:53:24', 'published', 1, NULL, '2018-04-05 10:53:24', '2018-04-05 10:53:24', 0, 1),
	(25, 'SilverStripe\\Versioned\\ChangeSet', '2018-04-05 11:04:26', '2018-04-05 11:04:26', 'Generated by publish of \'Blog\' at 5 apr. 2018 11:04:26', 'published', 1, NULL, '2018-04-05 11:04:26', '2018-04-05 11:04:26', 0, 1),
	(26, 'SilverStripe\\Versioned\\ChangeSet', '2018-07-04 17:18:02', '2018-07-04 17:18:02', 'Generated by publish of \'Your Site Name\' at 4 jul. 2018 17:18:02', 'published', 1, NULL, '2018-07-04 17:18:02', '2018-07-04 17:18:02', 0, 1),
	(27, 'SilverStripe\\Versioned\\ChangeSet', '2018-07-04 17:18:10', '2018-07-04 17:18:10', 'Generated by publish of \'Your Site Name\' at 4 jul. 2018 17:18:10', 'published', 1, NULL, '2018-07-04 17:18:10', '2018-07-04 17:18:10', 0, 1),
	(28, 'SilverStripe\\Versioned\\ChangeSet', '2018-07-06 12:23:59', '2018-07-06 12:23:59', 'Generated by publish of \'Frequently asked questions\' at 6 jul. 2018 12:23:59', 'published', 1, NULL, '2018-07-06 12:23:59', '2018-07-06 12:23:59', 0, 1),
	(29, 'SilverStripe\\Versioned\\ChangeSet', '2018-07-06 12:25:04', '2018-07-06 12:25:04', 'Generated by publish of \'Veelgestelde vragen\' at 6 jul. 2018 12:25:04', 'published', 1, NULL, '2018-07-06 12:25:04', '2018-07-06 12:25:04', 0, 1),
	(30, 'SilverStripe\\Versioned\\ChangeSet', '2018-07-06 12:25:18', '2018-07-06 12:25:18', 'Generated by publish of \'Algemeen\' at 6 jul. 2018 12:25:18', 'published', 1, NULL, '2018-07-06 12:25:18', '2018-07-06 12:25:18', 0, 1),
	(31, 'SilverStripe\\Versioned\\ChangeSet', '2018-07-06 12:25:44', '2018-07-06 12:25:44', 'Generated by publish of \'Is dit een faketekst?\' at 6 jul. 2018 12:25:44', 'published', 1, NULL, '2018-07-06 12:25:44', '2018-07-06 12:25:44', 0, 1),
	(32, 'SilverStripe\\Versioned\\ChangeSet', '2018-07-06 12:26:34', '2018-07-06 12:26:34', 'Generated by publish of \'Worden deze vragen later vervangen door de uiteindelijke tekst?\' at 6 jul. 2018 12:26:34', 'published', 1, NULL, '2018-07-06 12:26:34', '2018-07-06 12:26:34', 0, 1),
	(33, 'SilverStripe\\Versioned\\ChangeSet', '2018-07-06 12:26:57', '2018-07-06 12:26:57', 'Generated by publish of \'Financieel\' at 6 jul. 2018 12:26:57', 'published', 1, NULL, '2018-07-06 12:26:57', '2018-07-06 12:26:57', 0, 1),
	(34, 'SilverStripe\\Versioned\\ChangeSet', '2018-07-06 12:27:30', '2018-07-06 12:27:30', 'Generated by publish of \'Hoe kan ik mijn factuur betalen?\' at 6 jul. 2018 12:27:30', 'published', 1, NULL, '2018-07-06 12:27:30', '2018-07-06 12:27:30', 0, 1),
	(35, 'SilverStripe\\Versioned\\ChangeSet', '2018-07-06 12:28:18', '2018-07-06 12:28:18', 'Generated by publish of \'Is de faketekst een tekst die eigenlijk nergens over gaat?\' at 6 jul. 2018 12:28:18', 'published', 1, NULL, '2018-07-06 12:28:18', '2018-07-06 12:28:18', 0, 1),
	(36, 'SilverStripe\\Versioned\\ChangeSet', '2018-07-06 12:35:26', '2018-07-06 12:35:26', 'Generated by publish of \'About Us\' at 6 jul. 2018 12:35:26', 'published', 1, NULL, '2018-07-06 12:35:26', '2018-07-06 12:35:26', 0, 1),
	(37, 'SilverStripe\\Versioned\\ChangeSet', '2018-07-06 12:35:57', '2018-07-06 12:35:57', 'Generated by publish of \'About Us\' at 6 jul. 2018 12:35:57', 'published', 1, NULL, '2018-07-06 12:35:57', '2018-07-06 12:35:57', 0, 1),
	(38, 'SilverStripe\\Versioned\\ChangeSet', '2018-07-06 12:41:09', '2018-07-06 12:41:09', 'Generated by publish of \'Home\' at 6 jul. 2018 12:41:09', 'published', 1, NULL, '2018-07-06 12:41:09', '2018-07-06 12:41:09', 0, 1),
	(39, 'SilverStripe\\Versioned\\ChangeSet', '2018-07-06 12:41:27', '2018-07-06 12:41:27', 'Generated by publish of \'Over ons\' at 6 jul. 2018 12:41:27', 'published', 1, NULL, '2018-07-06 12:41:27', '2018-07-06 12:41:27', 0, 1),
	(40, 'SilverStripe\\Versioned\\ChangeSet', '2018-07-06 12:41:44', '2018-07-06 12:41:44', 'Generated by publish of \'Contact\' at 6 jul. 2018 12:41:44', 'published', 1, NULL, '2018-07-06 12:41:44', '2018-07-06 12:41:44', 0, 1),
	(41, 'SilverStripe\\Versioned\\ChangeSet', '2018-07-06 13:55:13', '2018-07-06 13:55:13', 'Generated by publish of \'Privacyverklaring\' at 6 jul. 2018 13:55:13', 'published', 1, NULL, '2018-07-06 13:55:13', '2018-07-06 13:55:13', 0, 1),
	(42, 'SilverStripe\\Versioned\\ChangeSet', '2018-07-06 13:55:26', '2018-07-06 13:55:26', 'Generated by publish of \'Privacyverklaring\' at 6 jul. 2018 13:55:26', 'published', 1, NULL, '2018-07-06 13:55:26', '2018-07-06 13:55:26', 0, 1),
	(43, 'SilverStripe\\Versioned\\ChangeSet', '2018-07-06 13:56:30', '2018-07-06 13:56:30', 'Generated by publish of \'Privacyverklaring\' at 6 jul. 2018 13:56:30', 'published', 1, NULL, '2018-07-06 13:56:30', '2018-07-06 13:56:30', 0, 1),
	(44, 'SilverStripe\\Versioned\\ChangeSet', '2018-07-06 13:56:58', '2018-07-06 13:56:58', 'Generated by publish of \'Privacyverklaring\' at 6 jul. 2018 13:56:58', 'published', 1, NULL, '2018-07-06 13:56:58', '2018-07-06 13:56:58', 0, 1),
	(45, 'SilverStripe\\Versioned\\ChangeSet', '2018-07-06 13:57:35', '2018-07-06 13:57:35', 'Generated by publish of \'Veelgestelde vragen\' at 6 jul. 2018 13:57:35', 'published', 1, NULL, '2018-07-06 13:57:35', '2018-07-06 13:57:35', 0, 1),
	(46, 'SilverStripe\\Versioned\\ChangeSet', '2018-07-06 13:58:14', '2018-07-06 13:58:14', 'Generated by publish of \'About Us\' at 6 jul. 2018 13:58:14', 'published', 1, NULL, '2018-07-06 13:58:14', '2018-07-06 13:58:14', 0, 1),
	(47, 'SilverStripe\\Versioned\\ChangeSet', '2018-07-06 13:58:41', '2018-07-06 13:58:41', 'Generated by publish of \'Hestec\' at 6 jul. 2018 13:58:41', 'published', 1, NULL, '2018-07-06 13:58:41', '2018-07-06 13:58:41', 0, 1),
	(48, 'SilverStripe\\Versioned\\ChangeSet', '2018-07-06 13:59:20', '2018-07-06 13:59:20', 'Generated by publish of \'info@ss-boilerplate.hst1.nl\' at 6 jul. 2018 13:59:20', 'published', 1, NULL, '2018-07-06 13:59:20', '2018-07-06 13:59:20', 0, 1),
	(49, 'SilverStripe\\Versioned\\ChangeSet', '2018-07-06 13:59:46', '2018-07-06 13:59:46', 'Generated by publish of \'Footer column 2\' at 6 jul. 2018 13:59:46', 'published', 1, NULL, '2018-07-06 13:59:46', '2018-07-06 13:59:46', 0, 1),
	(50, 'SilverStripe\\Versioned\\ChangeSet', '2018-07-09 15:51:28', '2018-07-09 15:51:28', 'Generated by publish of \'Pagina als dropdown\' at 9 jul. 2018 15:51:28', 'published', 1, NULL, '2018-07-09 15:51:28', '2018-07-09 15:51:28', 0, 1),
	(51, 'SilverStripe\\Versioned\\ChangeSet', '2018-07-09 15:52:07', '2018-07-09 15:52:06', 'Generated by publish of \'Nog een pagina\' at 9 jul. 2018 15:52:06', 'published', 1, NULL, '2018-07-09 15:52:07', '2018-07-09 15:52:07', 0, 1),
	(52, 'SilverStripe\\Versioned\\ChangeSet', '2018-07-10 14:24:56', '2018-07-10 14:24:56', 'Generated by publish of \'Your Site Name\' at 10 jul. 2018 14:24:56', 'published', 1, NULL, '2018-07-10 14:24:56', '2018-07-10 14:24:56', 0, 1),
	(53, 'SilverStripe\\Versioned\\ChangeSet', '2018-07-10 14:30:13', '2018-07-10 14:30:13', 'Generated by publish of \'Your Site Name\' at 10 jul. 2018 14:30:13', 'published', 1, NULL, '2018-07-10 14:30:13', '2018-07-10 14:30:13', 0, 1),
	(54, 'SilverStripe\\Versioned\\ChangeSet', '2018-07-10 14:39:42', '2018-07-10 14:39:42', 'Generated by publish of \'Dropdown\' at 10 jul. 2018 14:39:42', 'published', 1, NULL, '2018-07-10 14:39:42', '2018-07-10 14:39:42', 0, 1),
	(55, 'SilverStripe\\Versioned\\ChangeSet', '2018-07-10 14:39:51', '2018-07-10 14:39:51', 'Generated by publish of \'Dropdown\' at 10 jul. 2018 14:39:51', 'published', 1, NULL, '2018-07-10 14:39:51', '2018-07-10 14:39:51', 0, 1),
	(56, 'SilverStripe\\Versioned\\ChangeSet', '2018-07-10 14:40:11', '2018-07-10 14:40:11', 'Generated by publish of \'Pagina als dropdown\' at 10 jul. 2018 14:40:11', 'published', 1, NULL, '2018-07-10 14:40:11', '2018-07-10 14:40:11', 0, 1),
	(57, 'SilverStripe\\Versioned\\ChangeSet', '2018-07-10 14:40:18', '2018-07-10 14:40:18', 'Generated by publish of \'Nog een pagina\' at 10 jul. 2018 14:40:18', 'published', 1, NULL, '2018-07-10 14:40:18', '2018-07-10 14:40:18', 0, 1),
	(58, 'SilverStripe\\Versioned\\ChangeSet', '2018-07-10 14:40:40', '2018-07-10 14:40:40', 'Generated by publish of \'Elements\' at 10 jul. 2018 14:40:40', 'published', 1, NULL, '2018-07-10 14:40:40', '2018-07-10 14:40:40', 0, 1),
	(59, 'SilverStripe\\Versioned\\ChangeSet', '2018-07-10 14:42:17', '2018-07-10 14:42:17', 'Generated by publish of \'Video\' at 10 jul. 2018 14:42:17', 'published', 1, NULL, '2018-07-10 14:42:17', '2018-07-10 14:42:17', 0, 1),
	(60, 'SilverStripe\\Versioned\\ChangeSet', '2018-07-10 15:00:22', '2018-07-10 15:00:22', 'Generated by publish of \'Your Site Name\' at 10 jul. 2018 15:00:22', 'published', 1, NULL, '2018-07-10 15:00:22', '2018-07-10 15:00:22', 0, 1),
	(61, 'SilverStripe\\Versioned\\ChangeSet', '2018-07-10 16:40:21', '2018-07-10 16:40:21', 'Generated by publish of \'Veelgestelde vragen\' at 10 jul. 2018 16:40:21', 'published', 1, NULL, '2018-07-10 16:40:21', '2018-07-10 16:40:21', 0, 1),
	(62, 'SilverStripe\\Versioned\\ChangeSet', '2018-07-10 16:41:10', '2018-07-10 16:41:10', 'Generated by publish of \'Hoe laat is het?\' at 10 jul. 2018 16:41:10', 'published', 1, NULL, '2018-07-10 16:41:10', '2018-07-10 16:41:10', 0, 1),
	(63, 'SilverStripe\\Versioned\\ChangeSet', '2018-07-10 16:49:24', '2018-07-10 16:49:24', 'Generated by publish of \'Welke datum is het vandaag?\' at 10 jul. 2018 16:49:24', 'published', 1, NULL, '2018-07-10 16:49:24', '2018-07-10 16:49:24', 0, 1),
	(64, 'SilverStripe\\Versioned\\ChangeSet', '2018-07-10 16:53:17', '2018-07-10 16:53:17', 'Generated by publish of \'Veelgestelde vragen\' at 10 jul. 2018 16:53:17', 'published', 1, NULL, '2018-07-10 16:53:17', '2018-07-10 16:53:17', 0, 1),
	(65, 'SilverStripe\\Versioned\\ChangeSet', '2018-07-10 16:58:27', '2018-07-10 16:58:27', 'Generated by publish of \'Veelgestelde vragen\' at 10 jul. 2018 16:58:27', 'published', 1, NULL, '2018-07-10 16:58:27', '2018-07-10 16:58:27', 0, 1),
	(66, 'SilverStripe\\Versioned\\ChangeSet', '2018-07-12 12:42:38', '2018-07-12 12:42:38', 'Generated by publish of \'Elements\' at 12 jul. 2018 12:42:38', 'published', 1, NULL, '2018-07-12 12:42:38', '2018-07-12 12:42:38', 0, 1),
	(67, 'SilverStripe\\Versioned\\ChangeSet', '2018-07-13 16:12:02', '2018-07-13 16:12:02', 'Generated by publish of \'Veelgestelde vragen\' at 13 jul. 2018 16:12:02', 'published', 1, NULL, '2018-07-13 16:12:02', '2018-07-13 16:12:02', 0, 1),
	(68, 'SilverStripe\\Versioned\\ChangeSet', '2018-07-13 16:28:04', '2018-07-13 16:28:04', 'Generated by publish of \'Veelgestelde vragen\' at 13 jul. 2018 16:28:04', 'published', 1, NULL, '2018-07-13 16:28:04', '2018-07-13 16:28:04', 0, 1),
	(69, 'SilverStripe\\Versioned\\ChangeSet', '2018-08-02 17:44:02', '2018-08-02 17:44:02', 'Generated by publish of \'Test\' at 2 aug. 2018 17:44:02', 'published', 1, NULL, '2018-08-02 17:44:02', '2018-08-02 17:44:02', 0, 1),
	(70, 'SilverStripe\\Versioned\\ChangeSet', '2018-08-03 10:20:12', '2018-08-03 10:20:12', 'Generated by publish of \'Test\' at 3 aug. 2018 10:20:12', 'published', 1, NULL, '2018-08-03 10:20:12', '2018-08-03 10:20:12', 0, 1),
	(71, 'SilverStripe\\Versioned\\ChangeSet', '2018-08-03 10:26:36', '2018-08-03 10:26:36', 'Generated by publish of \'Test\' at 3 aug. 2018 10:26:36', 'published', 1, NULL, '2018-08-03 10:26:36', '2018-08-03 10:26:36', 0, 1),
	(72, 'SilverStripe\\Versioned\\ChangeSet', '2018-08-03 10:29:23', '2018-08-03 10:29:23', 'Generated by publish of \'Fotolia 96828948 M\' at 3 aug. 2018 10:29:23', 'published', 1, NULL, '2018-08-03 10:29:23', '2018-08-03 10:29:23', 0, 1),
	(73, 'SilverStripe\\Versioned\\ChangeSet', '2018-08-03 10:43:06', '2018-08-03 10:43:05', 'Generated by publish of \'Afbeelding links of rechts?\' at 3 aug. 2018 10:43:05', 'published', 1, NULL, '2018-08-03 10:43:06', '2018-08-03 10:43:06', 0, 1),
	(74, 'SilverStripe\\Versioned\\ChangeSet', '2018-08-03 11:03:42', '2018-08-03 11:03:42', 'Generated by publish of \'Afbeelding links of rechts?\' at 3 aug. 2018 11:03:42', 'published', 1, NULL, '2018-08-03 11:03:42', '2018-08-03 11:03:42', 0, 1),
	(75, 'SilverStripe\\Versioned\\ChangeSet', '2018-08-03 11:12:44', '2018-08-03 11:12:44', 'Generated by publish of \'Afbeelding links of rechts?\' at 3 aug. 2018 11:12:44', 'published', 1, NULL, '2018-08-03 11:12:44', '2018-08-03 11:12:44', 0, 1),
	(76, 'SilverStripe\\Versioned\\ChangeSet', '2018-08-03 11:13:15', '2018-08-03 11:13:15', 'Generated by publish of \'Afbeelding links of rechts?\' at 3 aug. 2018 11:13:15', 'published', 1, NULL, '2018-08-03 11:13:15', '2018-08-03 11:13:15', 0, 1),
	(77, 'SilverStripe\\Versioned\\ChangeSet', '2018-08-07 16:52:47', '2018-08-07 16:52:47', 'Generated by publish of \'Home\' at 7 aug. 2018 16:52:47', 'published', 1, NULL, '2018-08-07 16:52:47', '2018-08-07 16:52:47', 0, 1),
	(78, 'SilverStripe\\Versioned\\ChangeSet', '2018-09-18 10:22:54', '2018-09-18 10:22:54', 'Generated by publish of \'Veelgestelde vragen\' at 18 sep. 2018 10:22:54', 'published', 1, NULL, '2018-09-18 10:22:54', '2018-09-18 10:22:54', 0, 1),
	(79, 'SilverStripe\\Versioned\\ChangeSet', '2018-09-18 10:34:37', '2018-09-18 10:34:37', 'Generated by publish of \'test\' at 18 sep. 2018 10:34:37', 'published', 1, NULL, '2018-09-18 10:34:37', '2018-09-18 10:34:37', 0, 1),
	(80, 'SilverStripe\\Versioned\\ChangeSet', '2018-09-18 10:35:18', '2018-09-18 10:35:18', 'Generated by publish of \'Algemeen\' at 18 sep. 2018 10:35:18', 'published', 1, NULL, '2018-09-18 10:35:18', '2018-09-18 10:35:18', 0, 1),
	(81, 'SilverStripe\\Versioned\\ChangeSet', '2018-09-18 10:36:11', '2018-09-18 10:36:11', 'Generated by publish of \'Is dit een faketekst?\' at 18 sep. 2018 10:36:11', 'published', 1, NULL, '2018-09-18 10:36:11', '2018-09-18 10:36:11', 0, 1),
	(82, 'SilverStripe\\Versioned\\ChangeSet', '2018-09-18 10:36:56', '2018-09-18 10:36:56', 'Generated by publish of \'Worden deze vragen later vervangen door de uiteindelijke tekst?\' at 18 sep. 2018 10:36:56', 'published', 1, NULL, '2018-09-18 10:36:56', '2018-09-18 10:36:56', 0, 1),
	(83, 'SilverStripe\\Versioned\\ChangeSet', '2018-09-18 10:37:33', '2018-09-18 10:37:33', 'Generated by publish of \'Financieel\' at 18 sep. 2018 10:37:33', 'published', 1, NULL, '2018-09-18 10:37:33', '2018-09-18 10:37:33', 0, 1),
	(84, 'SilverStripe\\Versioned\\ChangeSet', '2018-09-18 10:38:11', '2018-09-18 10:38:11', 'Generated by publish of \'3\' at 18 sep. 2018 10:38:11', 'published', 1, NULL, '2018-09-18 10:38:11', '2018-09-18 10:38:11', 0, 1),
	(85, 'SilverStripe\\Versioned\\ChangeSet', '2018-09-18 10:38:37', '2018-09-18 10:38:37', 'Generated by publish of \'Is de faketekst een tekst die eigenlijk nergens over gaat?\' at 18 sep. 2018 10:38:37', 'published', 1, NULL, '2018-09-18 10:38:37', '2018-09-18 10:38:37', 0, 1),
	(86, 'SilverStripe\\Versioned\\ChangeSet', '2018-09-18 10:38:55', '2018-09-18 10:38:55', 'Generated by publish of \'Hoe kan ik mijn factuur betalen?\' at 18 sep. 2018 10:38:55', 'published', 1, NULL, '2018-09-18 10:38:55', '2018-09-18 10:38:55', 0, 1),
	(87, 'SilverStripe\\Versioned\\ChangeSet', '2018-09-18 10:49:00', '2018-09-18 10:49:00', 'Generated by publish of \'Test\' at 18 sep. 2018 10:49:00', 'published', 1, NULL, '2018-09-18 10:49:00', '2018-09-18 10:49:00', 0, 1);
/*!40000 ALTER TABLE `ChangeSet` ENABLE KEYS */;


-- Structuur van  tabel vagrant.ChangeSetItem wordt geschreven
CREATE TABLE IF NOT EXISTS `ChangeSetItem` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('SilverStripe\\Versioned\\ChangeSetItem') CHARACTER SET utf8 DEFAULT 'SilverStripe\\Versioned\\ChangeSetItem',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `VersionBefore` int(11) NOT NULL DEFAULT '0',
  `VersionAfter` int(11) NOT NULL DEFAULT '0',
  `Added` enum('explicitly','implicitly') CHARACTER SET utf8 DEFAULT 'implicitly',
  `ChangeSetID` int(11) NOT NULL DEFAULT '0',
  `ObjectID` int(11) NOT NULL DEFAULT '0',
  `ObjectClass` enum('Hestec\\SisowMethod\\SisowMethod','Hestec\\ElementalExtensions\\Dataobjects\\FaqQuestion','Hestec\\ExactOnline\\ExactOnlineConnection','Hestec\\FaqPage\\FaqCategorie','Hestec\\FaqPage\\FaqQuestion','BrandColor','BrandColorType','ExtraNavigationItem','Hestec\\Tools\\OwnIp','SilverStripe\\Assets\\File','SilverStripe\\SiteConfig\\SiteConfig','SilverStripe\\Versioned\\ChangeSetItem','SilverStripe\\Versioned\\ChangeSet','DNADesign\\Elemental\\Models\\BaseElement','DNADesign\\Elemental\\Models\\ElementalArea','gorriecoe\\Link\\Models\\Link','gorriecoe\\Menu\\Models\\MenuSet','SilverStripe\\Assets\\Shortcodes\\FileLink','SilverStripe\\Blog\\Model\\BlogCategory','SilverStripe\\Blog\\Model\\BlogTag','SilverStripe\\CMS\\Model\\SiteTreeLink','SilverStripe\\CMS\\Model\\SiteTree','SilverStripe\\Security\\Group','SilverStripe\\Security\\LoginAttempt','SilverStripe\\Security\\MemberPassword','SilverStripe\\Security\\Member','SilverStripe\\Security\\Permission','SilverStripe\\Security\\PermissionRoleCode','SilverStripe\\Security\\PermissionRole','SilverStripe\\Security\\RememberLoginHash','SilverStripe\\UserForms\\Model\\EditableCustomRule','SilverStripe\\UserForms\\Model\\EditableFormField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableOption','SilverStripe\\UserForms\\Model\\Recipient\\EmailRecipientCondition','SilverStripe\\UserForms\\Model\\Recipient\\EmailRecipient','SilverStripe\\UserForms\\Model\\Submission\\SubmittedFormField','SilverStripe\\UserForms\\Model\\Submission\\SubmittedForm','SilverStripe\\Assets\\Folder','SilverStripe\\Assets\\Image','Hestec\\ElementalExtensions\\Elements\\ElementContentImage','Hestec\\ElementalExtensions\\Elements\\ElementFaq','Hestec\\ElementalExtensions\\Elements\\ElementVideo','DNADesign\\Elemental\\Models\\ElementContent','gorriecoe\\Menu\\Models\\MenuLink','Page','ElementPage','HomePage','Hestec\\Faqpage\\FaqPage','FirstChildRedirect','SilverStripe\\ErrorPage\\ErrorPage','SilverStripe\\Blog\\Model\\Blog','SilverStripe\\Blog\\Model\\BlogPost','SilverStripe\\CMS\\Model\\RedirectorPage','SilverStripe\\CMS\\Model\\VirtualPage','SilverStripe\\UserForms\\Model\\UserDefinedForm','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableCheckbox','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableCountryDropdownField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableDateField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableEmailField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableFieldGroupEnd','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableFieldGroup','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableFileField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableFormHeading','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableFormStep','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableLiteralField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableMemberListField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableMultipleOptionField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableNumericField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableTextField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableCheckboxGroupField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableDropdown','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableRadioField','SilverStripe\\UserForms\\Model\\Submission\\SubmittedFileField','TheWebmen\\FAQ\\Model\\FAQCategorie','TheWebmen\\FAQ\\Model\\FAQQuestion','Hestec\\UpcomingEvents\\Event','OwnIp') CHARACTER SET utf8 DEFAULT 'Hestec\\SisowMethod\\SisowMethod',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ObjectUniquePerChangeSet` (`ObjectID`,`ObjectClass`,`ChangeSetID`),
  KEY `ClassName` (`ClassName`),
  KEY `ChangeSetID` (`ChangeSetID`),
  KEY `Object` (`ObjectID`,`ObjectClass`)
) ENGINE=InnoDB AUTO_INCREMENT=118 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.ChangeSetItem: ~107 rows (ongeveer)
DELETE FROM `ChangeSetItem`;
/*!40000 ALTER TABLE `ChangeSetItem` DISABLE KEYS */;
INSERT INTO `ChangeSetItem` (`ID`, `ClassName`, `LastEdited`, `Created`, `VersionBefore`, `VersionAfter`, `Added`, `ChangeSetID`, `ObjectID`, `ObjectClass`) VALUES
	(1, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-03-26 20:15:00', '2018-03-26 20:15:00', 0, 0, 'explicitly', 1, 1, 'OwnIp'),
	(2, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-03-26 22:28:55', '2018-03-26 22:28:55', 1, 2, 'explicitly', 2, 1, 'SilverStripe\\CMS\\Model\\SiteTree'),
	(3, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-03-26 22:28:55', '2018-03-26 22:28:55', 0, 1, 'implicitly', 2, 1, 'DNADesign\\Elemental\\Models\\ElementalArea'),
	(4, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-03-26 22:34:42', '2018-03-26 22:34:42', 0, 3, 'explicitly', 3, 1, 'DNADesign\\Elemental\\Models\\BaseElement'),
	(5, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-03-29 20:45:36', '2018-03-29 20:45:36', 0, 2, 'explicitly', 4, 6, 'SilverStripe\\CMS\\Model\\SiteTree'),
	(6, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-03-29 20:45:36', '2018-03-29 20:45:36', 0, 1, 'implicitly', 4, 2, 'DNADesign\\Elemental\\Models\\ElementalArea'),
	(7, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-03-29 20:45:36', '2018-03-29 20:45:36', 0, 1, 'implicitly', 4, 3, 'DNADesign\\Elemental\\Models\\ElementalArea'),
	(8, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-03-29 20:46:21', '2018-03-29 20:46:21', 0, 3, 'explicitly', 5, 2, 'DNADesign\\Elemental\\Models\\BaseElement'),
	(9, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-03-29 20:55:29', '2018-03-29 20:55:29', 0, 3, 'explicitly', 6, 3, 'DNADesign\\Elemental\\Models\\BaseElement'),
	(10, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-03-29 20:58:08', '2018-03-29 20:58:08', 3, 5, 'explicitly', 7, 3, 'DNADesign\\Elemental\\Models\\BaseElement'),
	(11, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-03-29 20:58:49', '2018-03-29 20:58:49', 5, 7, 'explicitly', 8, 3, 'DNADesign\\Elemental\\Models\\BaseElement'),
	(12, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-03-29 21:22:29', '2018-03-29 21:22:29', 2, 2, 'explicitly', 9, 6, 'SilverStripe\\CMS\\Model\\SiteTree'),
	(13, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-03-29 21:22:29', '2018-03-29 21:22:29', 1, 1, 'implicitly', 9, 2, 'DNADesign\\Elemental\\Models\\ElementalArea'),
	(14, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-03-29 21:22:29', '2018-03-29 21:22:29', 3, 3, 'implicitly', 9, 2, 'DNADesign\\Elemental\\Models\\BaseElement'),
	(15, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-03-29 21:22:29', '2018-03-29 21:22:29', 1, 1, 'implicitly', 9, 3, 'DNADesign\\Elemental\\Models\\ElementalArea'),
	(16, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-03-29 21:23:08', '2018-03-29 21:23:08', 0, 3, 'explicitly', 10, 4, 'DNADesign\\Elemental\\Models\\BaseElement'),
	(17, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-03-29 22:13:17', '2018-03-29 22:13:17', 0, 0, 'explicitly', 11, 1, 'SilverStripe\\Security\\Member'),
	(18, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-03-29 22:15:25', '2018-03-29 22:15:25', 3, 5, 'explicitly', 12, 4, 'DNADesign\\Elemental\\Models\\BaseElement'),
	(19, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-03-31 14:13:53', '2018-03-31 14:13:53', 2, 3, 'explicitly', 13, 6, 'SilverStripe\\CMS\\Model\\SiteTree'),
	(20, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-03-31 14:13:53', '2018-03-31 14:13:53', 1, 1, 'implicitly', 13, 2, 'DNADesign\\Elemental\\Models\\ElementalArea'),
	(21, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-03-31 14:13:53', '2018-03-31 14:13:53', 3, 3, 'implicitly', 13, 2, 'DNADesign\\Elemental\\Models\\BaseElement'),
	(22, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-03-31 14:13:53', '2018-03-31 14:13:53', 5, 5, 'implicitly', 13, 4, 'DNADesign\\Elemental\\Models\\BaseElement'),
	(23, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-03-31 14:13:54', '2018-03-31 14:13:53', 1, 1, 'implicitly', 13, 3, 'DNADesign\\Elemental\\Models\\ElementalArea'),
	(24, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-03-31 14:13:54', '2018-03-31 14:13:53', 0, 1, 'implicitly', 13, 4, 'DNADesign\\Elemental\\Models\\ElementalArea'),
	(25, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-03-31 14:15:54', '2018-03-31 14:15:54', 0, 3, 'explicitly', 14, 5, 'DNADesign\\Elemental\\Models\\BaseElement'),
	(26, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-03-31 15:13:38', '2018-03-31 15:13:38', 0, 3, 'explicitly', 15, 6, 'DNADesign\\Elemental\\Models\\BaseElement'),
	(27, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-03-31 16:16:05', '2018-03-31 16:16:05', 3, 3, 'explicitly', 16, 6, 'SilverStripe\\CMS\\Model\\SiteTree'),
	(28, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-03-31 16:16:05', '2018-03-31 16:16:05', 1, 1, 'implicitly', 16, 2, 'DNADesign\\Elemental\\Models\\ElementalArea'),
	(29, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-03-31 16:16:05', '2018-03-31 16:16:05', 3, 3, 'implicitly', 16, 5, 'DNADesign\\Elemental\\Models\\BaseElement'),
	(30, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-03-31 16:16:05', '2018-03-31 16:16:05', 3, 4, 'implicitly', 16, 6, 'DNADesign\\Elemental\\Models\\BaseElement'),
	(31, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-03-31 16:16:05', '2018-03-31 16:16:05', 3, 4, 'implicitly', 16, 2, 'DNADesign\\Elemental\\Models\\BaseElement'),
	(32, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-03-31 16:16:05', '2018-03-31 16:16:05', 1, 1, 'implicitly', 16, 3, 'DNADesign\\Elemental\\Models\\ElementalArea'),
	(33, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-03-31 16:16:05', '2018-03-31 16:16:05', 1, 1, 'implicitly', 16, 4, 'DNADesign\\Elemental\\Models\\ElementalArea'),
	(34, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-03-31 16:16:17', '2018-03-31 16:16:16', 3, 3, 'explicitly', 17, 6, 'SilverStripe\\CMS\\Model\\SiteTree'),
	(35, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-03-31 16:16:17', '2018-03-31 16:16:16', 1, 1, 'implicitly', 17, 2, 'DNADesign\\Elemental\\Models\\ElementalArea'),
	(36, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-03-31 16:16:17', '2018-03-31 16:16:16', 3, 3, 'implicitly', 17, 5, 'DNADesign\\Elemental\\Models\\BaseElement'),
	(37, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-03-31 16:16:17', '2018-03-31 16:16:16', 4, 5, 'implicitly', 17, 2, 'DNADesign\\Elemental\\Models\\BaseElement'),
	(38, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-03-31 16:16:17', '2018-03-31 16:16:16', 4, 5, 'implicitly', 17, 6, 'DNADesign\\Elemental\\Models\\BaseElement'),
	(39, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-03-31 16:16:17', '2018-03-31 16:16:16', 1, 1, 'implicitly', 17, 3, 'DNADesign\\Elemental\\Models\\ElementalArea'),
	(40, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-03-31 16:16:17', '2018-03-31 16:16:17', 1, 1, 'implicitly', 17, 4, 'DNADesign\\Elemental\\Models\\ElementalArea'),
	(41, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-04-04 10:48:57', '2018-04-04 10:48:57', 0, 2, 'explicitly', 18, 7, 'SilverStripe\\CMS\\Model\\SiteTree'),
	(42, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-04-04 11:25:08', '2018-04-04 11:25:08', 0, 0, 'explicitly', 19, 1, 'Hestec\\UpcomingEvents\\Event'),
	(43, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-07-04 17:18:02', '2018-07-04 17:18:02', 0, 0, 'explicitly', 26, 1, 'SilverStripe\\SiteConfig\\SiteConfig'),
	(44, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-07-04 17:18:10', '2018-07-04 17:18:10', 0, 0, 'explicitly', 27, 1, 'SilverStripe\\SiteConfig\\SiteConfig'),
	(45, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-07-06 12:23:59', '2018-07-06 12:23:59', 0, 5, 'explicitly', 28, 8, 'SilverStripe\\CMS\\Model\\SiteTree'),
	(46, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-07-06 12:25:04', '2018-07-06 12:25:04', 5, 6, 'explicitly', 29, 8, 'SilverStripe\\CMS\\Model\\SiteTree'),
	(47, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-07-06 12:25:18', '2018-07-06 12:25:18', 0, 0, 'explicitly', 30, 1, 'TheWebmen\\FAQ\\Model\\FAQCategorie'),
	(48, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-07-06 12:25:44', '2018-07-06 12:25:44', 0, 0, 'explicitly', 31, 1, 'TheWebmen\\FAQ\\Model\\FAQQuestion'),
	(49, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-07-06 12:26:34', '2018-07-06 12:26:34', 0, 0, 'explicitly', 32, 2, 'TheWebmen\\FAQ\\Model\\FAQQuestion'),
	(50, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-07-06 12:26:57', '2018-07-06 12:26:57', 0, 0, 'explicitly', 33, 2, 'TheWebmen\\FAQ\\Model\\FAQCategorie'),
	(51, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-07-06 12:27:30', '2018-07-06 12:27:30', 0, 0, 'explicitly', 34, 3, 'TheWebmen\\FAQ\\Model\\FAQQuestion'),
	(52, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-07-06 12:28:18', '2018-07-06 12:28:18', 0, 0, 'explicitly', 35, 4, 'TheWebmen\\FAQ\\Model\\FAQQuestion'),
	(53, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-07-06 12:35:26', '2018-07-06 12:35:26', 1, 2, 'explicitly', 36, 2, 'SilverStripe\\CMS\\Model\\SiteTree'),
	(54, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-07-06 12:35:57', '2018-07-06 12:35:57', 2, 3, 'explicitly', 37, 2, 'SilverStripe\\CMS\\Model\\SiteTree'),
	(55, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-07-06 12:41:09', '2018-07-06 12:41:09', 0, 0, 'explicitly', 38, 1, 'gorriecoe\\Link\\Models\\Link'),
	(56, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-07-06 12:41:27', '2018-07-06 12:41:27', 0, 0, 'explicitly', 39, 2, 'gorriecoe\\Link\\Models\\Link'),
	(57, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-07-06 12:41:44', '2018-07-06 12:41:44', 0, 0, 'explicitly', 40, 3, 'gorriecoe\\Link\\Models\\Link'),
	(58, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-07-06 13:55:13', '2018-07-06 13:55:13', 0, 6, 'explicitly', 41, 9, 'SilverStripe\\CMS\\Model\\SiteTree'),
	(59, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-07-06 13:55:26', '2018-07-06 13:55:26', 6, 7, 'explicitly', 42, 9, 'SilverStripe\\CMS\\Model\\SiteTree'),
	(60, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-07-06 13:56:30', '2018-07-06 13:56:30', 7, 8, 'explicitly', 43, 9, 'SilverStripe\\CMS\\Model\\SiteTree'),
	(61, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-07-06 13:56:58', '2018-07-06 13:56:58', 0, 0, 'explicitly', 44, 4, 'gorriecoe\\Link\\Models\\Link'),
	(62, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-07-06 13:57:35', '2018-07-06 13:57:35', 0, 0, 'explicitly', 45, 5, 'gorriecoe\\Link\\Models\\Link'),
	(63, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-07-06 13:58:14', '2018-07-06 13:58:14', 0, 0, 'explicitly', 46, 6, 'gorriecoe\\Link\\Models\\Link'),
	(64, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-07-06 13:58:41', '2018-07-06 13:58:41', 0, 0, 'explicitly', 47, 7, 'gorriecoe\\Link\\Models\\Link'),
	(65, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-07-06 13:59:20', '2018-07-06 13:59:20', 0, 0, 'explicitly', 48, 8, 'gorriecoe\\Link\\Models\\Link'),
	(66, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-07-06 13:59:46', '2018-07-06 13:59:46', 0, 0, 'explicitly', 49, 3, 'gorriecoe\\Menu\\Models\\MenuSet'),
	(67, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-07-09 15:51:28', '2018-07-09 15:51:28', 0, 4, 'explicitly', 50, 10, 'SilverStripe\\CMS\\Model\\SiteTree'),
	(68, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-07-09 15:52:07', '2018-07-09 15:52:06', 0, 4, 'explicitly', 51, 11, 'SilverStripe\\CMS\\Model\\SiteTree'),
	(69, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-07-10 14:24:56', '2018-07-10 14:24:56', 0, 0, 'explicitly', 52, 1, 'SilverStripe\\SiteConfig\\SiteConfig'),
	(70, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-07-10 14:30:13', '2018-07-10 14:30:13', 0, 0, 'explicitly', 53, 1, 'SilverStripe\\SiteConfig\\SiteConfig'),
	(71, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-07-10 14:39:42', '2018-07-10 14:39:42', 0, 2, 'explicitly', 54, 12, 'SilverStripe\\CMS\\Model\\SiteTree'),
	(72, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-07-10 14:39:51', '2018-07-10 14:39:51', 2, 3, 'explicitly', 55, 12, 'SilverStripe\\CMS\\Model\\SiteTree'),
	(73, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-07-10 14:40:11', '2018-07-10 14:40:11', 4, 5, 'explicitly', 56, 10, 'SilverStripe\\CMS\\Model\\SiteTree'),
	(74, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-07-10 14:40:18', '2018-07-10 14:40:18', 4, 5, 'explicitly', 57, 11, 'SilverStripe\\CMS\\Model\\SiteTree'),
	(75, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-07-10 14:40:40', '2018-07-10 14:40:40', 3, 5, 'explicitly', 58, 6, 'SilverStripe\\CMS\\Model\\SiteTree'),
	(76, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-07-10 14:40:40', '2018-07-10 14:40:40', 1, 1, 'implicitly', 58, 2, 'DNADesign\\Elemental\\Models\\ElementalArea'),
	(77, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-07-10 14:40:40', '2018-07-10 14:40:40', 3, 3, 'implicitly', 58, 5, 'DNADesign\\Elemental\\Models\\BaseElement'),
	(78, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-07-10 14:40:40', '2018-07-10 14:40:40', 5, 5, 'implicitly', 58, 2, 'DNADesign\\Elemental\\Models\\BaseElement'),
	(79, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-07-10 14:40:40', '2018-07-10 14:40:40', 5, 5, 'implicitly', 58, 6, 'DNADesign\\Elemental\\Models\\BaseElement'),
	(80, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-07-10 14:40:40', '2018-07-10 14:40:40', 1, 1, 'implicitly', 58, 3, 'DNADesign\\Elemental\\Models\\ElementalArea'),
	(81, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-07-10 14:40:40', '2018-07-10 14:40:40', 1, 1, 'implicitly', 58, 4, 'DNADesign\\Elemental\\Models\\ElementalArea'),
	(82, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-07-10 14:42:17', '2018-07-10 14:42:17', 3, 5, 'explicitly', 59, 5, 'DNADesign\\Elemental\\Models\\BaseElement'),
	(83, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-07-10 15:00:22', '2018-07-10 15:00:22', 0, 0, 'explicitly', 60, 1, 'SilverStripe\\SiteConfig\\SiteConfig'),
	(84, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-07-10 16:40:21', '2018-07-10 16:40:21', 0, 3, 'explicitly', 61, 7, 'DNADesign\\Elemental\\Models\\BaseElement'),
	(85, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-07-10 16:41:10', '2018-07-10 16:41:10', 0, 0, 'explicitly', 62, 1, 'Hestec\\ElementalExtensions\\Dataobjects\\FaqQuestion'),
	(86, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-07-10 16:49:24', '2018-07-10 16:49:24', 0, 0, 'explicitly', 63, 2, 'Hestec\\ElementalExtensions\\Dataobjects\\FaqQuestion'),
	(87, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-07-10 16:53:17', '2018-07-10 16:53:17', 3, 5, 'explicitly', 64, 7, 'DNADesign\\Elemental\\Models\\BaseElement'),
	(88, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-07-10 16:58:27', '2018-07-10 16:58:27', 5, 7, 'explicitly', 65, 7, 'DNADesign\\Elemental\\Models\\BaseElement'),
	(89, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-07-12 12:42:38', '2018-07-12 12:42:38', 5, 5, 'explicitly', 66, 6, 'SilverStripe\\CMS\\Model\\SiteTree'),
	(90, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-07-12 12:42:38', '2018-07-12 12:42:38', 1, 1, 'implicitly', 66, 2, 'DNADesign\\Elemental\\Models\\ElementalArea'),
	(91, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-07-12 12:42:38', '2018-07-12 12:42:38', 5, 5, 'implicitly', 66, 5, 'DNADesign\\Elemental\\Models\\BaseElement'),
	(92, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-07-12 12:42:38', '2018-07-12 12:42:38', 7, 7, 'implicitly', 66, 7, 'DNADesign\\Elemental\\Models\\BaseElement'),
	(93, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-07-12 12:42:38', '2018-07-12 12:42:38', 5, 5, 'implicitly', 66, 2, 'DNADesign\\Elemental\\Models\\BaseElement'),
	(94, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-07-12 12:42:38', '2018-07-12 12:42:38', 1, 1, 'implicitly', 66, 3, 'DNADesign\\Elemental\\Models\\ElementalArea'),
	(95, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-07-12 12:42:38', '2018-07-12 12:42:38', 1, 1, 'implicitly', 66, 4, 'DNADesign\\Elemental\\Models\\ElementalArea'),
	(96, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-07-13 16:12:02', '2018-07-13 16:12:02', 7, 9, 'explicitly', 67, 7, 'DNADesign\\Elemental\\Models\\BaseElement'),
	(97, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-07-13 16:28:04', '2018-07-13 16:28:04', 9, 11, 'explicitly', 68, 7, 'DNADesign\\Elemental\\Models\\BaseElement'),
	(98, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-08-02 17:44:02', '2018-08-02 17:44:02', 0, 4, 'explicitly', 69, 8, 'DNADesign\\Elemental\\Models\\BaseElement'),
	(99, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-08-03 10:20:12', '2018-08-03 10:20:12', 4, 7, 'explicitly', 70, 8, 'DNADesign\\Elemental\\Models\\BaseElement'),
	(100, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-08-03 10:26:36', '2018-08-03 10:26:36', 7, 10, 'explicitly', 71, 8, 'DNADesign\\Elemental\\Models\\BaseElement'),
	(101, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-08-03 10:29:23', '2018-08-03 10:29:23', 0, 3, 'explicitly', 72, 2, 'SilverStripe\\Assets\\File'),
	(102, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-08-03 10:43:06', '2018-08-03 10:43:06', 10, 13, 'explicitly', 73, 8, 'DNADesign\\Elemental\\Models\\BaseElement'),
	(103, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-08-03 11:03:42', '2018-08-03 11:03:42', 13, 16, 'explicitly', 74, 8, 'DNADesign\\Elemental\\Models\\BaseElement'),
	(104, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-08-03 11:12:44', '2018-08-03 11:12:44', 16, 19, 'explicitly', 75, 8, 'DNADesign\\Elemental\\Models\\BaseElement'),
	(105, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-08-03 11:13:15', '2018-08-03 11:13:15', 19, 22, 'explicitly', 76, 8, 'DNADesign\\Elemental\\Models\\BaseElement'),
	(106, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-08-03 11:13:15', '2018-08-03 11:13:15', 0, 2, 'implicitly', 76, 3, 'SilverStripe\\Assets\\File'),
	(107, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-08-07 16:52:47', '2018-08-07 16:52:47', 2, 4, 'explicitly', 77, 1, 'SilverStripe\\CMS\\Model\\SiteTree'),
	(108, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-09-18 10:22:54', '2018-09-18 10:22:54', 6, 8, 'explicitly', 78, 8, 'SilverStripe\\CMS\\Model\\SiteTree'),
	(109, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-09-18 10:34:37', '2018-09-18 10:34:37', 0, 0, 'explicitly', 79, 5, 'Hestec\\FaqPage\\FaqQuestion'),
	(110, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-09-18 10:35:18', '2018-09-18 10:35:18', 0, 0, 'explicitly', 80, 3, 'Hestec\\FaqPage\\FaqCategorie'),
	(111, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-09-18 10:36:11', '2018-09-18 10:36:11', 0, 0, 'explicitly', 81, 6, 'Hestec\\FaqPage\\FaqQuestion'),
	(112, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-09-18 10:36:56', '2018-09-18 10:36:56', 0, 0, 'explicitly', 82, 7, 'Hestec\\FaqPage\\FaqQuestion'),
	(113, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-09-18 10:37:33', '2018-09-18 10:37:33', 0, 0, 'explicitly', 83, 4, 'Hestec\\FaqPage\\FaqCategorie'),
	(114, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-09-18 10:38:11', '2018-09-18 10:38:11', 0, 0, 'explicitly', 84, 8, 'Hestec\\FaqPage\\FaqQuestion'),
	(115, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-09-18 10:38:37', '2018-09-18 10:38:37', 0, 0, 'explicitly', 85, 9, 'Hestec\\FaqPage\\FaqQuestion'),
	(116, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-09-18 10:38:55', '2018-09-18 10:38:55', 0, 0, 'explicitly', 86, 8, 'Hestec\\FaqPage\\FaqQuestion'),
	(117, 'SilverStripe\\Versioned\\ChangeSetItem', '2018-09-18 10:49:00', '2018-09-18 10:49:00', 0, 0, 'explicitly', 87, 5, 'Hestec\\FaqPage\\FaqCategorie');
/*!40000 ALTER TABLE `ChangeSetItem` ENABLE KEYS */;


-- Structuur van  tabel vagrant.ChangeSetItem_ReferencedBy wordt geschreven
CREATE TABLE IF NOT EXISTS `ChangeSetItem_ReferencedBy` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ChangeSetItemID` int(11) NOT NULL DEFAULT '0',
  `ChildID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ChangeSetItemID` (`ChangeSetItemID`),
  KEY `ChildID` (`ChildID`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.ChangeSetItem_ReferencedBy: ~35 rows (ongeveer)
DELETE FROM `ChangeSetItem_ReferencedBy`;
/*!40000 ALTER TABLE `ChangeSetItem_ReferencedBy` DISABLE KEYS */;
INSERT INTO `ChangeSetItem_ReferencedBy` (`ID`, `ChangeSetItemID`, `ChildID`) VALUES
	(1, 3, 2),
	(2, 6, 5),
	(3, 7, 5),
	(4, 13, 12),
	(5, 14, 12),
	(6, 15, 12),
	(7, 20, 19),
	(8, 21, 19),
	(9, 22, 19),
	(10, 23, 19),
	(11, 24, 19),
	(12, 28, 27),
	(13, 29, 27),
	(14, 30, 27),
	(15, 31, 27),
	(16, 32, 27),
	(17, 33, 27),
	(18, 35, 34),
	(19, 36, 34),
	(20, 37, 34),
	(21, 38, 34),
	(22, 39, 34),
	(23, 40, 34),
	(24, 76, 75),
	(25, 77, 75),
	(26, 78, 75),
	(27, 79, 75),
	(28, 80, 75),
	(29, 81, 75),
	(30, 90, 89),
	(31, 91, 89),
	(32, 92, 89),
	(33, 93, 89),
	(34, 94, 89),
	(35, 95, 89),
	(36, 106, 105);
/*!40000 ALTER TABLE `ChangeSetItem_ReferencedBy` ENABLE KEYS */;


-- Structuur van  tabel vagrant.EditableCheckbox wordt geschreven
CREATE TABLE IF NOT EXISTS `EditableCheckbox` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CheckedDefault` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.EditableCheckbox: ~0 rows (ongeveer)
DELETE FROM `EditableCheckbox`;
/*!40000 ALTER TABLE `EditableCheckbox` DISABLE KEYS */;
/*!40000 ALTER TABLE `EditableCheckbox` ENABLE KEYS */;


-- Structuur van  tabel vagrant.EditableCheckbox_Live wordt geschreven
CREATE TABLE IF NOT EXISTS `EditableCheckbox_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CheckedDefault` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.EditableCheckbox_Live: ~0 rows (ongeveer)
DELETE FROM `EditableCheckbox_Live`;
/*!40000 ALTER TABLE `EditableCheckbox_Live` DISABLE KEYS */;
/*!40000 ALTER TABLE `EditableCheckbox_Live` ENABLE KEYS */;


-- Structuur van  tabel vagrant.EditableCheckbox_Versions wordt geschreven
CREATE TABLE IF NOT EXISTS `EditableCheckbox_Versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `CheckedDefault` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.EditableCheckbox_Versions: ~0 rows (ongeveer)
DELETE FROM `EditableCheckbox_Versions`;
/*!40000 ALTER TABLE `EditableCheckbox_Versions` DISABLE KEYS */;
/*!40000 ALTER TABLE `EditableCheckbox_Versions` ENABLE KEYS */;


-- Structuur van  tabel vagrant.EditableCountryDropdownField wordt geschreven
CREATE TABLE IF NOT EXISTS `EditableCountryDropdownField` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `UseEmptyString` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `EmptyString` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.EditableCountryDropdownField: ~0 rows (ongeveer)
DELETE FROM `EditableCountryDropdownField`;
/*!40000 ALTER TABLE `EditableCountryDropdownField` DISABLE KEYS */;
/*!40000 ALTER TABLE `EditableCountryDropdownField` ENABLE KEYS */;


-- Structuur van  tabel vagrant.EditableCountryDropdownField_Live wordt geschreven
CREATE TABLE IF NOT EXISTS `EditableCountryDropdownField_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `UseEmptyString` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `EmptyString` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.EditableCountryDropdownField_Live: ~0 rows (ongeveer)
DELETE FROM `EditableCountryDropdownField_Live`;
/*!40000 ALTER TABLE `EditableCountryDropdownField_Live` DISABLE KEYS */;
/*!40000 ALTER TABLE `EditableCountryDropdownField_Live` ENABLE KEYS */;


-- Structuur van  tabel vagrant.EditableCountryDropdownField_Versions wordt geschreven
CREATE TABLE IF NOT EXISTS `EditableCountryDropdownField_Versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `UseEmptyString` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `EmptyString` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.EditableCountryDropdownField_Versions: ~0 rows (ongeveer)
DELETE FROM `EditableCountryDropdownField_Versions`;
/*!40000 ALTER TABLE `EditableCountryDropdownField_Versions` DISABLE KEYS */;
/*!40000 ALTER TABLE `EditableCountryDropdownField_Versions` ENABLE KEYS */;


-- Structuur van  tabel vagrant.EditableCustomRule wordt geschreven
CREATE TABLE IF NOT EXISTS `EditableCustomRule` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('SilverStripe\\UserForms\\Model\\EditableCustomRule') CHARACTER SET utf8 DEFAULT 'SilverStripe\\UserForms\\Model\\EditableCustomRule',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Display` enum('Show','Hide') CHARACTER SET utf8 DEFAULT 'Show',
  `ConditionOption` enum('IsBlank','IsNotBlank','HasValue','ValueNot','ValueLessThan','ValueLessThanEqual','ValueGreaterThan','ValueGreaterThanEqual') CHARACTER SET utf8 DEFAULT 'IsBlank',
  `FieldValue` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Version` int(11) NOT NULL DEFAULT '0',
  `ParentID` int(11) NOT NULL DEFAULT '0',
  `ConditionFieldID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ClassName` (`ClassName`),
  KEY `ParentID` (`ParentID`),
  KEY `ConditionFieldID` (`ConditionFieldID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.EditableCustomRule: ~0 rows (ongeveer)
DELETE FROM `EditableCustomRule`;
/*!40000 ALTER TABLE `EditableCustomRule` DISABLE KEYS */;
/*!40000 ALTER TABLE `EditableCustomRule` ENABLE KEYS */;


-- Structuur van  tabel vagrant.EditableCustomRule_Live wordt geschreven
CREATE TABLE IF NOT EXISTS `EditableCustomRule_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('SilverStripe\\UserForms\\Model\\EditableCustomRule') CHARACTER SET utf8 DEFAULT 'SilverStripe\\UserForms\\Model\\EditableCustomRule',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Display` enum('Show','Hide') CHARACTER SET utf8 DEFAULT 'Show',
  `ConditionOption` enum('IsBlank','IsNotBlank','HasValue','ValueNot','ValueLessThan','ValueLessThanEqual','ValueGreaterThan','ValueGreaterThanEqual') CHARACTER SET utf8 DEFAULT 'IsBlank',
  `FieldValue` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Version` int(11) NOT NULL DEFAULT '0',
  `ParentID` int(11) NOT NULL DEFAULT '0',
  `ConditionFieldID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ClassName` (`ClassName`),
  KEY `ParentID` (`ParentID`),
  KEY `ConditionFieldID` (`ConditionFieldID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.EditableCustomRule_Live: ~0 rows (ongeveer)
DELETE FROM `EditableCustomRule_Live`;
/*!40000 ALTER TABLE `EditableCustomRule_Live` DISABLE KEYS */;
/*!40000 ALTER TABLE `EditableCustomRule_Live` ENABLE KEYS */;


-- Structuur van  tabel vagrant.EditableCustomRule_Versions wordt geschreven
CREATE TABLE IF NOT EXISTS `EditableCustomRule_Versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `WasPublished` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `AuthorID` int(11) NOT NULL DEFAULT '0',
  `PublisherID` int(11) NOT NULL DEFAULT '0',
  `ClassName` enum('SilverStripe\\UserForms\\Model\\EditableCustomRule') CHARACTER SET utf8 DEFAULT 'SilverStripe\\UserForms\\Model\\EditableCustomRule',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Display` enum('Show','Hide') CHARACTER SET utf8 DEFAULT 'Show',
  `ConditionOption` enum('IsBlank','IsNotBlank','HasValue','ValueNot','ValueLessThan','ValueLessThanEqual','ValueGreaterThan','ValueGreaterThanEqual') CHARACTER SET utf8 DEFAULT 'IsBlank',
  `FieldValue` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `ParentID` int(11) NOT NULL DEFAULT '0',
  `ConditionFieldID` int(11) NOT NULL DEFAULT '0',
  `WasDeleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `WasDraft` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`),
  KEY `AuthorID` (`AuthorID`),
  KEY `PublisherID` (`PublisherID`),
  KEY `ClassName` (`ClassName`),
  KEY `ParentID` (`ParentID`),
  KEY `ConditionFieldID` (`ConditionFieldID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.EditableCustomRule_Versions: ~0 rows (ongeveer)
DELETE FROM `EditableCustomRule_Versions`;
/*!40000 ALTER TABLE `EditableCustomRule_Versions` DISABLE KEYS */;
/*!40000 ALTER TABLE `EditableCustomRule_Versions` ENABLE KEYS */;


-- Structuur van  tabel vagrant.EditableDateField wordt geschreven
CREATE TABLE IF NOT EXISTS `EditableDateField` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DefaultToToday` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.EditableDateField: ~0 rows (ongeveer)
DELETE FROM `EditableDateField`;
/*!40000 ALTER TABLE `EditableDateField` DISABLE KEYS */;
/*!40000 ALTER TABLE `EditableDateField` ENABLE KEYS */;


-- Structuur van  tabel vagrant.EditableDateField_Live wordt geschreven
CREATE TABLE IF NOT EXISTS `EditableDateField_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DefaultToToday` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.EditableDateField_Live: ~0 rows (ongeveer)
DELETE FROM `EditableDateField_Live`;
/*!40000 ALTER TABLE `EditableDateField_Live` DISABLE KEYS */;
/*!40000 ALTER TABLE `EditableDateField_Live` ENABLE KEYS */;


-- Structuur van  tabel vagrant.EditableDateField_Versions wordt geschreven
CREATE TABLE IF NOT EXISTS `EditableDateField_Versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `DefaultToToday` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.EditableDateField_Versions: ~0 rows (ongeveer)
DELETE FROM `EditableDateField_Versions`;
/*!40000 ALTER TABLE `EditableDateField_Versions` DISABLE KEYS */;
/*!40000 ALTER TABLE `EditableDateField_Versions` ENABLE KEYS */;


-- Structuur van  tabel vagrant.EditableDropdown wordt geschreven
CREATE TABLE IF NOT EXISTS `EditableDropdown` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `UseEmptyString` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `EmptyString` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.EditableDropdown: ~0 rows (ongeveer)
DELETE FROM `EditableDropdown`;
/*!40000 ALTER TABLE `EditableDropdown` DISABLE KEYS */;
/*!40000 ALTER TABLE `EditableDropdown` ENABLE KEYS */;


-- Structuur van  tabel vagrant.EditableDropdown_Live wordt geschreven
CREATE TABLE IF NOT EXISTS `EditableDropdown_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `UseEmptyString` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `EmptyString` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.EditableDropdown_Live: ~0 rows (ongeveer)
DELETE FROM `EditableDropdown_Live`;
/*!40000 ALTER TABLE `EditableDropdown_Live` DISABLE KEYS */;
/*!40000 ALTER TABLE `EditableDropdown_Live` ENABLE KEYS */;


-- Structuur van  tabel vagrant.EditableDropdown_Versions wordt geschreven
CREATE TABLE IF NOT EXISTS `EditableDropdown_Versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `UseEmptyString` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `EmptyString` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.EditableDropdown_Versions: ~0 rows (ongeveer)
DELETE FROM `EditableDropdown_Versions`;
/*!40000 ALTER TABLE `EditableDropdown_Versions` DISABLE KEYS */;
/*!40000 ALTER TABLE `EditableDropdown_Versions` ENABLE KEYS */;


-- Structuur van  tabel vagrant.EditableFieldGroup wordt geschreven
CREATE TABLE IF NOT EXISTS `EditableFieldGroup` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `EndID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `EndID` (`EndID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.EditableFieldGroup: ~0 rows (ongeveer)
DELETE FROM `EditableFieldGroup`;
/*!40000 ALTER TABLE `EditableFieldGroup` DISABLE KEYS */;
/*!40000 ALTER TABLE `EditableFieldGroup` ENABLE KEYS */;


-- Structuur van  tabel vagrant.EditableFieldGroup_Live wordt geschreven
CREATE TABLE IF NOT EXISTS `EditableFieldGroup_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `EndID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `EndID` (`EndID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.EditableFieldGroup_Live: ~0 rows (ongeveer)
DELETE FROM `EditableFieldGroup_Live`;
/*!40000 ALTER TABLE `EditableFieldGroup_Live` DISABLE KEYS */;
/*!40000 ALTER TABLE `EditableFieldGroup_Live` ENABLE KEYS */;


-- Structuur van  tabel vagrant.EditableFieldGroup_Versions wordt geschreven
CREATE TABLE IF NOT EXISTS `EditableFieldGroup_Versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `EndID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`),
  KEY `EndID` (`EndID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.EditableFieldGroup_Versions: ~0 rows (ongeveer)
DELETE FROM `EditableFieldGroup_Versions`;
/*!40000 ALTER TABLE `EditableFieldGroup_Versions` DISABLE KEYS */;
/*!40000 ALTER TABLE `EditableFieldGroup_Versions` ENABLE KEYS */;


-- Structuur van  tabel vagrant.EditableFileField wordt geschreven
CREATE TABLE IF NOT EXISTS `EditableFileField` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `MaxFileSizeMB` float NOT NULL DEFAULT '0',
  `FolderID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `FolderID` (`FolderID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.EditableFileField: ~0 rows (ongeveer)
DELETE FROM `EditableFileField`;
/*!40000 ALTER TABLE `EditableFileField` DISABLE KEYS */;
/*!40000 ALTER TABLE `EditableFileField` ENABLE KEYS */;


-- Structuur van  tabel vagrant.EditableFileField_Live wordt geschreven
CREATE TABLE IF NOT EXISTS `EditableFileField_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `MaxFileSizeMB` float NOT NULL DEFAULT '0',
  `FolderID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `FolderID` (`FolderID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.EditableFileField_Live: ~0 rows (ongeveer)
DELETE FROM `EditableFileField_Live`;
/*!40000 ALTER TABLE `EditableFileField_Live` DISABLE KEYS */;
/*!40000 ALTER TABLE `EditableFileField_Live` ENABLE KEYS */;


-- Structuur van  tabel vagrant.EditableFileField_Versions wordt geschreven
CREATE TABLE IF NOT EXISTS `EditableFileField_Versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `MaxFileSizeMB` float NOT NULL DEFAULT '0',
  `FolderID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`),
  KEY `FolderID` (`FolderID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.EditableFileField_Versions: ~0 rows (ongeveer)
DELETE FROM `EditableFileField_Versions`;
/*!40000 ALTER TABLE `EditableFileField_Versions` DISABLE KEYS */;
/*!40000 ALTER TABLE `EditableFileField_Versions` ENABLE KEYS */;


-- Structuur van  tabel vagrant.EditableFormField wordt geschreven
CREATE TABLE IF NOT EXISTS `EditableFormField` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('SilverStripe\\UserForms\\Model\\EditableFormField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableCheckbox','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableCountryDropdownField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableDateField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableEmailField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableFieldGroupEnd','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableFieldGroup','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableFileField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableFormHeading','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableFormStep','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableLiteralField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableMemberListField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableMultipleOptionField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableNumericField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableTextField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableCheckboxGroupField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableDropdown','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableRadioField') CHARACTER SET utf8 DEFAULT 'SilverStripe\\UserForms\\Model\\EditableFormField',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Default` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Sort` int(11) NOT NULL DEFAULT '0',
  `Required` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `CustomErrorMessage` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `ExtraClass` mediumtext CHARACTER SET utf8,
  `RightTitle` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `ShowOnLoad` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `ShowInSummary` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Placeholder` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `DisplayRulesConjunction` enum('And','Or') CHARACTER SET utf8 DEFAULT 'Or',
  `Version` int(11) NOT NULL DEFAULT '0',
  `ParentID` int(11) NOT NULL DEFAULT '0',
  `ParentClass` enum('Hestec\\SisowMethod\\SisowMethod','Hestec\\ElementalExtensions\\Dataobjects\\FaqQuestion','Hestec\\ExactOnline\\ExactOnlineConnection','Hestec\\FaqPage\\FaqCategorie','Hestec\\FaqPage\\FaqQuestion','BrandColor','BrandColorType','ExtraNavigationItem','Hestec\\Tools\\OwnIp','SilverStripe\\Assets\\File','SilverStripe\\SiteConfig\\SiteConfig','SilverStripe\\Versioned\\ChangeSetItem','SilverStripe\\Versioned\\ChangeSet','DNADesign\\Elemental\\Models\\BaseElement','DNADesign\\Elemental\\Models\\ElementalArea','gorriecoe\\Link\\Models\\Link','gorriecoe\\Menu\\Models\\MenuSet','SilverStripe\\Assets\\Shortcodes\\FileLink','SilverStripe\\Blog\\Model\\BlogCategory','SilverStripe\\Blog\\Model\\BlogTag','SilverStripe\\CMS\\Model\\SiteTreeLink','SilverStripe\\CMS\\Model\\SiteTree','SilverStripe\\Security\\Group','SilverStripe\\Security\\LoginAttempt','SilverStripe\\Security\\MemberPassword','SilverStripe\\Security\\Member','SilverStripe\\Security\\Permission','SilverStripe\\Security\\PermissionRoleCode','SilverStripe\\Security\\PermissionRole','SilverStripe\\Security\\RememberLoginHash','SilverStripe\\UserForms\\Model\\EditableCustomRule','SilverStripe\\UserForms\\Model\\EditableFormField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableOption','SilverStripe\\UserForms\\Model\\Recipient\\EmailRecipientCondition','SilverStripe\\UserForms\\Model\\Recipient\\EmailRecipient','SilverStripe\\UserForms\\Model\\Submission\\SubmittedFormField','SilverStripe\\UserForms\\Model\\Submission\\SubmittedForm','SilverStripe\\Assets\\Folder','SilverStripe\\Assets\\Image','Hestec\\ElementalExtensions\\Elements\\ElementContentImage','Hestec\\ElementalExtensions\\Elements\\ElementFaq','Hestec\\ElementalExtensions\\Elements\\ElementVideo','DNADesign\\Elemental\\Models\\ElementContent','gorriecoe\\Menu\\Models\\MenuLink','Page','ElementPage','HomePage','Hestec\\Faqpage\\FaqPage','FirstChildRedirect','SilverStripe\\ErrorPage\\ErrorPage','SilverStripe\\Blog\\Model\\Blog','SilverStripe\\Blog\\Model\\BlogPost','SilverStripe\\CMS\\Model\\RedirectorPage','SilverStripe\\CMS\\Model\\VirtualPage','SilverStripe\\UserForms\\Model\\UserDefinedForm','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableCheckbox','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableCountryDropdownField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableDateField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableEmailField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableFieldGroupEnd','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableFieldGroup','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableFileField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableFormHeading','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableFormStep','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableLiteralField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableMemberListField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableMultipleOptionField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableNumericField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableTextField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableCheckboxGroupField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableDropdown','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableRadioField','SilverStripe\\UserForms\\Model\\Submission\\SubmittedFileField') CHARACTER SET utf8 DEFAULT 'Hestec\\SisowMethod\\SisowMethod',
  PRIMARY KEY (`ID`),
  KEY `Sort` (`Sort`),
  KEY `ClassName` (`ClassName`),
  KEY `Parent` (`ParentID`,`ParentClass`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.EditableFormField: ~0 rows (ongeveer)
DELETE FROM `EditableFormField`;
/*!40000 ALTER TABLE `EditableFormField` DISABLE KEYS */;
/*!40000 ALTER TABLE `EditableFormField` ENABLE KEYS */;


-- Structuur van  tabel vagrant.EditableFormField_Live wordt geschreven
CREATE TABLE IF NOT EXISTS `EditableFormField_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('SilverStripe\\UserForms\\Model\\EditableFormField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableCheckbox','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableCountryDropdownField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableDateField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableEmailField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableFieldGroupEnd','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableFieldGroup','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableFileField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableFormHeading','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableFormStep','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableLiteralField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableMemberListField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableMultipleOptionField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableNumericField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableTextField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableCheckboxGroupField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableDropdown','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableRadioField') CHARACTER SET utf8 DEFAULT 'SilverStripe\\UserForms\\Model\\EditableFormField',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Default` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Sort` int(11) NOT NULL DEFAULT '0',
  `Required` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `CustomErrorMessage` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `ExtraClass` mediumtext CHARACTER SET utf8,
  `RightTitle` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `ShowOnLoad` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `ShowInSummary` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Placeholder` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `DisplayRulesConjunction` enum('And','Or') CHARACTER SET utf8 DEFAULT 'Or',
  `Version` int(11) NOT NULL DEFAULT '0',
  `ParentID` int(11) NOT NULL DEFAULT '0',
  `ParentClass` enum('Hestec\\SisowMethod\\SisowMethod','Hestec\\ElementalExtensions\\Dataobjects\\FaqQuestion','Hestec\\ExactOnline\\ExactOnlineConnection','Hestec\\FaqPage\\FaqCategorie','Hestec\\FaqPage\\FaqQuestion','BrandColor','BrandColorType','ExtraNavigationItem','Hestec\\Tools\\OwnIp','SilverStripe\\Assets\\File','SilverStripe\\SiteConfig\\SiteConfig','SilverStripe\\Versioned\\ChangeSetItem','SilverStripe\\Versioned\\ChangeSet','DNADesign\\Elemental\\Models\\BaseElement','DNADesign\\Elemental\\Models\\ElementalArea','gorriecoe\\Link\\Models\\Link','gorriecoe\\Menu\\Models\\MenuSet','SilverStripe\\Assets\\Shortcodes\\FileLink','SilverStripe\\Blog\\Model\\BlogCategory','SilverStripe\\Blog\\Model\\BlogTag','SilverStripe\\CMS\\Model\\SiteTreeLink','SilverStripe\\CMS\\Model\\SiteTree','SilverStripe\\Security\\Group','SilverStripe\\Security\\LoginAttempt','SilverStripe\\Security\\MemberPassword','SilverStripe\\Security\\Member','SilverStripe\\Security\\Permission','SilverStripe\\Security\\PermissionRoleCode','SilverStripe\\Security\\PermissionRole','SilverStripe\\Security\\RememberLoginHash','SilverStripe\\UserForms\\Model\\EditableCustomRule','SilverStripe\\UserForms\\Model\\EditableFormField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableOption','SilverStripe\\UserForms\\Model\\Recipient\\EmailRecipientCondition','SilverStripe\\UserForms\\Model\\Recipient\\EmailRecipient','SilverStripe\\UserForms\\Model\\Submission\\SubmittedFormField','SilverStripe\\UserForms\\Model\\Submission\\SubmittedForm','SilverStripe\\Assets\\Folder','SilverStripe\\Assets\\Image','Hestec\\ElementalExtensions\\Elements\\ElementContentImage','Hestec\\ElementalExtensions\\Elements\\ElementFaq','Hestec\\ElementalExtensions\\Elements\\ElementVideo','DNADesign\\Elemental\\Models\\ElementContent','gorriecoe\\Menu\\Models\\MenuLink','Page','ElementPage','HomePage','Hestec\\Faqpage\\FaqPage','FirstChildRedirect','SilverStripe\\ErrorPage\\ErrorPage','SilverStripe\\Blog\\Model\\Blog','SilverStripe\\Blog\\Model\\BlogPost','SilverStripe\\CMS\\Model\\RedirectorPage','SilverStripe\\CMS\\Model\\VirtualPage','SilverStripe\\UserForms\\Model\\UserDefinedForm','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableCheckbox','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableCountryDropdownField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableDateField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableEmailField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableFieldGroupEnd','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableFieldGroup','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableFileField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableFormHeading','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableFormStep','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableLiteralField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableMemberListField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableMultipleOptionField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableNumericField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableTextField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableCheckboxGroupField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableDropdown','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableRadioField','SilverStripe\\UserForms\\Model\\Submission\\SubmittedFileField') CHARACTER SET utf8 DEFAULT 'Hestec\\SisowMethod\\SisowMethod',
  PRIMARY KEY (`ID`),
  KEY `Sort` (`Sort`),
  KEY `ClassName` (`ClassName`),
  KEY `Parent` (`ParentID`,`ParentClass`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.EditableFormField_Live: ~0 rows (ongeveer)
DELETE FROM `EditableFormField_Live`;
/*!40000 ALTER TABLE `EditableFormField_Live` DISABLE KEYS */;
/*!40000 ALTER TABLE `EditableFormField_Live` ENABLE KEYS */;


-- Structuur van  tabel vagrant.EditableFormField_Versions wordt geschreven
CREATE TABLE IF NOT EXISTS `EditableFormField_Versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `WasPublished` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `AuthorID` int(11) NOT NULL DEFAULT '0',
  `PublisherID` int(11) NOT NULL DEFAULT '0',
  `ClassName` enum('SilverStripe\\UserForms\\Model\\EditableFormField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableCheckbox','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableCountryDropdownField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableDateField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableEmailField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableFieldGroupEnd','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableFieldGroup','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableFileField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableFormHeading','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableFormStep','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableLiteralField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableMemberListField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableMultipleOptionField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableNumericField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableTextField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableCheckboxGroupField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableDropdown','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableRadioField') CHARACTER SET utf8 DEFAULT 'SilverStripe\\UserForms\\Model\\EditableFormField',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Default` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Sort` int(11) NOT NULL DEFAULT '0',
  `Required` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `CustomErrorMessage` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `ExtraClass` mediumtext CHARACTER SET utf8,
  `RightTitle` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `ShowOnLoad` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `ShowInSummary` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Placeholder` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `DisplayRulesConjunction` enum('And','Or') CHARACTER SET utf8 DEFAULT 'Or',
  `ParentID` int(11) NOT NULL DEFAULT '0',
  `ParentClass` enum('Hestec\\SisowMethod\\SisowMethod','Hestec\\ElementalExtensions\\Dataobjects\\FaqQuestion','Hestec\\ExactOnline\\ExactOnlineConnection','Hestec\\FaqPage\\FaqCategorie','Hestec\\FaqPage\\FaqQuestion','BrandColor','BrandColorType','ExtraNavigationItem','Hestec\\Tools\\OwnIp','SilverStripe\\Assets\\File','SilverStripe\\SiteConfig\\SiteConfig','SilverStripe\\Versioned\\ChangeSetItem','SilverStripe\\Versioned\\ChangeSet','DNADesign\\Elemental\\Models\\BaseElement','DNADesign\\Elemental\\Models\\ElementalArea','gorriecoe\\Link\\Models\\Link','gorriecoe\\Menu\\Models\\MenuSet','SilverStripe\\Assets\\Shortcodes\\FileLink','SilverStripe\\Blog\\Model\\BlogCategory','SilverStripe\\Blog\\Model\\BlogTag','SilverStripe\\CMS\\Model\\SiteTreeLink','SilverStripe\\CMS\\Model\\SiteTree','SilverStripe\\Security\\Group','SilverStripe\\Security\\LoginAttempt','SilverStripe\\Security\\MemberPassword','SilverStripe\\Security\\Member','SilverStripe\\Security\\Permission','SilverStripe\\Security\\PermissionRoleCode','SilverStripe\\Security\\PermissionRole','SilverStripe\\Security\\RememberLoginHash','SilverStripe\\UserForms\\Model\\EditableCustomRule','SilverStripe\\UserForms\\Model\\EditableFormField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableOption','SilverStripe\\UserForms\\Model\\Recipient\\EmailRecipientCondition','SilverStripe\\UserForms\\Model\\Recipient\\EmailRecipient','SilverStripe\\UserForms\\Model\\Submission\\SubmittedFormField','SilverStripe\\UserForms\\Model\\Submission\\SubmittedForm','SilverStripe\\Assets\\Folder','SilverStripe\\Assets\\Image','Hestec\\ElementalExtensions\\Elements\\ElementContentImage','Hestec\\ElementalExtensions\\Elements\\ElementFaq','Hestec\\ElementalExtensions\\Elements\\ElementVideo','DNADesign\\Elemental\\Models\\ElementContent','gorriecoe\\Menu\\Models\\MenuLink','Page','ElementPage','HomePage','Hestec\\Faqpage\\FaqPage','FirstChildRedirect','SilverStripe\\ErrorPage\\ErrorPage','SilverStripe\\Blog\\Model\\Blog','SilverStripe\\Blog\\Model\\BlogPost','SilverStripe\\CMS\\Model\\RedirectorPage','SilverStripe\\CMS\\Model\\VirtualPage','SilverStripe\\UserForms\\Model\\UserDefinedForm','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableCheckbox','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableCountryDropdownField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableDateField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableEmailField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableFieldGroupEnd','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableFieldGroup','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableFileField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableFormHeading','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableFormStep','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableLiteralField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableMemberListField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableMultipleOptionField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableNumericField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableTextField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableCheckboxGroupField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableDropdown','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableRadioField','SilverStripe\\UserForms\\Model\\Submission\\SubmittedFileField') CHARACTER SET utf8 DEFAULT 'Hestec\\SisowMethod\\SisowMethod',
  `WasDeleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `WasDraft` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`),
  KEY `AuthorID` (`AuthorID`),
  KEY `PublisherID` (`PublisherID`),
  KEY `Sort` (`Sort`),
  KEY `ClassName` (`ClassName`),
  KEY `Parent` (`ParentID`,`ParentClass`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.EditableFormField_Versions: ~0 rows (ongeveer)
DELETE FROM `EditableFormField_Versions`;
/*!40000 ALTER TABLE `EditableFormField_Versions` DISABLE KEYS */;
/*!40000 ALTER TABLE `EditableFormField_Versions` ENABLE KEYS */;


-- Structuur van  tabel vagrant.EditableFormHeading wordt geschreven
CREATE TABLE IF NOT EXISTS `EditableFormHeading` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Level` int(11) NOT NULL DEFAULT '3',
  `HideFromReports` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.EditableFormHeading: ~0 rows (ongeveer)
DELETE FROM `EditableFormHeading`;
/*!40000 ALTER TABLE `EditableFormHeading` DISABLE KEYS */;
/*!40000 ALTER TABLE `EditableFormHeading` ENABLE KEYS */;


-- Structuur van  tabel vagrant.EditableFormHeading_Live wordt geschreven
CREATE TABLE IF NOT EXISTS `EditableFormHeading_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Level` int(11) NOT NULL DEFAULT '3',
  `HideFromReports` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.EditableFormHeading_Live: ~0 rows (ongeveer)
DELETE FROM `EditableFormHeading_Live`;
/*!40000 ALTER TABLE `EditableFormHeading_Live` DISABLE KEYS */;
/*!40000 ALTER TABLE `EditableFormHeading_Live` ENABLE KEYS */;


-- Structuur van  tabel vagrant.EditableFormHeading_Versions wordt geschreven
CREATE TABLE IF NOT EXISTS `EditableFormHeading_Versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `Level` int(11) NOT NULL DEFAULT '3',
  `HideFromReports` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.EditableFormHeading_Versions: ~0 rows (ongeveer)
DELETE FROM `EditableFormHeading_Versions`;
/*!40000 ALTER TABLE `EditableFormHeading_Versions` DISABLE KEYS */;
/*!40000 ALTER TABLE `EditableFormHeading_Versions` ENABLE KEYS */;


-- Structuur van  tabel vagrant.EditableLiteralField wordt geschreven
CREATE TABLE IF NOT EXISTS `EditableLiteralField` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Content` mediumtext CHARACTER SET utf8,
  `HideFromReports` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `HideLabel` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.EditableLiteralField: ~0 rows (ongeveer)
DELETE FROM `EditableLiteralField`;
/*!40000 ALTER TABLE `EditableLiteralField` DISABLE KEYS */;
/*!40000 ALTER TABLE `EditableLiteralField` ENABLE KEYS */;


-- Structuur van  tabel vagrant.EditableLiteralField_Live wordt geschreven
CREATE TABLE IF NOT EXISTS `EditableLiteralField_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Content` mediumtext CHARACTER SET utf8,
  `HideFromReports` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `HideLabel` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.EditableLiteralField_Live: ~0 rows (ongeveer)
DELETE FROM `EditableLiteralField_Live`;
/*!40000 ALTER TABLE `EditableLiteralField_Live` DISABLE KEYS */;
/*!40000 ALTER TABLE `EditableLiteralField_Live` ENABLE KEYS */;


-- Structuur van  tabel vagrant.EditableLiteralField_Versions wordt geschreven
CREATE TABLE IF NOT EXISTS `EditableLiteralField_Versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `Content` mediumtext CHARACTER SET utf8,
  `HideFromReports` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `HideLabel` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.EditableLiteralField_Versions: ~0 rows (ongeveer)
DELETE FROM `EditableLiteralField_Versions`;
/*!40000 ALTER TABLE `EditableLiteralField_Versions` DISABLE KEYS */;
/*!40000 ALTER TABLE `EditableLiteralField_Versions` ENABLE KEYS */;


-- Structuur van  tabel vagrant.EditableMemberListField wordt geschreven
CREATE TABLE IF NOT EXISTS `EditableMemberListField` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `GroupID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `GroupID` (`GroupID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.EditableMemberListField: ~0 rows (ongeveer)
DELETE FROM `EditableMemberListField`;
/*!40000 ALTER TABLE `EditableMemberListField` DISABLE KEYS */;
/*!40000 ALTER TABLE `EditableMemberListField` ENABLE KEYS */;


-- Structuur van  tabel vagrant.EditableMemberListField_Live wordt geschreven
CREATE TABLE IF NOT EXISTS `EditableMemberListField_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `GroupID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `GroupID` (`GroupID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.EditableMemberListField_Live: ~0 rows (ongeveer)
DELETE FROM `EditableMemberListField_Live`;
/*!40000 ALTER TABLE `EditableMemberListField_Live` DISABLE KEYS */;
/*!40000 ALTER TABLE `EditableMemberListField_Live` ENABLE KEYS */;


-- Structuur van  tabel vagrant.EditableMemberListField_Versions wordt geschreven
CREATE TABLE IF NOT EXISTS `EditableMemberListField_Versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `GroupID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`),
  KEY `GroupID` (`GroupID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.EditableMemberListField_Versions: ~0 rows (ongeveer)
DELETE FROM `EditableMemberListField_Versions`;
/*!40000 ALTER TABLE `EditableMemberListField_Versions` DISABLE KEYS */;
/*!40000 ALTER TABLE `EditableMemberListField_Versions` ENABLE KEYS */;


-- Structuur van  tabel vagrant.EditableNumericField wordt geschreven
CREATE TABLE IF NOT EXISTS `EditableNumericField` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `MinValue` int(11) NOT NULL DEFAULT '0',
  `MaxValue` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.EditableNumericField: ~0 rows (ongeveer)
DELETE FROM `EditableNumericField`;
/*!40000 ALTER TABLE `EditableNumericField` DISABLE KEYS */;
/*!40000 ALTER TABLE `EditableNumericField` ENABLE KEYS */;


-- Structuur van  tabel vagrant.EditableNumericField_Live wordt geschreven
CREATE TABLE IF NOT EXISTS `EditableNumericField_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `MinValue` int(11) NOT NULL DEFAULT '0',
  `MaxValue` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.EditableNumericField_Live: ~0 rows (ongeveer)
DELETE FROM `EditableNumericField_Live`;
/*!40000 ALTER TABLE `EditableNumericField_Live` DISABLE KEYS */;
/*!40000 ALTER TABLE `EditableNumericField_Live` ENABLE KEYS */;


-- Structuur van  tabel vagrant.EditableNumericField_Versions wordt geschreven
CREATE TABLE IF NOT EXISTS `EditableNumericField_Versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `MinValue` int(11) NOT NULL DEFAULT '0',
  `MaxValue` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.EditableNumericField_Versions: ~0 rows (ongeveer)
DELETE FROM `EditableNumericField_Versions`;
/*!40000 ALTER TABLE `EditableNumericField_Versions` DISABLE KEYS */;
/*!40000 ALTER TABLE `EditableNumericField_Versions` ENABLE KEYS */;


-- Structuur van  tabel vagrant.EditableOption wordt geschreven
CREATE TABLE IF NOT EXISTS `EditableOption` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('SilverStripe\\UserForms\\Model\\EditableFormField\\EditableOption') CHARACTER SET utf8 DEFAULT 'SilverStripe\\UserForms\\Model\\EditableFormField\\EditableOption',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Default` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Sort` int(11) NOT NULL DEFAULT '0',
  `Value` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Version` int(11) NOT NULL DEFAULT '0',
  `ParentID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `Sort` (`Sort`),
  KEY `ClassName` (`ClassName`),
  KEY `ParentID` (`ParentID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.EditableOption: ~0 rows (ongeveer)
DELETE FROM `EditableOption`;
/*!40000 ALTER TABLE `EditableOption` DISABLE KEYS */;
/*!40000 ALTER TABLE `EditableOption` ENABLE KEYS */;


-- Structuur van  tabel vagrant.EditableOption_Live wordt geschreven
CREATE TABLE IF NOT EXISTS `EditableOption_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('SilverStripe\\UserForms\\Model\\EditableFormField\\EditableOption') CHARACTER SET utf8 DEFAULT 'SilverStripe\\UserForms\\Model\\EditableFormField\\EditableOption',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Default` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Sort` int(11) NOT NULL DEFAULT '0',
  `Value` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Version` int(11) NOT NULL DEFAULT '0',
  `ParentID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `Sort` (`Sort`),
  KEY `ClassName` (`ClassName`),
  KEY `ParentID` (`ParentID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.EditableOption_Live: ~0 rows (ongeveer)
DELETE FROM `EditableOption_Live`;
/*!40000 ALTER TABLE `EditableOption_Live` DISABLE KEYS */;
/*!40000 ALTER TABLE `EditableOption_Live` ENABLE KEYS */;


-- Structuur van  tabel vagrant.EditableOption_Versions wordt geschreven
CREATE TABLE IF NOT EXISTS `EditableOption_Versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `WasPublished` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `AuthorID` int(11) NOT NULL DEFAULT '0',
  `PublisherID` int(11) NOT NULL DEFAULT '0',
  `ClassName` enum('SilverStripe\\UserForms\\Model\\EditableFormField\\EditableOption') CHARACTER SET utf8 DEFAULT 'SilverStripe\\UserForms\\Model\\EditableFormField\\EditableOption',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Default` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Sort` int(11) NOT NULL DEFAULT '0',
  `Value` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `ParentID` int(11) NOT NULL DEFAULT '0',
  `WasDeleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `WasDraft` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`),
  KEY `AuthorID` (`AuthorID`),
  KEY `PublisherID` (`PublisherID`),
  KEY `Sort` (`Sort`),
  KEY `ClassName` (`ClassName`),
  KEY `ParentID` (`ParentID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.EditableOption_Versions: ~0 rows (ongeveer)
DELETE FROM `EditableOption_Versions`;
/*!40000 ALTER TABLE `EditableOption_Versions` DISABLE KEYS */;
/*!40000 ALTER TABLE `EditableOption_Versions` ENABLE KEYS */;


-- Structuur van  tabel vagrant.EditableTextField wordt geschreven
CREATE TABLE IF NOT EXISTS `EditableTextField` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `MinLength` int(11) NOT NULL DEFAULT '0',
  `MaxLength` int(11) NOT NULL DEFAULT '0',
  `Rows` int(11) NOT NULL DEFAULT '1',
  `Autocomplete` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.EditableTextField: ~0 rows (ongeveer)
DELETE FROM `EditableTextField`;
/*!40000 ALTER TABLE `EditableTextField` DISABLE KEYS */;
/*!40000 ALTER TABLE `EditableTextField` ENABLE KEYS */;


-- Structuur van  tabel vagrant.EditableTextField_Live wordt geschreven
CREATE TABLE IF NOT EXISTS `EditableTextField_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `MinLength` int(11) NOT NULL DEFAULT '0',
  `MaxLength` int(11) NOT NULL DEFAULT '0',
  `Rows` int(11) NOT NULL DEFAULT '1',
  `Autocomplete` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.EditableTextField_Live: ~0 rows (ongeveer)
DELETE FROM `EditableTextField_Live`;
/*!40000 ALTER TABLE `EditableTextField_Live` DISABLE KEYS */;
/*!40000 ALTER TABLE `EditableTextField_Live` ENABLE KEYS */;


-- Structuur van  tabel vagrant.EditableTextField_Versions wordt geschreven
CREATE TABLE IF NOT EXISTS `EditableTextField_Versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `MinLength` int(11) NOT NULL DEFAULT '0',
  `MaxLength` int(11) NOT NULL DEFAULT '0',
  `Rows` int(11) NOT NULL DEFAULT '1',
  `Autocomplete` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.EditableTextField_Versions: ~0 rows (ongeveer)
DELETE FROM `EditableTextField_Versions`;
/*!40000 ALTER TABLE `EditableTextField_Versions` DISABLE KEYS */;
/*!40000 ALTER TABLE `EditableTextField_Versions` ENABLE KEYS */;


-- Structuur van  tabel vagrant.Element wordt geschreven
CREATE TABLE IF NOT EXISTS `Element` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('DNADesign\\Elemental\\Models\\BaseElement','Hestec\\ElementalExtensions\\Elements\\ElementContentImage','Hestec\\ElementalExtensions\\Elements\\ElementFaq','Hestec\\ElementalExtensions\\Elements\\ElementVideo','DNADesign\\Elemental\\Models\\ElementContent') CHARACTER SET utf8 DEFAULT 'DNADesign\\Elemental\\Models\\BaseElement',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `ShowTitle` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Sort` int(11) NOT NULL DEFAULT '0',
  `ExtraClass` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Style` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Version` int(11) NOT NULL DEFAULT '0',
  `ParentID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `Sort` (`Sort`),
  KEY `ClassName` (`ClassName`),
  KEY `ParentID` (`ParentID`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.Element: ~4 rows (ongeveer)
DELETE FROM `Element`;
/*!40000 ALTER TABLE `Element` DISABLE KEYS */;
INSERT INTO `Element` (`ID`, `ClassName`, `LastEdited`, `Created`, `Title`, `ShowTitle`, `Sort`, `ExtraClass`, `Style`, `Version`, `ParentID`) VALUES
	(2, 'DNADesign\\Elemental\\Models\\ElementContent', '2018-03-31 16:16:17', '2018-03-29 20:46:08', 'Dit is een faketekst', 0, 2, NULL, NULL, 5, 2),
	(5, 'Hestec\\ElementalExtensions\\Elements\\ElementVideo', '2018-07-10 14:42:17', '2018-03-31 14:15:28', 'Video', 0, 1, NULL, NULL, 5, 2),
	(7, 'Hestec\\ElementalExtensions\\Elements\\ElementFaq', '2018-07-13 16:28:04', '2018-07-10 16:34:43', 'Veelgestelde vragen', 1, 1, NULL, NULL, 11, 2),
	(8, 'Hestec\\ElementalExtensions\\Elements\\ElementContentImage', '2018-08-03 11:13:15', '2018-08-02 17:43:53', 'Afbeelding links of rechts?', 1, 1, NULL, NULL, 22, 2);
/*!40000 ALTER TABLE `Element` ENABLE KEYS */;


-- Structuur van  tabel vagrant.ElementalArea wordt geschreven
CREATE TABLE IF NOT EXISTS `ElementalArea` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('DNADesign\\Elemental\\Models\\ElementalArea') CHARACTER SET utf8 DEFAULT 'DNADesign\\Elemental\\Models\\ElementalArea',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `OwnerClassName` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.ElementalArea: ~4 rows (ongeveer)
DELETE FROM `ElementalArea`;
/*!40000 ALTER TABLE `ElementalArea` DISABLE KEYS */;
INSERT INTO `ElementalArea` (`ID`, `ClassName`, `LastEdited`, `Created`, `OwnerClassName`, `Version`) VALUES
	(1, 'DNADesign\\Elemental\\Models\\ElementalArea', '2018-03-26 22:28:55', '2018-03-26 22:27:42', 'Page', 1),
	(2, 'DNADesign\\Elemental\\Models\\ElementalArea', '2018-03-29 20:45:36', '2018-03-29 20:45:22', 'ElementPage', 1),
	(3, 'DNADesign\\Elemental\\Models\\ElementalArea', '2018-03-29 20:45:36', '2018-03-29 20:45:22', 'ElementPage', 1),
	(4, 'DNADesign\\Elemental\\Models\\ElementalArea', '2018-03-31 14:13:54', '2018-03-31 14:10:14', 'ElementPage', 1);
/*!40000 ALTER TABLE `ElementalArea` ENABLE KEYS */;


-- Structuur van  tabel vagrant.ElementalArea_Live wordt geschreven
CREATE TABLE IF NOT EXISTS `ElementalArea_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('DNADesign\\Elemental\\Models\\ElementalArea') CHARACTER SET utf8 DEFAULT 'DNADesign\\Elemental\\Models\\ElementalArea',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `OwnerClassName` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.ElementalArea_Live: ~4 rows (ongeveer)
DELETE FROM `ElementalArea_Live`;
/*!40000 ALTER TABLE `ElementalArea_Live` DISABLE KEYS */;
INSERT INTO `ElementalArea_Live` (`ID`, `ClassName`, `LastEdited`, `Created`, `OwnerClassName`, `Version`) VALUES
	(1, 'DNADesign\\Elemental\\Models\\ElementalArea', '2018-03-26 22:28:55', '2018-03-26 22:27:42', 'Page', 1),
	(2, 'DNADesign\\Elemental\\Models\\ElementalArea', '2018-03-29 20:45:36', '2018-03-29 20:45:22', 'ElementPage', 1),
	(3, 'DNADesign\\Elemental\\Models\\ElementalArea', '2018-03-29 20:45:36', '2018-03-29 20:45:22', 'ElementPage', 1),
	(4, 'DNADesign\\Elemental\\Models\\ElementalArea', '2018-03-31 14:13:54', '2018-03-31 14:10:14', 'ElementPage', 1);
/*!40000 ALTER TABLE `ElementalArea_Live` ENABLE KEYS */;


-- Structuur van  tabel vagrant.ElementalArea_Versions wordt geschreven
CREATE TABLE IF NOT EXISTS `ElementalArea_Versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `WasPublished` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `AuthorID` int(11) NOT NULL DEFAULT '0',
  `PublisherID` int(11) NOT NULL DEFAULT '0',
  `ClassName` enum('DNADesign\\Elemental\\Models\\ElementalArea') CHARACTER SET utf8 DEFAULT 'DNADesign\\Elemental\\Models\\ElementalArea',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `OwnerClassName` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `WasDeleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `WasDraft` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`),
  KEY `AuthorID` (`AuthorID`),
  KEY `PublisherID` (`PublisherID`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.ElementalArea_Versions: ~4 rows (ongeveer)
DELETE FROM `ElementalArea_Versions`;
/*!40000 ALTER TABLE `ElementalArea_Versions` DISABLE KEYS */;
INSERT INTO `ElementalArea_Versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `LastEdited`, `Created`, `OwnerClassName`, `WasDeleted`, `WasDraft`) VALUES
	(1, 1, 1, 1, 1, 1, 'DNADesign\\Elemental\\Models\\ElementalArea', '2018-03-26 22:27:42', '2018-03-26 22:27:42', NULL, 0, 0),
	(2, 2, 1, 1, 1, 1, 'DNADesign\\Elemental\\Models\\ElementalArea', '2018-03-29 20:45:22', '2018-03-29 20:45:22', 'ElementPage', 0, 0),
	(3, 3, 1, 1, 1, 1, 'DNADesign\\Elemental\\Models\\ElementalArea', '2018-03-29 20:45:22', '2018-03-29 20:45:22', 'ElementPage', 0, 0),
	(4, 4, 1, 1, 1, 1, 'DNADesign\\Elemental\\Models\\ElementalArea', '2018-03-31 14:10:14', '2018-03-31 14:10:14', NULL, 0, 0);
/*!40000 ALTER TABLE `ElementalArea_Versions` ENABLE KEYS */;


-- Structuur van  tabel vagrant.ElementContent wordt geschreven
CREATE TABLE IF NOT EXISTS `ElementContent` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `HTML` mediumtext CHARACTER SET utf8,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.ElementContent: ~0 rows (ongeveer)
DELETE FROM `ElementContent`;
/*!40000 ALTER TABLE `ElementContent` DISABLE KEYS */;
INSERT INTO `ElementContent` (`ID`, `HTML`) VALUES
	(2, '<p>Dit is een faketekst. Alles wat hier staat is slechts om een indruk te geven van het grafische effect van tekst op deze plek. Wat u hier leest is een voorbeeldtekst. Deze wordt later vervangen door de uiteindelijke tekst, die nu nog niet bekend is. De faketekst is dus een tekst die eigenlijk nergens over gaat. Het grappige is, dat mensen deze toch vaak lezen. Zelfs als men weet dat het om een faketekst gaat, lezen ze toch door.</p>');
/*!40000 ALTER TABLE `ElementContent` ENABLE KEYS */;


-- Structuur van  tabel vagrant.ElementContent_Live wordt geschreven
CREATE TABLE IF NOT EXISTS `ElementContent_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `HTML` mediumtext CHARACTER SET utf8,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.ElementContent_Live: ~0 rows (ongeveer)
DELETE FROM `ElementContent_Live`;
/*!40000 ALTER TABLE `ElementContent_Live` DISABLE KEYS */;
INSERT INTO `ElementContent_Live` (`ID`, `HTML`) VALUES
	(2, '<p>Dit is een faketekst. Alles wat hier staat is slechts om een indruk te geven van het grafische effect van tekst op deze plek. Wat u hier leest is een voorbeeldtekst. Deze wordt later vervangen door de uiteindelijke tekst, die nu nog niet bekend is. De faketekst is dus een tekst die eigenlijk nergens over gaat. Het grappige is, dat mensen deze toch vaak lezen. Zelfs als men weet dat het om een faketekst gaat, lezen ze toch door.</p>');
/*!40000 ALTER TABLE `ElementContent_Live` ENABLE KEYS */;


-- Structuur van  tabel vagrant.ElementContent_Versions wordt geschreven
CREATE TABLE IF NOT EXISTS `ElementContent_Versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `HTML` mediumtext CHARACTER SET utf8,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.ElementContent_Versions: ~5 rows (ongeveer)
DELETE FROM `ElementContent_Versions`;
/*!40000 ALTER TABLE `ElementContent_Versions` DISABLE KEYS */;
INSERT INTO `ElementContent_Versions` (`ID`, `RecordID`, `Version`, `HTML`) VALUES
	(1, 2, 1, '<p>Dit is een faketekst. Alles wat hier staat is slechts om een indruk te geven van het grafische effect van tekst op deze plek. Wat u hier leest is een voorbeeldtekst. Deze wordt later vervangen door de uiteindelijke tekst, die nu nog niet bekend is. De faketekst is dus een tekst die eigenlijk nergens over gaat. Het grappige is, dat mensen deze toch vaak lezen. Zelfs als men weet dat het om een faketekst gaat, lezen ze toch door.</p>'),
	(2, 2, 2, '<p>Dit is een faketekst. Alles wat hier staat is slechts om een indruk te geven van het grafische effect van tekst op deze plek. Wat u hier leest is een voorbeeldtekst. Deze wordt later vervangen door de uiteindelijke tekst, die nu nog niet bekend is. De faketekst is dus een tekst die eigenlijk nergens over gaat. Het grappige is, dat mensen deze toch vaak lezen. Zelfs als men weet dat het om een faketekst gaat, lezen ze toch door.</p>'),
	(3, 2, 3, '<p>Dit is een faketekst. Alles wat hier staat is slechts om een indruk te geven van het grafische effect van tekst op deze plek. Wat u hier leest is een voorbeeldtekst. Deze wordt later vervangen door de uiteindelijke tekst, die nu nog niet bekend is. De faketekst is dus een tekst die eigenlijk nergens over gaat. Het grappige is, dat mensen deze toch vaak lezen. Zelfs als men weet dat het om een faketekst gaat, lezen ze toch door.</p>'),
	(4, 2, 4, '<p>Dit is een faketekst. Alles wat hier staat is slechts om een indruk te geven van het grafische effect van tekst op deze plek. Wat u hier leest is een voorbeeldtekst. Deze wordt later vervangen door de uiteindelijke tekst, die nu nog niet bekend is. De faketekst is dus een tekst die eigenlijk nergens over gaat. Het grappige is, dat mensen deze toch vaak lezen. Zelfs als men weet dat het om een faketekst gaat, lezen ze toch door.</p>'),
	(5, 2, 5, '<p>Dit is een faketekst. Alles wat hier staat is slechts om een indruk te geven van het grafische effect van tekst op deze plek. Wat u hier leest is een voorbeeldtekst. Deze wordt later vervangen door de uiteindelijke tekst, die nu nog niet bekend is. De faketekst is dus een tekst die eigenlijk nergens over gaat. Het grappige is, dat mensen deze toch vaak lezen. Zelfs als men weet dat het om een faketekst gaat, lezen ze toch door.</p>');
/*!40000 ALTER TABLE `ElementContent_Versions` ENABLE KEYS */;


-- Structuur van  tabel vagrant.ElementList wordt geschreven
CREATE TABLE IF NOT EXISTS `ElementList` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ElementsID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ElementsID` (`ElementsID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.ElementList: ~0 rows (ongeveer)
DELETE FROM `ElementList`;
/*!40000 ALTER TABLE `ElementList` DISABLE KEYS */;
/*!40000 ALTER TABLE `ElementList` ENABLE KEYS */;


-- Structuur van  tabel vagrant.ElementList_Live wordt geschreven
CREATE TABLE IF NOT EXISTS `ElementList_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ElementsID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ElementsID` (`ElementsID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.ElementList_Live: ~0 rows (ongeveer)
DELETE FROM `ElementList_Live`;
/*!40000 ALTER TABLE `ElementList_Live` DISABLE KEYS */;
/*!40000 ALTER TABLE `ElementList_Live` ENABLE KEYS */;


-- Structuur van  tabel vagrant.ElementList_Versions wordt geschreven
CREATE TABLE IF NOT EXISTS `ElementList_Versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `ElementsID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`),
  KEY `ElementsID` (`ElementsID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.ElementList_Versions: ~0 rows (ongeveer)
DELETE FROM `ElementList_Versions`;
/*!40000 ALTER TABLE `ElementList_Versions` DISABLE KEYS */;
/*!40000 ALTER TABLE `ElementList_Versions` ENABLE KEYS */;


-- Structuur van  tabel vagrant.ElementPage wordt geschreven
CREATE TABLE IF NOT EXISTS `ElementPage` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ContentAreaID` int(11) NOT NULL DEFAULT '0',
  `PanelAreaID` int(11) NOT NULL DEFAULT '0',
  `ElementalAreaID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ContentAreaID` (`ContentAreaID`),
  KEY `PanelAreaID` (`PanelAreaID`),
  KEY `ElementalAreaID` (`ElementalAreaID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.ElementPage: ~0 rows (ongeveer)
DELETE FROM `ElementPage`;
/*!40000 ALTER TABLE `ElementPage` DISABLE KEYS */;
INSERT INTO `ElementPage` (`ID`, `ContentAreaID`, `PanelAreaID`, `ElementalAreaID`) VALUES
	(6, 2, 3, 4);
/*!40000 ALTER TABLE `ElementPage` ENABLE KEYS */;


-- Structuur van  tabel vagrant.ElementPage_Live wordt geschreven
CREATE TABLE IF NOT EXISTS `ElementPage_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ContentAreaID` int(11) NOT NULL DEFAULT '0',
  `PanelAreaID` int(11) NOT NULL DEFAULT '0',
  `ElementalAreaID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ContentAreaID` (`ContentAreaID`),
  KEY `PanelAreaID` (`PanelAreaID`),
  KEY `ElementalAreaID` (`ElementalAreaID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.ElementPage_Live: ~0 rows (ongeveer)
DELETE FROM `ElementPage_Live`;
/*!40000 ALTER TABLE `ElementPage_Live` DISABLE KEYS */;
INSERT INTO `ElementPage_Live` (`ID`, `ContentAreaID`, `PanelAreaID`, `ElementalAreaID`) VALUES
	(6, 2, 3, 4);
/*!40000 ALTER TABLE `ElementPage_Live` ENABLE KEYS */;


-- Structuur van  tabel vagrant.ElementPage_Versions wordt geschreven
CREATE TABLE IF NOT EXISTS `ElementPage_Versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `ContentAreaID` int(11) NOT NULL DEFAULT '0',
  `PanelAreaID` int(11) NOT NULL DEFAULT '0',
  `ElementalAreaID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`),
  KEY `ContentAreaID` (`ContentAreaID`),
  KEY `PanelAreaID` (`PanelAreaID`),
  KEY `ElementalAreaID` (`ElementalAreaID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.ElementPage_Versions: ~5 rows (ongeveer)
DELETE FROM `ElementPage_Versions`;
/*!40000 ALTER TABLE `ElementPage_Versions` DISABLE KEYS */;
INSERT INTO `ElementPage_Versions` (`ID`, `RecordID`, `Version`, `ContentAreaID`, `PanelAreaID`, `ElementalAreaID`) VALUES
	(1, 6, 1, 2, 3, 0),
	(2, 6, 2, 2, 3, 0),
	(3, 6, 3, 2, 3, 4),
	(4, 6, 4, 2, 3, 4),
	(5, 6, 5, 2, 3, 4);
/*!40000 ALTER TABLE `ElementPage_Versions` ENABLE KEYS */;


-- Structuur van  tabel vagrant.ElementVideo wordt geschreven
CREATE TABLE IF NOT EXISTS `ElementVideo` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `VideoCode` varchar(25) CHARACTER SET utf8 DEFAULT NULL,
  `YoutubeUrl` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.ElementVideo: ~0 rows (ongeveer)
DELETE FROM `ElementVideo`;
/*!40000 ALTER TABLE `ElementVideo` DISABLE KEYS */;
/*!40000 ALTER TABLE `ElementVideo` ENABLE KEYS */;


-- Structuur van  tabel vagrant.ElementVideo_Live wordt geschreven
CREATE TABLE IF NOT EXISTS `ElementVideo_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `VideoCode` varchar(25) CHARACTER SET utf8 DEFAULT NULL,
  `YoutubeUrl` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.ElementVideo_Live: ~2 rows (ongeveer)
DELETE FROM `ElementVideo_Live`;
/*!40000 ALTER TABLE `ElementVideo_Live` DISABLE KEYS */;
INSERT INTO `ElementVideo_Live` (`ID`, `VideoCode`, `YoutubeUrl`) VALUES
	(1, 'test', NULL),
	(3, NULL, 'https://www.youtube.com/watch?v=iW3xYZzn8G8');
/*!40000 ALTER TABLE `ElementVideo_Live` ENABLE KEYS */;


-- Structuur van  tabel vagrant.ElementVideo_Versions wordt geschreven
CREATE TABLE IF NOT EXISTS `ElementVideo_Versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `VideoCode` varchar(25) CHARACTER SET utf8 DEFAULT NULL,
  `YoutubeUrl` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.ElementVideo_Versions: ~10 rows (ongeveer)
DELETE FROM `ElementVideo_Versions`;
/*!40000 ALTER TABLE `ElementVideo_Versions` DISABLE KEYS */;
INSERT INTO `ElementVideo_Versions` (`ID`, `RecordID`, `Version`, `VideoCode`, `YoutubeUrl`) VALUES
	(1, 1, 1, 'test', NULL),
	(2, 1, 2, 'test', NULL),
	(3, 1, 3, 'test', NULL),
	(4, 3, 1, NULL, NULL),
	(5, 3, 2, NULL, NULL),
	(6, 3, 3, NULL, NULL),
	(7, 3, 4, NULL, 'https://www.youtube.com/watch?v=iW3xYZzn8G8'),
	(8, 3, 5, NULL, 'https://www.youtube.com/watch?v=iW3xYZzn8G8'),
	(9, 3, 6, NULL, 'https://www.youtube.com/watch?v=iW3xYZzn8G8'),
	(10, 3, 7, NULL, 'https://www.youtube.com/watch?v=iW3xYZzn8G8');
/*!40000 ALTER TABLE `ElementVideo_Versions` ENABLE KEYS */;


-- Structuur van  tabel vagrant.Element_Live wordt geschreven
CREATE TABLE IF NOT EXISTS `Element_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('DNADesign\\Elemental\\Models\\BaseElement','Hestec\\ElementalExtensions\\Elements\\ElementContentImage','Hestec\\ElementalExtensions\\Elements\\ElementFaq','Hestec\\ElementalExtensions\\Elements\\ElementVideo','DNADesign\\Elemental\\Models\\ElementContent','Hestec\\ElementalExtensions\\Elements\\ElementCard','Hestec\\Tools\\Elements\\ElementVideo','ElementVideo') CHARACTER SET utf8 DEFAULT 'DNADesign\\Elemental\\Models\\BaseElement',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `ShowTitle` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Sort` int(11) NOT NULL DEFAULT '0',
  `ExtraClass` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Style` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Version` int(11) NOT NULL DEFAULT '0',
  `ParentID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `Sort` (`Sort`),
  KEY `ClassName` (`ClassName`),
  KEY `ParentID` (`ParentID`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.Element_Live: ~7 rows (ongeveer)
DELETE FROM `Element_Live`;
/*!40000 ALTER TABLE `Element_Live` DISABLE KEYS */;
INSERT INTO `Element_Live` (`ID`, `ClassName`, `LastEdited`, `Created`, `Title`, `ShowTitle`, `Sort`, `ExtraClass`, `Style`, `Version`, `ParentID`) VALUES
	(1, 'ElementVideo', '2018-03-26 22:34:42', '2018-03-26 22:34:36', 'test', 0, 1, NULL, NULL, 3, 1),
	(2, 'DNADesign\\Elemental\\Models\\ElementContent', '2018-03-31 16:16:17', '2018-03-29 20:46:08', 'Dit is een faketekst', 0, 2, NULL, NULL, 5, 2),
	(3, 'ElementVideo', '2018-03-29 20:58:49', '2018-03-29 20:55:23', 'Video', 0, 2, NULL, NULL, 7, 0),
	(4, 'Hestec\\Tools\\Elements\\ElementVideo', '2018-03-29 22:15:25', '2018-03-29 21:22:51', 'Video', 1, 1, NULL, NULL, 5, 0),
	(5, 'Hestec\\ElementalExtensions\\Elements\\ElementVideo', '2018-07-10 14:42:17', '2018-03-31 14:15:28', 'Video', 0, 1, NULL, NULL, 5, 2),
	(6, 'Hestec\\ElementalExtensions\\Elements\\ElementCard', '2018-03-31 16:16:17', '2018-03-31 15:13:32', 'Panel', 0, 3, NULL, NULL, 5, 0),
	(7, 'Hestec\\ElementalExtensions\\Elements\\ElementFaq', '2018-07-13 16:28:04', '2018-07-10 16:34:43', 'Veelgestelde vragen', 1, 1, NULL, NULL, 11, 2),
	(8, 'Hestec\\ElementalExtensions\\Elements\\ElementContentImage', '2018-08-03 11:13:15', '2018-08-02 17:43:53', 'Afbeelding links of rechts?', 1, 1, NULL, NULL, 22, 2);
/*!40000 ALTER TABLE `Element_Live` ENABLE KEYS */;


-- Structuur van  tabel vagrant.Element_Versions wordt geschreven
CREATE TABLE IF NOT EXISTS `Element_Versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `WasPublished` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `AuthorID` int(11) NOT NULL DEFAULT '0',
  `PublisherID` int(11) NOT NULL DEFAULT '0',
  `ClassName` enum('DNADesign\\Elemental\\Models\\BaseElement','Hestec\\ElementalExtensions\\Elements\\ElementContentImage','Hestec\\ElementalExtensions\\Elements\\ElementFaq','Hestec\\ElementalExtensions\\Elements\\ElementVideo','DNADesign\\Elemental\\Models\\ElementContent','Hestec\\ElementalExtensions\\Elements\\ElementCard','Hestec\\Tools\\Elements\\ElementVideo','ElementVideo') CHARACTER SET utf8 DEFAULT 'DNADesign\\Elemental\\Models\\BaseElement',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `ShowTitle` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Sort` int(11) NOT NULL DEFAULT '0',
  `ExtraClass` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Style` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `ParentID` int(11) NOT NULL DEFAULT '0',
  `WasDeleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `WasDraft` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`),
  KEY `AuthorID` (`AuthorID`),
  KEY `PublisherID` (`PublisherID`),
  KEY `Sort` (`Sort`),
  KEY `ClassName` (`ClassName`),
  KEY `ParentID` (`ParentID`)
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.Element_Versions: ~51 rows (ongeveer)
DELETE FROM `Element_Versions`;
/*!40000 ALTER TABLE `Element_Versions` DISABLE KEYS */;
INSERT INTO `Element_Versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `LastEdited`, `Created`, `Title`, `ShowTitle`, `Sort`, `ExtraClass`, `Style`, `ParentID`, `WasDeleted`, `WasDraft`) VALUES
	(1, 1, 1, 0, 1, 0, 'ElementVideo', '2018-03-26 22:34:36', '2018-03-26 22:34:36', 'test', 0, 1, NULL, NULL, 1, 0, 0),
	(2, 1, 2, 0, 1, 0, 'ElementVideo', '2018-03-26 22:34:42', '2018-03-26 22:34:36', 'test', 0, 1, NULL, NULL, 1, 0, 0),
	(3, 1, 3, 1, 1, 1, 'ElementVideo', '2018-03-26 22:34:42', '2018-03-26 22:34:36', 'test', 0, 1, NULL, NULL, 1, 0, 0),
	(4, 2, 1, 0, 1, 0, 'DNADesign\\Elemental\\Models\\ElementContent', '2018-03-29 20:46:08', '2018-03-29 20:46:08', 'Test', 0, 1, NULL, NULL, 2, 0, 0),
	(5, 2, 2, 0, 1, 0, 'DNADesign\\Elemental\\Models\\ElementContent', '2018-03-29 20:46:21', '2018-03-29 20:46:08', 'Dit is een faketekst', 0, 1, NULL, NULL, 2, 0, 0),
	(6, 2, 3, 1, 1, 1, 'DNADesign\\Elemental\\Models\\ElementContent', '2018-03-29 20:46:21', '2018-03-29 20:46:08', 'Dit is een faketekst', 0, 1, NULL, NULL, 2, 0, 0),
	(7, 3, 1, 0, 1, 0, 'ElementVideo', '2018-03-29 20:55:23', '2018-03-29 20:55:23', 'Video', 0, 2, NULL, NULL, 2, 0, 0),
	(8, 3, 2, 0, 1, 0, 'ElementVideo', '2018-03-29 20:55:29', '2018-03-29 20:55:23', 'Video', 0, 2, NULL, NULL, 2, 0, 0),
	(9, 3, 3, 1, 1, 1, 'ElementVideo', '2018-03-29 20:55:29', '2018-03-29 20:55:23', 'Video', 0, 2, NULL, NULL, 2, 0, 0),
	(10, 3, 4, 0, 1, 0, 'ElementVideo', '2018-03-29 20:58:08', '2018-03-29 20:55:23', 'Video', 0, 2, NULL, NULL, 2, 0, 0),
	(11, 3, 5, 1, 1, 1, 'ElementVideo', '2018-03-29 20:58:08', '2018-03-29 20:55:23', 'Video', 0, 2, NULL, NULL, 2, 0, 0),
	(12, 3, 6, 0, 1, 0, 'ElementVideo', '2018-03-29 20:58:49', '2018-03-29 20:55:23', 'Video', 0, 2, NULL, NULL, 2, 0, 0),
	(13, 3, 7, 1, 1, 1, 'ElementVideo', '2018-03-29 20:58:49', '2018-03-29 20:55:23', 'Video', 0, 2, NULL, NULL, 2, 0, 0),
	(14, 4, 1, 0, 1, 0, 'Hestec\\Tools\\Elements\\ElementVideo', '2018-03-29 21:22:51', '2018-03-29 21:22:51', 'Video', 1, 1, NULL, NULL, 2, 0, 0),
	(15, 4, 2, 0, 1, 0, 'Hestec\\Tools\\Elements\\ElementVideo', '2018-03-29 21:23:08', '2018-03-29 21:22:51', 'Video', 1, 1, NULL, NULL, 2, 0, 0),
	(16, 4, 3, 1, 1, 1, 'Hestec\\Tools\\Elements\\ElementVideo', '2018-03-29 21:23:08', '2018-03-29 21:22:51', 'Video', 1, 1, NULL, NULL, 2, 0, 0),
	(17, 4, 4, 0, 1, 0, 'Hestec\\Tools\\Elements\\ElementVideo', '2018-03-29 22:15:25', '2018-03-29 21:22:51', 'Video', 1, 1, NULL, NULL, 2, 0, 0),
	(18, 4, 5, 1, 1, 1, 'Hestec\\Tools\\Elements\\ElementVideo', '2018-03-29 22:15:25', '2018-03-29 21:22:51', 'Video', 1, 1, NULL, NULL, 2, 0, 0),
	(19, 5, 1, 0, 1, 0, 'Hestec\\ElementalExtensions\\Elements\\ElementVideo', '2018-03-31 14:15:28', '2018-03-31 14:15:28', 'Video', 0, 1, NULL, NULL, 2, 0, 0),
	(20, 5, 2, 0, 1, 0, 'Hestec\\ElementalExtensions\\Elements\\ElementVideo', '2018-03-31 14:15:54', '2018-03-31 14:15:28', 'Video', 0, 1, NULL, NULL, 2, 0, 0),
	(21, 5, 3, 1, 1, 1, 'Hestec\\ElementalExtensions\\Elements\\ElementVideo', '2018-03-31 14:15:54', '2018-03-31 14:15:28', 'Video', 0, 1, NULL, NULL, 2, 0, 0),
	(22, 6, 1, 0, 1, 0, 'Hestec\\ElementalExtensions\\Elements\\ElementCard', '2018-03-31 15:13:32', '2018-03-31 15:13:32', 'Panel', 0, 1, NULL, NULL, 2, 0, 0),
	(23, 6, 2, 0, 1, 0, 'Hestec\\ElementalExtensions\\Elements\\ElementCard', '2018-03-31 15:13:38', '2018-03-31 15:13:32', 'Panel', 0, 1, NULL, NULL, 2, 0, 0),
	(24, 6, 3, 1, 1, 1, 'Hestec\\ElementalExtensions\\Elements\\ElementCard', '2018-03-31 15:13:38', '2018-03-31 15:13:32', 'Panel', 0, 1, NULL, NULL, 2, 0, 0),
	(25, 6, 4, 1, 1, 1, 'Hestec\\ElementalExtensions\\Elements\\ElementCard', '2018-03-31 16:15:59', '2018-03-31 15:13:32', 'Panel', 0, 2, NULL, NULL, 2, 0, 0),
	(26, 2, 4, 1, 1, 1, 'DNADesign\\Elemental\\Models\\ElementContent', '2018-03-31 16:15:59', '2018-03-29 20:46:08', 'Dit is een faketekst', 0, 3, NULL, NULL, 2, 0, 0),
	(27, 2, 5, 1, 1, 1, 'DNADesign\\Elemental\\Models\\ElementContent', '2018-03-31 16:16:14', '2018-03-29 20:46:08', 'Dit is een faketekst', 0, 2, NULL, NULL, 2, 0, 0),
	(28, 6, 5, 1, 1, 1, 'Hestec\\ElementalExtensions\\Elements\\ElementCard', '2018-03-31 16:16:14', '2018-03-31 15:13:32', 'Panel', 0, 3, NULL, NULL, 2, 0, 0),
	(29, 5, 4, 0, 1, 0, 'Hestec\\ElementalExtensions\\Elements\\ElementVideo', '2018-07-10 14:42:17', '2018-03-31 14:15:28', 'Video', 0, 1, NULL, NULL, 2, 0, 0),
	(30, 5, 5, 1, 1, 1, 'Hestec\\ElementalExtensions\\Elements\\ElementVideo', '2018-07-10 14:42:17', '2018-03-31 14:15:28', 'Video', 0, 1, NULL, NULL, 2, 0, 0),
	(31, 7, 1, 0, 1, 0, 'Hestec\\ElementalExtensions\\Elements\\ElementFaq', '2018-07-10 16:34:43', '2018-07-10 16:34:43', 'Veelgestelde vragen', 0, 1, NULL, NULL, 2, 0, 0),
	(32, 7, 2, 0, 1, 0, 'Hestec\\ElementalExtensions\\Elements\\ElementFaq', '2018-07-10 16:40:21', '2018-07-10 16:34:43', 'Veelgestelde vragen', 0, 1, NULL, NULL, 2, 0, 0),
	(33, 7, 3, 1, 1, 1, 'Hestec\\ElementalExtensions\\Elements\\ElementFaq', '2018-07-10 16:40:21', '2018-07-10 16:34:43', 'Veelgestelde vragen', 0, 1, NULL, NULL, 2, 0, 0),
	(34, 7, 4, 0, 1, 0, 'Hestec\\ElementalExtensions\\Elements\\ElementFaq', '2018-07-10 16:53:17', '2018-07-10 16:34:43', 'Veelgestelde vragen', 0, 1, NULL, NULL, 2, 0, 0),
	(35, 7, 5, 1, 1, 1, 'Hestec\\ElementalExtensions\\Elements\\ElementFaq', '2018-07-10 16:53:17', '2018-07-10 16:34:43', 'Veelgestelde vragen', 0, 1, NULL, NULL, 2, 0, 0),
	(36, 7, 6, 0, 1, 0, 'Hestec\\ElementalExtensions\\Elements\\ElementFaq', '2018-07-10 16:58:27', '2018-07-10 16:34:43', 'Veelgestelde vragen', 1, 1, NULL, NULL, 2, 0, 0),
	(37, 7, 7, 1, 1, 1, 'Hestec\\ElementalExtensions\\Elements\\ElementFaq', '2018-07-10 16:58:27', '2018-07-10 16:34:43', 'Veelgestelde vragen', 1, 1, NULL, NULL, 2, 0, 0),
	(38, 7, 8, 0, 1, 0, 'Hestec\\ElementalExtensions\\Elements\\ElementFaq', '2018-07-13 16:12:02', '2018-07-10 16:34:43', 'Veelgestelde vragen', 1, 1, NULL, NULL, 2, 0, 0),
	(39, 7, 9, 1, 1, 1, 'Hestec\\ElementalExtensions\\Elements\\ElementFaq', '2018-07-13 16:12:02', '2018-07-10 16:34:43', 'Veelgestelde vragen', 1, 1, NULL, NULL, 2, 0, 0),
	(40, 7, 10, 0, 1, 0, 'Hestec\\ElementalExtensions\\Elements\\ElementFaq', '2018-07-13 16:28:04', '2018-07-10 16:34:43', 'Veelgestelde vragen', 1, 1, NULL, NULL, 2, 0, 0),
	(41, 7, 11, 1, 1, 1, 'Hestec\\ElementalExtensions\\Elements\\ElementFaq', '2018-07-13 16:28:04', '2018-07-10 16:34:43', 'Veelgestelde vragen', 1, 1, NULL, NULL, 2, 0, 0),
	(42, 8, 1, 0, 1, 0, 'Hestec\\ElementalExtensions\\Elements\\ElementContentImage', '2018-08-02 17:43:53', '2018-08-02 17:43:53', 'Test', 0, 1, NULL, NULL, 2, 0, 1),
	(43, 8, 2, 0, 1, 0, 'Hestec\\ElementalExtensions\\Elements\\ElementContentImage', '2018-08-02 17:44:02', '2018-08-02 17:43:53', 'Test', 0, 1, NULL, NULL, 2, 0, 1),
	(44, 8, 3, 0, 1, 0, 'Hestec\\ElementalExtensions\\Elements\\ElementContentImage', '2018-08-02 17:44:02', '2018-08-02 17:43:53', 'Test', 0, 1, NULL, NULL, 2, 0, 1),
	(45, 8, 4, 1, 1, 1, 'Hestec\\ElementalExtensions\\Elements\\ElementContentImage', '2018-08-02 17:44:02', '2018-08-02 17:43:53', 'Test', 0, 1, NULL, NULL, 2, 0, 1),
	(46, 8, 5, 0, 1, 0, 'Hestec\\ElementalExtensions\\Elements\\ElementContentImage', '2018-08-03 10:20:12', '2018-08-02 17:43:53', 'Test', 1, 1, NULL, NULL, 2, 0, 1),
	(47, 8, 6, 0, 1, 0, 'Hestec\\ElementalExtensions\\Elements\\ElementContentImage', '2018-08-03 10:20:12', '2018-08-02 17:43:53', 'Test', 1, 1, NULL, NULL, 2, 0, 1),
	(48, 8, 7, 1, 1, 1, 'Hestec\\ElementalExtensions\\Elements\\ElementContentImage', '2018-08-03 10:20:12', '2018-08-02 17:43:53', 'Test', 1, 1, NULL, NULL, 2, 0, 1),
	(49, 8, 8, 0, 1, 0, 'Hestec\\ElementalExtensions\\Elements\\ElementContentImage', '2018-08-03 10:26:36', '2018-08-02 17:43:53', 'Test', 1, 1, NULL, NULL, 2, 0, 1),
	(50, 8, 9, 0, 1, 0, 'Hestec\\ElementalExtensions\\Elements\\ElementContentImage', '2018-08-03 10:26:36', '2018-08-02 17:43:53', 'Test', 1, 1, NULL, NULL, 2, 0, 1),
	(51, 8, 10, 1, 1, 1, 'Hestec\\ElementalExtensions\\Elements\\ElementContentImage', '2018-08-03 10:26:36', '2018-08-02 17:43:53', 'Test', 1, 1, NULL, NULL, 2, 0, 1),
	(52, 8, 11, 0, 1, 0, 'Hestec\\ElementalExtensions\\Elements\\ElementContentImage', '2018-08-03 10:43:05', '2018-08-02 17:43:53', 'Afbeelding links of rechts?', 1, 1, NULL, NULL, 2, 0, 1),
	(53, 8, 12, 0, 1, 0, 'Hestec\\ElementalExtensions\\Elements\\ElementContentImage', '2018-08-03 10:43:05', '2018-08-02 17:43:53', 'Afbeelding links of rechts?', 1, 1, NULL, NULL, 2, 0, 1),
	(54, 8, 13, 1, 1, 1, 'Hestec\\ElementalExtensions\\Elements\\ElementContentImage', '2018-08-03 10:43:06', '2018-08-02 17:43:53', 'Afbeelding links of rechts?', 1, 1, NULL, NULL, 2, 0, 1),
	(55, 8, 14, 0, 1, 0, 'Hestec\\ElementalExtensions\\Elements\\ElementContentImage', '2018-08-03 11:03:42', '2018-08-02 17:43:53', 'Afbeelding links of rechts?', 1, 1, NULL, NULL, 2, 0, 1),
	(56, 8, 15, 0, 1, 0, 'Hestec\\ElementalExtensions\\Elements\\ElementContentImage', '2018-08-03 11:03:42', '2018-08-02 17:43:53', 'Afbeelding links of rechts?', 1, 1, NULL, NULL, 2, 0, 1),
	(57, 8, 16, 1, 1, 1, 'Hestec\\ElementalExtensions\\Elements\\ElementContentImage', '2018-08-03 11:03:42', '2018-08-02 17:43:53', 'Afbeelding links of rechts?', 1, 1, NULL, NULL, 2, 0, 1),
	(58, 8, 17, 0, 1, 0, 'Hestec\\ElementalExtensions\\Elements\\ElementContentImage', '2018-08-03 11:12:44', '2018-08-02 17:43:53', 'Afbeelding links of rechts?', 1, 1, NULL, NULL, 2, 0, 1),
	(59, 8, 18, 0, 1, 0, 'Hestec\\ElementalExtensions\\Elements\\ElementContentImage', '2018-08-03 11:12:44', '2018-08-02 17:43:53', 'Afbeelding links of rechts?', 1, 1, NULL, NULL, 2, 0, 1),
	(60, 8, 19, 1, 1, 1, 'Hestec\\ElementalExtensions\\Elements\\ElementContentImage', '2018-08-03 11:12:44', '2018-08-02 17:43:53', 'Afbeelding links of rechts?', 1, 1, NULL, NULL, 2, 0, 1),
	(61, 8, 20, 0, 1, 0, 'Hestec\\ElementalExtensions\\Elements\\ElementContentImage', '2018-08-03 11:13:14', '2018-08-02 17:43:53', 'Afbeelding links of rechts?', 1, 1, NULL, NULL, 2, 0, 1),
	(62, 8, 21, 0, 1, 0, 'Hestec\\ElementalExtensions\\Elements\\ElementContentImage', '2018-08-03 11:13:14', '2018-08-02 17:43:53', 'Afbeelding links of rechts?', 1, 1, NULL, NULL, 2, 0, 1),
	(63, 8, 22, 1, 1, 1, 'Hestec\\ElementalExtensions\\Elements\\ElementContentImage', '2018-08-03 11:13:15', '2018-08-02 17:43:53', 'Afbeelding links of rechts?', 1, 1, NULL, NULL, 2, 0, 1);
/*!40000 ALTER TABLE `Element_Versions` ENABLE KEYS */;


-- Structuur van  tabel vagrant.EnquiryFormField wordt geschreven
CREATE TABLE IF NOT EXISTS `EnquiryFormField` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('Axllent\\EnquiryPage\\Model\\EnquiryFormField') CHARACTER SET utf8 DEFAULT 'Axllent\\EnquiryPage\\Model\\EnquiryFormField',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `SortOrder` int(11) NOT NULL DEFAULT '0',
  `FieldName` varchar(150) CHARACTER SET utf8 DEFAULT NULL,
  `FieldType` enum('Text','Email','Select','Checkbox','Radio','Header','HTML') CHARACTER SET utf8 DEFAULT 'Text',
  `FieldOptions` mediumtext CHARACTER SET utf8,
  `PlaceholderText` varchar(150) CHARACTER SET utf8 DEFAULT NULL,
  `RequiredField` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `EnquiryPageID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ClassName` (`ClassName`),
  KEY `EnquiryPageID` (`EnquiryPageID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.EnquiryFormField: ~0 rows (ongeveer)
DELETE FROM `EnquiryFormField`;
/*!40000 ALTER TABLE `EnquiryFormField` DISABLE KEYS */;
/*!40000 ALTER TABLE `EnquiryFormField` ENABLE KEYS */;


-- Structuur van  tabel vagrant.EnquiryPage wordt geschreven
CREATE TABLE IF NOT EXISTS `EnquiryPage` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `EmailTo` varchar(254) CHARACTER SET utf8 DEFAULT NULL,
  `EmailBcc` varchar(254) CHARACTER SET utf8 DEFAULT NULL,
  `EmailFrom` varchar(254) CHARACTER SET utf8 DEFAULT NULL,
  `EmailSubject` varchar(254) CHARACTER SET utf8 DEFAULT NULL,
  `EmailSubmitButtonText` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `EmailSubmitCompletion` mediumtext CHARACTER SET utf8,
  `EmailPlain` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `AddCaptcha` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `CaptchaText` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `CaptchaHelp` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.EnquiryPage: ~0 rows (ongeveer)
DELETE FROM `EnquiryPage`;
/*!40000 ALTER TABLE `EnquiryPage` DISABLE KEYS */;
/*!40000 ALTER TABLE `EnquiryPage` ENABLE KEYS */;


-- Structuur van  tabel vagrant.EnquiryPage_Live wordt geschreven
CREATE TABLE IF NOT EXISTS `EnquiryPage_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `EmailTo` varchar(254) CHARACTER SET utf8 DEFAULT NULL,
  `EmailBcc` varchar(254) CHARACTER SET utf8 DEFAULT NULL,
  `EmailFrom` varchar(254) CHARACTER SET utf8 DEFAULT NULL,
  `EmailSubject` varchar(254) CHARACTER SET utf8 DEFAULT NULL,
  `EmailSubmitButtonText` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `EmailSubmitCompletion` mediumtext CHARACTER SET utf8,
  `EmailPlain` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `AddCaptcha` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `CaptchaText` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `CaptchaHelp` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.EnquiryPage_Live: ~0 rows (ongeveer)
DELETE FROM `EnquiryPage_Live`;
/*!40000 ALTER TABLE `EnquiryPage_Live` DISABLE KEYS */;
/*!40000 ALTER TABLE `EnquiryPage_Live` ENABLE KEYS */;


-- Structuur van  tabel vagrant.EnquiryPage_Versions wordt geschreven
CREATE TABLE IF NOT EXISTS `EnquiryPage_Versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `EmailTo` varchar(254) CHARACTER SET utf8 DEFAULT NULL,
  `EmailBcc` varchar(254) CHARACTER SET utf8 DEFAULT NULL,
  `EmailFrom` varchar(254) CHARACTER SET utf8 DEFAULT NULL,
  `EmailSubject` varchar(254) CHARACTER SET utf8 DEFAULT NULL,
  `EmailSubmitButtonText` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `EmailSubmitCompletion` mediumtext CHARACTER SET utf8,
  `EmailPlain` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `AddCaptcha` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `CaptchaText` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `CaptchaHelp` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.EnquiryPage_Versions: ~2 rows (ongeveer)
DELETE FROM `EnquiryPage_Versions`;
/*!40000 ALTER TABLE `EnquiryPage_Versions` DISABLE KEYS */;
INSERT INTO `EnquiryPage_Versions` (`ID`, `RecordID`, `Version`, `EmailTo`, `EmailBcc`, `EmailFrom`, `EmailSubject`, `EmailSubmitButtonText`, `EmailSubmitCompletion`, `EmailPlain`, `AddCaptcha`, `CaptchaText`, `CaptchaHelp`) VALUES
	(1, 10, 1, NULL, NULL, NULL, 'Website Enquiry', 'Submit Enquiry', '<p>Thanks, we\'ve received your enquiry and will respond as soon as we\'re able.</p>', 0, 0, 'Verification Image', NULL),
	(2, 10, 2, NULL, NULL, NULL, 'Website Enquiry', 'Submit Enquiry', '<p>Thanks, we\'ve received your enquiry and will respond as soon as we\'re able.</p>', 0, 0, 'Verification Image', NULL);
/*!40000 ALTER TABLE `EnquiryPage_Versions` ENABLE KEYS */;


-- Structuur van  tabel vagrant.ErrorPage wordt geschreven
CREATE TABLE IF NOT EXISTS `ErrorPage` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ErrorCode` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.ErrorPage: ~2 rows (ongeveer)
DELETE FROM `ErrorPage`;
/*!40000 ALTER TABLE `ErrorPage` DISABLE KEYS */;
INSERT INTO `ErrorPage` (`ID`, `ErrorCode`) VALUES
	(4, 404),
	(5, 500);
/*!40000 ALTER TABLE `ErrorPage` ENABLE KEYS */;


-- Structuur van  tabel vagrant.ErrorPage_Live wordt geschreven
CREATE TABLE IF NOT EXISTS `ErrorPage_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ErrorCode` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.ErrorPage_Live: ~2 rows (ongeveer)
DELETE FROM `ErrorPage_Live`;
/*!40000 ALTER TABLE `ErrorPage_Live` DISABLE KEYS */;
INSERT INTO `ErrorPage_Live` (`ID`, `ErrorCode`) VALUES
	(4, 404),
	(5, 500);
/*!40000 ALTER TABLE `ErrorPage_Live` ENABLE KEYS */;


-- Structuur van  tabel vagrant.ErrorPage_Versions wordt geschreven
CREATE TABLE IF NOT EXISTS `ErrorPage_Versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `ErrorCode` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.ErrorPage_Versions: ~2 rows (ongeveer)
DELETE FROM `ErrorPage_Versions`;
/*!40000 ALTER TABLE `ErrorPage_Versions` DISABLE KEYS */;
INSERT INTO `ErrorPage_Versions` (`ID`, `RecordID`, `Version`, `ErrorCode`) VALUES
	(1, 4, 1, 404),
	(2, 5, 1, 500);
/*!40000 ALTER TABLE `ErrorPage_Versions` ENABLE KEYS */;


-- Structuur van  tabel vagrant.Event wordt geschreven
CREATE TABLE IF NOT EXISTS `Event` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('Event') CHARACTER SET utf8 DEFAULT 'Event',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Description` mediumtext CHARACTER SET utf8,
  `DateTime` datetime DEFAULT NULL,
  `EventsPageID` int(11) NOT NULL DEFAULT '0',
  `ImageID` int(11) NOT NULL DEFAULT '0',
  `PriceCurrency` varchar(3) CHARACTER SET utf8 DEFAULT NULL,
  `PriceAmount` decimal(19,4) NOT NULL DEFAULT '0.0000',
  PRIMARY KEY (`ID`),
  KEY `ClassName` (`ClassName`),
  KEY `EventsPageID` (`EventsPageID`),
  KEY `ImageID` (`ImageID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.Event: ~0 rows (ongeveer)
DELETE FROM `Event`;
/*!40000 ALTER TABLE `Event` DISABLE KEYS */;
/*!40000 ALTER TABLE `Event` ENABLE KEYS */;


-- Structuur van  tabel vagrant.ExactOnlineConnection wordt geschreven
CREATE TABLE IF NOT EXISTS `ExactOnlineConnection` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('Hestec\\ExactOnline\\ExactOnlineConnection') CHARACTER SET utf8 DEFAULT 'Hestec\\ExactOnline\\ExactOnlineConnection',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `OauthCode` mediumtext CHARACTER SET utf8,
  `AccessToken` mediumtext CHARACTER SET utf8,
  `RefreshToken` mediumtext CHARACTER SET utf8,
  `TokenExpires` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.ExactOnlineConnection: ~0 rows (ongeveer)
DELETE FROM `ExactOnlineConnection`;
/*!40000 ALTER TABLE `ExactOnlineConnection` DISABLE KEYS */;
INSERT INTO `ExactOnlineConnection` (`ID`, `ClassName`, `LastEdited`, `Created`, `OauthCode`, `AccessToken`, `RefreshToken`, `TokenExpires`) VALUES
	(1, 'Hestec\\ExactOnline\\ExactOnlineConnection', '2018-04-23 23:17:09', '2018-04-23 21:56:53', '1524515757', 's:550:"gAAAAFCjo4tRpw2M-gmCrBW6N2nqzOCx5jkVRPJfcbRKYK227_nZaGOln5J8fg-D7tHUoae0XEpFO3QJYHzVwCedD8CQDtc669L4V66tBkfeEDadn_sSQT-wDZXRIAL66rMyn735fAkQLV8yONgrJkDPOgLKijiaIq836ji_vaoVUuPcFAEAAIAAAADEVLSn2pvyP2h5NoLVAitC8h4AzG8YNNV_IB8W5hiG9eO7Q7wvBHAb-pFy5Q1woCJYhq1p-VYuRlvAEXP1_AJGrAcIqPKsJxqjgARB7-gpboBbNhU_Caq5fr_6_eIhAWZRZpTZooUPLSKZrSm8qHRcA2tqOdvW0Vr3kgoC4QAqihtWUWgBJRmjMAaq50pjCRguz24-QMiUg0h7lrsQvrQvcmNa82UNnfc16RomW-YojbXj8bzpHHvkc9dXkLo_LkqPkH11UDNe2MzxWpBqLAMyYrbXQ_hI3RSvU6NCgHKnYE1xA4ZYB0uJxBrg7-DWB1OgMAzwSxHHGhrBdZCkRBgD3mG8D6gyyT3jlfOmF6Zyvg";', 'zsPl!IAAAAG7MXC3WqjOmX8Q6SFaiG-B6yVlfhDScWgzrLXpq3zI0oQAAAAEzV8n8i9jKIYfex-J0dHCONuUR7tAcZjcqjKAcHIOe-eYH_w8eU9gu93PWVJwvY9ws8mkuhYheVCx9-0N8WkUEaZ6oqsuoG3pdCZOsOHFBiI_Fp_W72KE5VHK93NMwLJz5WGf4yduWd0S5WpkOt0jIfGNHLleFs_O5MG1iiUY68alPI8QB7VKqNUOfwLQVWExSLFhen_DMmiFVxlTlx231', '1524518829');
/*!40000 ALTER TABLE `ExactOnlineConnection` ENABLE KEYS */;


-- Structuur van  tabel vagrant.ExtraNavigationItem wordt geschreven
CREATE TABLE IF NOT EXISTS `ExtraNavigationItem` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('ExtraNavigationItem') CHARACTER SET utf8 DEFAULT 'ExtraNavigationItem',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Label` varchar(28) CHARACTER SET utf8 DEFAULT NULL,
  `ExternLink` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `SortOrder` int(11) NOT NULL DEFAULT '0',
  `InternLinkID` int(11) NOT NULL DEFAULT '0',
  `SiteConfigID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `SortOrder` (`SortOrder`),
  KEY `ClassName` (`ClassName`),
  KEY `InternLinkID` (`InternLinkID`),
  KEY `SiteConfigID` (`SiteConfigID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.ExtraNavigationItem: ~0 rows (ongeveer)
DELETE FROM `ExtraNavigationItem`;
/*!40000 ALTER TABLE `ExtraNavigationItem` DISABLE KEYS */;
/*!40000 ALTER TABLE `ExtraNavigationItem` ENABLE KEYS */;


-- Structuur van  tabel vagrant.File wordt geschreven
CREATE TABLE IF NOT EXISTS `File` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('SilverStripe\\Assets\\File','SilverStripe\\Assets\\Folder','SilverStripe\\Assets\\Image') CHARACTER SET utf8 DEFAULT 'SilverStripe\\Assets\\File',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `ShowInSearch` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `CanViewType` enum('Anyone','LoggedInUsers','OnlyTheseUsers','Inherit') CHARACTER SET utf8 DEFAULT 'Inherit',
  `CanEditType` enum('LoggedInUsers','OnlyTheseUsers','Inherit') CHARACTER SET utf8 DEFAULT 'Inherit',
  `Version` int(11) NOT NULL DEFAULT '0',
  `ParentID` int(11) NOT NULL DEFAULT '0',
  `OwnerID` int(11) NOT NULL DEFAULT '0',
  `FileHash` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `FileFilename` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `FileVariant` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `Name` (`Name`),
  KEY `ClassName` (`ClassName`),
  KEY `ParentID` (`ParentID`),
  KEY `OwnerID` (`OwnerID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.File: ~3 rows (ongeveer)
DELETE FROM `File`;
/*!40000 ALTER TABLE `File` DISABLE KEYS */;
INSERT INTO `File` (`ID`, `ClassName`, `LastEdited`, `Created`, `Name`, `Title`, `ShowInSearch`, `CanViewType`, `CanEditType`, `Version`, `ParentID`, `OwnerID`, `FileHash`, `FileFilename`, `FileVariant`) VALUES
	(1, 'SilverStripe\\Assets\\Folder', '2018-04-04 11:22:57', '2018-04-04 11:22:57', 'Uploads', 'Uploads', 1, 'Inherit', 'Inherit', 1, 0, 1, NULL, NULL, NULL),
	(2, 'SilverStripe\\Assets\\Image', '2018-08-03 10:29:23', '2018-08-03 10:26:25', 'Fotolia_96828948_M.png', 'Fotolia 96828948 M', 1, 'Inherit', 'Inherit', 3, 1, 1, '4146a156ef1a6044f7094e221d9e323a7d16591d', 'Uploads/Fotolia_96828948_M.png', NULL),
	(3, 'SilverStripe\\Assets\\Image', '2018-08-03 11:13:15', '2018-08-03 11:13:11', 'Fotolia_96828948_M-v2.png', 'Fotolia 96828948 M v2', 1, 'Inherit', 'Inherit', 2, 1, 1, '3322fc43f39d9a795698b0bcb2b8bda18e30e599', 'Uploads/Fotolia_96828948_M-v2.png', NULL);
/*!40000 ALTER TABLE `File` ENABLE KEYS */;


-- Structuur van  tabel vagrant.FileLink wordt geschreven
CREATE TABLE IF NOT EXISTS `FileLink` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('SilverStripe\\Assets\\Shortcodes\\FileLink') CHARACTER SET utf8 DEFAULT 'SilverStripe\\Assets\\Shortcodes\\FileLink',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `LinkedID` int(11) NOT NULL DEFAULT '0',
  `ParentID` int(11) NOT NULL DEFAULT '0',
  `ParentClass` enum('Hestec\\SisowMethod\\SisowMethod','Hestec\\ElementalExtensions\\Dataobjects\\FaqQuestion','Hestec\\ExactOnline\\ExactOnlineConnection','Hestec\\FaqPage\\FaqCategorie','Hestec\\FaqPage\\FaqQuestion','BrandColor','BrandColorType','ExtraNavigationItem','Hestec\\Tools\\OwnIp','SilverStripe\\Assets\\File','SilverStripe\\SiteConfig\\SiteConfig','SilverStripe\\Versioned\\ChangeSetItem','SilverStripe\\Versioned\\ChangeSet','DNADesign\\Elemental\\Models\\BaseElement','DNADesign\\Elemental\\Models\\ElementalArea','gorriecoe\\Link\\Models\\Link','gorriecoe\\Menu\\Models\\MenuSet','SilverStripe\\Assets\\Shortcodes\\FileLink','SilverStripe\\Blog\\Model\\BlogCategory','SilverStripe\\Blog\\Model\\BlogTag','SilverStripe\\CMS\\Model\\SiteTreeLink','SilverStripe\\CMS\\Model\\SiteTree','SilverStripe\\Security\\Group','SilverStripe\\Security\\LoginAttempt','SilverStripe\\Security\\MemberPassword','SilverStripe\\Security\\Member','SilverStripe\\Security\\Permission','SilverStripe\\Security\\PermissionRoleCode','SilverStripe\\Security\\PermissionRole','SilverStripe\\Security\\RememberLoginHash','SilverStripe\\UserForms\\Model\\EditableCustomRule','SilverStripe\\UserForms\\Model\\EditableFormField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableOption','SilverStripe\\UserForms\\Model\\Recipient\\EmailRecipientCondition','SilverStripe\\UserForms\\Model\\Recipient\\EmailRecipient','SilverStripe\\UserForms\\Model\\Submission\\SubmittedFormField','SilverStripe\\UserForms\\Model\\Submission\\SubmittedForm','SilverStripe\\Assets\\Folder','SilverStripe\\Assets\\Image','Hestec\\ElementalExtensions\\Elements\\ElementContentImage','Hestec\\ElementalExtensions\\Elements\\ElementFaq','Hestec\\ElementalExtensions\\Elements\\ElementVideo','DNADesign\\Elemental\\Models\\ElementContent','gorriecoe\\Menu\\Models\\MenuLink','Page','ElementPage','HomePage','Hestec\\Faqpage\\FaqPage','FirstChildRedirect','SilverStripe\\ErrorPage\\ErrorPage','SilverStripe\\Blog\\Model\\Blog','SilverStripe\\Blog\\Model\\BlogPost','SilverStripe\\CMS\\Model\\RedirectorPage','SilverStripe\\CMS\\Model\\VirtualPage','SilverStripe\\UserForms\\Model\\UserDefinedForm','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableCheckbox','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableCountryDropdownField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableDateField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableEmailField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableFieldGroupEnd','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableFieldGroup','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableFileField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableFormHeading','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableFormStep','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableLiteralField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableMemberListField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableMultipleOptionField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableNumericField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableTextField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableCheckboxGroupField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableDropdown','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableRadioField','SilverStripe\\UserForms\\Model\\Submission\\SubmittedFileField') CHARACTER SET utf8 DEFAULT 'Hestec\\SisowMethod\\SisowMethod',
  PRIMARY KEY (`ID`),
  KEY `ClassName` (`ClassName`),
  KEY `LinkedID` (`LinkedID`),
  KEY `Parent` (`ParentID`,`ParentClass`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.FileLink: ~0 rows (ongeveer)
DELETE FROM `FileLink`;
/*!40000 ALTER TABLE `FileLink` DISABLE KEYS */;
/*!40000 ALTER TABLE `FileLink` ENABLE KEYS */;


-- Structuur van  tabel vagrant.File_EditorGroups wordt geschreven
CREATE TABLE IF NOT EXISTS `File_EditorGroups` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `FileID` int(11) NOT NULL DEFAULT '0',
  `GroupID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `FileID` (`FileID`),
  KEY `GroupID` (`GroupID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.File_EditorGroups: ~0 rows (ongeveer)
DELETE FROM `File_EditorGroups`;
/*!40000 ALTER TABLE `File_EditorGroups` DISABLE KEYS */;
/*!40000 ALTER TABLE `File_EditorGroups` ENABLE KEYS */;


-- Structuur van  tabel vagrant.File_Live wordt geschreven
CREATE TABLE IF NOT EXISTS `File_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('SilverStripe\\Assets\\File','SilverStripe\\Assets\\Folder','SilverStripe\\Assets\\Image') CHARACTER SET utf8 DEFAULT 'SilverStripe\\Assets\\File',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `ShowInSearch` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `CanViewType` enum('Anyone','LoggedInUsers','OnlyTheseUsers','Inherit') CHARACTER SET utf8 DEFAULT 'Inherit',
  `CanEditType` enum('LoggedInUsers','OnlyTheseUsers','Inherit') CHARACTER SET utf8 DEFAULT 'Inherit',
  `Version` int(11) NOT NULL DEFAULT '0',
  `ParentID` int(11) NOT NULL DEFAULT '0',
  `OwnerID` int(11) NOT NULL DEFAULT '0',
  `FileHash` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `FileFilename` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `FileVariant` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `Name` (`Name`),
  KEY `ClassName` (`ClassName`),
  KEY `ParentID` (`ParentID`),
  KEY `OwnerID` (`OwnerID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.File_Live: ~0 rows (ongeveer)
DELETE FROM `File_Live`;
/*!40000 ALTER TABLE `File_Live` DISABLE KEYS */;
INSERT INTO `File_Live` (`ID`, `ClassName`, `LastEdited`, `Created`, `Name`, `Title`, `ShowInSearch`, `CanViewType`, `CanEditType`, `Version`, `ParentID`, `OwnerID`, `FileHash`, `FileFilename`, `FileVariant`) VALUES
	(1, 'SilverStripe\\Assets\\Folder', '2018-04-04 11:22:57', '2018-04-04 11:22:57', 'Uploads', 'Uploads', 1, 'Inherit', 'Inherit', 1, 0, 1, NULL, NULL, NULL),
	(2, 'SilverStripe\\Assets\\Image', '2018-08-03 10:29:23', '2018-08-03 10:26:25', 'Fotolia_96828948_M.png', 'Fotolia 96828948 M', 1, 'Inherit', 'Inherit', 3, 1, 1, '4146a156ef1a6044f7094e221d9e323a7d16591d', 'Uploads/Fotolia_96828948_M.png', NULL),
	(3, 'SilverStripe\\Assets\\Image', '2018-08-03 11:13:15', '2018-08-03 11:13:11', 'Fotolia_96828948_M-v2.png', 'Fotolia 96828948 M v2', 1, 'Inherit', 'Inherit', 2, 1, 1, '3322fc43f39d9a795698b0bcb2b8bda18e30e599', 'Uploads/Fotolia_96828948_M-v2.png', NULL);
/*!40000 ALTER TABLE `File_Live` ENABLE KEYS */;


-- Structuur van  tabel vagrant.File_Versions wordt geschreven
CREATE TABLE IF NOT EXISTS `File_Versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `WasPublished` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `AuthorID` int(11) NOT NULL DEFAULT '0',
  `PublisherID` int(11) NOT NULL DEFAULT '0',
  `ClassName` enum('SilverStripe\\Assets\\File','SilverStripe\\Assets\\Folder','SilverStripe\\Assets\\Image') CHARACTER SET utf8 DEFAULT 'SilverStripe\\Assets\\File',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `ShowInSearch` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `CanViewType` enum('Anyone','LoggedInUsers','OnlyTheseUsers','Inherit') CHARACTER SET utf8 DEFAULT 'Inherit',
  `CanEditType` enum('LoggedInUsers','OnlyTheseUsers','Inherit') CHARACTER SET utf8 DEFAULT 'Inherit',
  `ParentID` int(11) NOT NULL DEFAULT '0',
  `OwnerID` int(11) NOT NULL DEFAULT '0',
  `FileHash` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `FileFilename` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `FileVariant` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `WasDeleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `WasDraft` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`),
  KEY `AuthorID` (`AuthorID`),
  KEY `PublisherID` (`PublisherID`),
  KEY `Name` (`Name`),
  KEY `ClassName` (`ClassName`),
  KEY `ParentID` (`ParentID`),
  KEY `OwnerID` (`OwnerID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.File_Versions: ~6 rows (ongeveer)
DELETE FROM `File_Versions`;
/*!40000 ALTER TABLE `File_Versions` DISABLE KEYS */;
INSERT INTO `File_Versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `LastEdited`, `Created`, `Name`, `Title`, `ShowInSearch`, `CanViewType`, `CanEditType`, `ParentID`, `OwnerID`, `FileHash`, `FileFilename`, `FileVariant`, `WasDeleted`, `WasDraft`) VALUES
	(1, 1, 1, 1, 1, 1, 'SilverStripe\\Assets\\Folder', '2018-04-04 11:22:57', '2018-04-04 11:22:57', 'Uploads', 'Uploads', 1, 'Inherit', 'Inherit', 0, 1, NULL, NULL, NULL, 0, 0),
	(2, 2, 1, 0, 1, 0, 'SilverStripe\\Assets\\Image', '2018-08-03 10:26:25', '2018-08-03 10:26:25', 'Fotolia_96828948_M.png', 'Fotolia 96828948 M', 1, 'Inherit', 'Inherit', 1, 1, '4146a156ef1a6044f7094e221d9e323a7d16591d', 'Uploads/Fotolia_96828948_M.png', NULL, 0, 1),
	(3, 2, 2, 0, 1, 0, 'SilverStripe\\Assets\\Image', '2018-08-03 10:29:23', '2018-08-03 10:26:25', 'Fotolia_96828948_M.png', 'Fotolia 96828948 M', 1, 'Inherit', 'Inherit', 1, 1, '4146a156ef1a6044f7094e221d9e323a7d16591d', 'Uploads/Fotolia_96828948_M.png', NULL, 0, 1),
	(4, 2, 3, 1, 1, 1, 'SilverStripe\\Assets\\Image', '2018-08-03 10:29:23', '2018-08-03 10:26:25', 'Fotolia_96828948_M.png', 'Fotolia 96828948 M', 1, 'Inherit', 'Inherit', 1, 1, '4146a156ef1a6044f7094e221d9e323a7d16591d', 'Uploads/Fotolia_96828948_M.png', NULL, 0, 1),
	(5, 3, 1, 0, 1, 0, 'SilverStripe\\Assets\\Image', '2018-08-03 11:13:11', '2018-08-03 11:13:11', 'Fotolia_96828948_M-v2.png', 'Fotolia 96828948 M v2', 1, 'Inherit', 'Inherit', 1, 1, '3322fc43f39d9a795698b0bcb2b8bda18e30e599', 'Uploads/Fotolia_96828948_M-v2.png', NULL, 0, 1),
	(6, 3, 2, 1, 1, 1, 'SilverStripe\\Assets\\Image', '2018-08-03 11:13:15', '2018-08-03 11:13:11', 'Fotolia_96828948_M-v2.png', 'Fotolia 96828948 M v2', 1, 'Inherit', 'Inherit', 1, 1, '3322fc43f39d9a795698b0bcb2b8bda18e30e599', 'Uploads/Fotolia_96828948_M-v2.png', NULL, 0, 1);
/*!40000 ALTER TABLE `File_Versions` ENABLE KEYS */;


-- Structuur van  tabel vagrant.File_ViewerGroups wordt geschreven
CREATE TABLE IF NOT EXISTS `File_ViewerGroups` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `FileID` int(11) NOT NULL DEFAULT '0',
  `GroupID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `FileID` (`FileID`),
  KEY `GroupID` (`GroupID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.File_ViewerGroups: ~0 rows (ongeveer)
DELETE FROM `File_ViewerGroups`;
/*!40000 ALTER TABLE `File_ViewerGroups` DISABLE KEYS */;
/*!40000 ALTER TABLE `File_ViewerGroups` ENABLE KEYS */;


-- Structuur van  tabel vagrant.Group wordt geschreven
CREATE TABLE IF NOT EXISTS `Group` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('SilverStripe\\Security\\Group') CHARACTER SET utf8 DEFAULT 'SilverStripe\\Security\\Group',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Description` mediumtext CHARACTER SET utf8,
  `Code` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Locked` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Sort` int(11) NOT NULL DEFAULT '0',
  `HtmlEditorConfig` mediumtext CHARACTER SET utf8,
  `ParentID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ClassName` (`ClassName`),
  KEY `ParentID` (`ParentID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.Group: ~3 rows (ongeveer)
DELETE FROM `Group`;
/*!40000 ALTER TABLE `Group` DISABLE KEYS */;
INSERT INTO `Group` (`ID`, `ClassName`, `LastEdited`, `Created`, `Title`, `Description`, `Code`, `Locked`, `Sort`, `HtmlEditorConfig`, `ParentID`) VALUES
	(1, 'SilverStripe\\Security\\Group', '2018-03-22 17:59:28', '2018-03-22 17:59:28', 'Content Authors', NULL, 'content-authors', 0, 1, NULL, 0),
	(2, 'SilverStripe\\Security\\Group', '2018-03-22 17:59:28', '2018-03-22 17:59:28', 'Administrators', NULL, 'administrators', 0, 0, NULL, 0),
	(3, 'SilverStripe\\Security\\Group', '2018-04-05 11:04:10', '2018-04-05 11:04:10', 'Blog users', NULL, 'blog-users', 0, 0, NULL, 0);
/*!40000 ALTER TABLE `Group` ENABLE KEYS */;


-- Structuur van  tabel vagrant.Group_Members wordt geschreven
CREATE TABLE IF NOT EXISTS `Group_Members` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `GroupID` int(11) NOT NULL DEFAULT '0',
  `MemberID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `GroupID` (`GroupID`),
  KEY `MemberID` (`MemberID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.Group_Members: ~0 rows (ongeveer)
DELETE FROM `Group_Members`;
/*!40000 ALTER TABLE `Group_Members` DISABLE KEYS */;
INSERT INTO `Group_Members` (`ID`, `GroupID`, `MemberID`) VALUES
	(1, 2, 1);
/*!40000 ALTER TABLE `Group_Members` ENABLE KEYS */;


-- Structuur van  tabel vagrant.Group_Roles wordt geschreven
CREATE TABLE IF NOT EXISTS `Group_Roles` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `GroupID` int(11) NOT NULL DEFAULT '0',
  `PermissionRoleID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `GroupID` (`GroupID`),
  KEY `PermissionRoleID` (`PermissionRoleID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.Group_Roles: ~0 rows (ongeveer)
DELETE FROM `Group_Roles`;
/*!40000 ALTER TABLE `Group_Roles` DISABLE KEYS */;
/*!40000 ALTER TABLE `Group_Roles` ENABLE KEYS */;


-- Structuur van  tabel vagrant.HestecElementCard wordt geschreven
CREATE TABLE IF NOT EXISTS `HestecElementCard` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `IntroText` mediumtext CHARACTER SET utf8,
  `ContentText` mediumtext CHARACTER SET utf8,
  `FooterText` mediumtext CHARACTER SET utf8,
  `LinkTitle` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `LinkUrlID` int(11) NOT NULL DEFAULT '0',
  `LinkParams` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `LinkUrlID` (`LinkUrlID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.HestecElementCard: ~0 rows (ongeveer)
DELETE FROM `HestecElementCard`;
/*!40000 ALTER TABLE `HestecElementCard` DISABLE KEYS */;
/*!40000 ALTER TABLE `HestecElementCard` ENABLE KEYS */;


-- Structuur van  tabel vagrant.HestecElementCard_Live wordt geschreven
CREATE TABLE IF NOT EXISTS `HestecElementCard_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `IntroText` mediumtext CHARACTER SET utf8,
  `ContentText` mediumtext CHARACTER SET utf8,
  `FooterText` mediumtext CHARACTER SET utf8,
  `LinkTitle` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `LinkUrlID` int(11) NOT NULL DEFAULT '0',
  `LinkParams` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `LinkUrlID` (`LinkUrlID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.HestecElementCard_Live: ~0 rows (ongeveer)
DELETE FROM `HestecElementCard_Live`;
/*!40000 ALTER TABLE `HestecElementCard_Live` DISABLE KEYS */;
INSERT INTO `HestecElementCard_Live` (`ID`, `IntroText`, `ContentText`, `FooterText`, `LinkTitle`, `LinkUrlID`, `LinkParams`) VALUES
	(6, NULL, NULL, NULL, NULL, 0, NULL);
/*!40000 ALTER TABLE `HestecElementCard_Live` ENABLE KEYS */;


-- Structuur van  tabel vagrant.HestecElementCard_Versions wordt geschreven
CREATE TABLE IF NOT EXISTS `HestecElementCard_Versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `IntroText` mediumtext CHARACTER SET utf8,
  `ContentText` mediumtext CHARACTER SET utf8,
  `FooterText` mediumtext CHARACTER SET utf8,
  `LinkTitle` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `LinkUrlID` int(11) NOT NULL DEFAULT '0',
  `LinkParams` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`),
  KEY `LinkUrlID` (`LinkUrlID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.HestecElementCard_Versions: ~5 rows (ongeveer)
DELETE FROM `HestecElementCard_Versions`;
/*!40000 ALTER TABLE `HestecElementCard_Versions` DISABLE KEYS */;
INSERT INTO `HestecElementCard_Versions` (`ID`, `RecordID`, `Version`, `IntroText`, `ContentText`, `FooterText`, `LinkTitle`, `LinkUrlID`, `LinkParams`) VALUES
	(1, 6, 1, NULL, NULL, NULL, NULL, 0, NULL),
	(2, 6, 2, NULL, NULL, NULL, NULL, 0, NULL),
	(3, 6, 3, NULL, NULL, NULL, NULL, 0, NULL),
	(4, 6, 4, NULL, NULL, NULL, NULL, 0, NULL),
	(5, 6, 5, NULL, NULL, NULL, NULL, 0, NULL);
/*!40000 ALTER TABLE `HestecElementCard_Versions` ENABLE KEYS */;


-- Structuur van  tabel vagrant.HestecElementContentImage wordt geschreven
CREATE TABLE IF NOT EXISTS `HestecElementContentImage` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ImageID` int(11) NOT NULL DEFAULT '0',
  `Content` mediumtext CHARACTER SET utf8,
  `ImagePosition` enum('left','right') CHARACTER SET utf8 DEFAULT 'left',
  PRIMARY KEY (`ID`),
  KEY `ImageID` (`ImageID`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.HestecElementContentImage: ~1 rows (ongeveer)
DELETE FROM `HestecElementContentImage`;
/*!40000 ALTER TABLE `HestecElementContentImage` DISABLE KEYS */;
INSERT INTO `HestecElementContentImage` (`ID`, `ImageID`, `Content`, `ImagePosition`) VALUES
	(8, 3, '<p>Dit is een faketekst. Alles wat hier staat is slechts om een indruk te geven van het grafische effect van tekst op deze plek. Wat u hier leest is een voorbeeldtekst. Deze wordt later vervangen door de uiteindelijke tekst, die nu nog niet bekend is. De faketekst is dus een tekst die eigenlijk nergens over gaat. Het grappige is, dat mensen deze toch vaak lezen. Zelfs als men weet dat het om een faketekst gaat, lezen ze toch door.</p>', 'right');
/*!40000 ALTER TABLE `HestecElementContentImage` ENABLE KEYS */;


-- Structuur van  tabel vagrant.HestecElementContentImage_Live wordt geschreven
CREATE TABLE IF NOT EXISTS `HestecElementContentImage_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ImageID` int(11) NOT NULL DEFAULT '0',
  `Content` mediumtext CHARACTER SET utf8,
  `ImagePosition` enum('left','right') CHARACTER SET utf8 DEFAULT 'left',
  PRIMARY KEY (`ID`),
  KEY `ImageID` (`ImageID`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.HestecElementContentImage_Live: ~1 rows (ongeveer)
DELETE FROM `HestecElementContentImage_Live`;
/*!40000 ALTER TABLE `HestecElementContentImage_Live` DISABLE KEYS */;
INSERT INTO `HestecElementContentImage_Live` (`ID`, `ImageID`, `Content`, `ImagePosition`) VALUES
	(8, 3, '<p>Dit is een faketekst. Alles wat hier staat is slechts om een indruk te geven van het grafische effect van tekst op deze plek. Wat u hier leest is een voorbeeldtekst. Deze wordt later vervangen door de uiteindelijke tekst, die nu nog niet bekend is. De faketekst is dus een tekst die eigenlijk nergens over gaat. Het grappige is, dat mensen deze toch vaak lezen. Zelfs als men weet dat het om een faketekst gaat, lezen ze toch door.</p>', 'right');
/*!40000 ALTER TABLE `HestecElementContentImage_Live` ENABLE KEYS */;


-- Structuur van  tabel vagrant.HestecElementContentImage_Versions wordt geschreven
CREATE TABLE IF NOT EXISTS `HestecElementContentImage_Versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `ImageID` int(11) NOT NULL DEFAULT '0',
  `Content` mediumtext CHARACTER SET utf8,
  `ImagePosition` enum('left','right') CHARACTER SET utf8 DEFAULT 'left',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`),
  KEY `ImageID` (`ImageID`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.HestecElementContentImage_Versions: ~19 rows (ongeveer)
DELETE FROM `HestecElementContentImage_Versions`;
/*!40000 ALTER TABLE `HestecElementContentImage_Versions` DISABLE KEYS */;
INSERT INTO `HestecElementContentImage_Versions` (`ID`, `RecordID`, `Version`, `ImageID`, `Content`, `ImagePosition`) VALUES
	(1, 8, 1, 0, '<p>Test</p>', 'left'),
	(2, 8, 2, 0, '<p>Test</p>', 'left'),
	(3, 8, 3, 0, '<p>Test</p>', 'left'),
	(4, 8, 4, 0, '<p>Test</p>', 'left'),
	(5, 8, 5, 0, '<p>Test</p>', 'left'),
	(6, 8, 6, 0, '<p>Test</p>', 'left'),
	(7, 8, 7, 0, '<p>Test</p>', 'left'),
	(8, 8, 8, 2, '<p>Test</p>', 'left'),
	(9, 8, 9, 2, '<p>Test</p>', 'left'),
	(10, 8, 10, 2, '<p>Test</p>', 'left'),
	(11, 8, 11, 2, '<p>Dit is een faketekst. Alles wat hier staat is slechts om een indruk te geven van het grafische effect van tekst op deze plek. Wat u hier leest is een voorbeeldtekst. Deze wordt later vervangen door de uiteindelijke tekst, die nu nog niet bekend is. De faketekst is dus een tekst die eigenlijk nergens over gaat. Het grappige is, dat mensen deze toch vaak lezen. Zelfs als men weet dat het om een faketekst gaat, lezen ze toch door.</p>', 'left'),
	(12, 8, 12, 2, '<p>Dit is een faketekst. Alles wat hier staat is slechts om een indruk te geven van het grafische effect van tekst op deze plek. Wat u hier leest is een voorbeeldtekst. Deze wordt later vervangen door de uiteindelijke tekst, die nu nog niet bekend is. De faketekst is dus een tekst die eigenlijk nergens over gaat. Het grappige is, dat mensen deze toch vaak lezen. Zelfs als men weet dat het om een faketekst gaat, lezen ze toch door.</p>', 'left'),
	(13, 8, 13, 2, '<p>Dit is een faketekst. Alles wat hier staat is slechts om een indruk te geven van het grafische effect van tekst op deze plek. Wat u hier leest is een voorbeeldtekst. Deze wordt later vervangen door de uiteindelijke tekst, die nu nog niet bekend is. De faketekst is dus een tekst die eigenlijk nergens over gaat. Het grappige is, dat mensen deze toch vaak lezen. Zelfs als men weet dat het om een faketekst gaat, lezen ze toch door.</p>', 'left'),
	(14, 8, 14, 2, '<p>Dit is een faketekst. Alles wat hier staat is slechts om een indruk te geven van het grafische effect van tekst op deze plek. Wat u hier leest is een voorbeeldtekst. Deze wordt later vervangen door de uiteindelijke tekst, die nu nog niet bekend is. De faketekst is dus een tekst die eigenlijk nergens over gaat. Het grappige is, dat mensen deze toch vaak lezen. Zelfs als men weet dat het om een faketekst gaat, lezen ze toch door.</p>', 'right'),
	(15, 8, 15, 2, '<p>Dit is een faketekst. Alles wat hier staat is slechts om een indruk te geven van het grafische effect van tekst op deze plek. Wat u hier leest is een voorbeeldtekst. Deze wordt later vervangen door de uiteindelijke tekst, die nu nog niet bekend is. De faketekst is dus een tekst die eigenlijk nergens over gaat. Het grappige is, dat mensen deze toch vaak lezen. Zelfs als men weet dat het om een faketekst gaat, lezen ze toch door.</p>', 'right'),
	(16, 8, 16, 2, '<p>Dit is een faketekst. Alles wat hier staat is slechts om een indruk te geven van het grafische effect van tekst op deze plek. Wat u hier leest is een voorbeeldtekst. Deze wordt later vervangen door de uiteindelijke tekst, die nu nog niet bekend is. De faketekst is dus een tekst die eigenlijk nergens over gaat. Het grappige is, dat mensen deze toch vaak lezen. Zelfs als men weet dat het om een faketekst gaat, lezen ze toch door.</p>', 'right'),
	(17, 8, 17, 0, '<p>Dit is een faketekst. Alles wat hier staat is slechts om een indruk te geven van het grafische effect van tekst op deze plek. Wat u hier leest is een voorbeeldtekst. Deze wordt later vervangen door de uiteindelijke tekst, die nu nog niet bekend is. De faketekst is dus een tekst die eigenlijk nergens over gaat. Het grappige is, dat mensen deze toch vaak lezen. Zelfs als men weet dat het om een faketekst gaat, lezen ze toch door.</p>', 'right'),
	(18, 8, 18, 0, '<p>Dit is een faketekst. Alles wat hier staat is slechts om een indruk te geven van het grafische effect van tekst op deze plek. Wat u hier leest is een voorbeeldtekst. Deze wordt later vervangen door de uiteindelijke tekst, die nu nog niet bekend is. De faketekst is dus een tekst die eigenlijk nergens over gaat. Het grappige is, dat mensen deze toch vaak lezen. Zelfs als men weet dat het om een faketekst gaat, lezen ze toch door.</p>', 'right'),
	(19, 8, 19, 0, '<p>Dit is een faketekst. Alles wat hier staat is slechts om een indruk te geven van het grafische effect van tekst op deze plek. Wat u hier leest is een voorbeeldtekst. Deze wordt later vervangen door de uiteindelijke tekst, die nu nog niet bekend is. De faketekst is dus een tekst die eigenlijk nergens over gaat. Het grappige is, dat mensen deze toch vaak lezen. Zelfs als men weet dat het om een faketekst gaat, lezen ze toch door.</p>', 'right'),
	(20, 8, 20, 3, '<p>Dit is een faketekst. Alles wat hier staat is slechts om een indruk te geven van het grafische effect van tekst op deze plek. Wat u hier leest is een voorbeeldtekst. Deze wordt later vervangen door de uiteindelijke tekst, die nu nog niet bekend is. De faketekst is dus een tekst die eigenlijk nergens over gaat. Het grappige is, dat mensen deze toch vaak lezen. Zelfs als men weet dat het om een faketekst gaat, lezen ze toch door.</p>', 'right'),
	(21, 8, 21, 3, '<p>Dit is een faketekst. Alles wat hier staat is slechts om een indruk te geven van het grafische effect van tekst op deze plek. Wat u hier leest is een voorbeeldtekst. Deze wordt later vervangen door de uiteindelijke tekst, die nu nog niet bekend is. De faketekst is dus een tekst die eigenlijk nergens over gaat. Het grappige is, dat mensen deze toch vaak lezen. Zelfs als men weet dat het om een faketekst gaat, lezen ze toch door.</p>', 'right'),
	(22, 8, 22, 3, '<p>Dit is een faketekst. Alles wat hier staat is slechts om een indruk te geven van het grafische effect van tekst op deze plek. Wat u hier leest is een voorbeeldtekst. Deze wordt later vervangen door de uiteindelijke tekst, die nu nog niet bekend is. De faketekst is dus een tekst die eigenlijk nergens over gaat. Het grappige is, dat mensen deze toch vaak lezen. Zelfs als men weet dat het om een faketekst gaat, lezen ze toch door.</p>', 'right');
/*!40000 ALTER TABLE `HestecElementContentImage_Versions` ENABLE KEYS */;


-- Structuur van  tabel vagrant.HestecElementFaq wordt geschreven
CREATE TABLE IF NOT EXISTS `HestecElementFaq` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `IntroText` mediumtext CHARACTER SET utf8,
  `FAQPageID` int(11) NOT NULL DEFAULT '0',
  `FAQCategorieID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `FAQPageID` (`FAQPageID`),
  KEY `FAQCategorieID` (`FAQCategorieID`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.HestecElementFaq: ~0 rows (ongeveer)
DELETE FROM `HestecElementFaq`;
/*!40000 ALTER TABLE `HestecElementFaq` DISABLE KEYS */;
INSERT INTO `HestecElementFaq` (`ID`, `IntroText`, `FAQPageID`, `FAQCategorieID`) VALUES
	(7, NULL, 8, 2);
/*!40000 ALTER TABLE `HestecElementFaq` ENABLE KEYS */;


-- Structuur van  tabel vagrant.HestecElementFaqQuestion wordt geschreven
CREATE TABLE IF NOT EXISTS `HestecElementFaqQuestion` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('Hestec\\ElementalExtensions\\Dataobjects\\FaqQuestion') CHARACTER SET utf8 DEFAULT 'Hestec\\ElementalExtensions\\Dataobjects\\FaqQuestion',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Question` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Answer` mediumtext CHARACTER SET utf8,
  `Sort` int(11) NOT NULL DEFAULT '0',
  `ElementFaqID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `Sort` (`Sort`),
  KEY `ClassName` (`ClassName`),
  KEY `ElementFaqID` (`ElementFaqID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.HestecElementFaqQuestion: ~2 rows (ongeveer)
DELETE FROM `HestecElementFaqQuestion`;
/*!40000 ALTER TABLE `HestecElementFaqQuestion` DISABLE KEYS */;
INSERT INTO `HestecElementFaqQuestion` (`ID`, `ClassName`, `LastEdited`, `Created`, `Question`, `Answer`, `Sort`, `ElementFaqID`) VALUES
	(1, 'Hestec\\ElementalExtensions\\Dataobjects\\FaqQuestion', '2018-07-10 16:49:46', '2018-07-10 16:41:10', 'Hoe laat is het?', '<p>Het is nu 16:40, oftwel 10 minuten over half 5.</p>', 2, 7),
	(2, 'Hestec\\ElementalExtensions\\Dataobjects\\FaqQuestion', '2018-07-10 16:49:46', '2018-07-10 16:49:24', 'Welke datum is het vandaag?', '<p>Het is vandaag 10 juli 2018.</p>', 1, 7);
/*!40000 ALTER TABLE `HestecElementFaqQuestion` ENABLE KEYS */;


-- Structuur van  tabel vagrant.HestecElementFaq_Live wordt geschreven
CREATE TABLE IF NOT EXISTS `HestecElementFaq_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `IntroText` mediumtext CHARACTER SET utf8,
  `FAQPageID` int(11) NOT NULL DEFAULT '0',
  `FAQCategorieID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `FAQPageID` (`FAQPageID`),
  KEY `FAQCategorieID` (`FAQCategorieID`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.HestecElementFaq_Live: ~0 rows (ongeveer)
DELETE FROM `HestecElementFaq_Live`;
/*!40000 ALTER TABLE `HestecElementFaq_Live` DISABLE KEYS */;
INSERT INTO `HestecElementFaq_Live` (`ID`, `IntroText`, `FAQPageID`, `FAQCategorieID`) VALUES
	(7, NULL, 8, 2);
/*!40000 ALTER TABLE `HestecElementFaq_Live` ENABLE KEYS */;


-- Structuur van  tabel vagrant.HestecElementFaq_Versions wordt geschreven
CREATE TABLE IF NOT EXISTS `HestecElementFaq_Versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `IntroText` mediumtext CHARACTER SET utf8,
  `FAQPageID` int(11) NOT NULL DEFAULT '0',
  `FAQCategorieID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`),
  KEY `FAQPageID` (`FAQPageID`),
  KEY `FAQCategorieID` (`FAQCategorieID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.HestecElementFaq_Versions: ~6 rows (ongeveer)
DELETE FROM `HestecElementFaq_Versions`;
/*!40000 ALTER TABLE `HestecElementFaq_Versions` DISABLE KEYS */;
INSERT INTO `HestecElementFaq_Versions` (`ID`, `RecordID`, `Version`, `IntroText`, `FAQPageID`, `FAQCategorieID`) VALUES
	(1, 7, 6, NULL, 0, 0),
	(2, 7, 7, NULL, 0, 0),
	(3, 7, 8, NULL, 8, 2),
	(4, 7, 9, NULL, 8, 2),
	(5, 7, 10, NULL, 8, 2),
	(6, 7, 11, NULL, 8, 2);
/*!40000 ALTER TABLE `HestecElementFaq_Versions` ENABLE KEYS */;


-- Structuur van  tabel vagrant.HestecElementVideo wordt geschreven
CREATE TABLE IF NOT EXISTS `HestecElementVideo` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `YoutubeUrl` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `AboveVideoText` mediumtext CHARACTER SET utf8,
  `BelowVideoText` mediumtext CHARACTER SET utf8,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.HestecElementVideo: ~0 rows (ongeveer)
DELETE FROM `HestecElementVideo`;
/*!40000 ALTER TABLE `HestecElementVideo` DISABLE KEYS */;
INSERT INTO `HestecElementVideo` (`ID`, `YoutubeUrl`, `AboveVideoText`, `BelowVideoText`) VALUES
	(5, 'https://www.youtube.com/watch?v=G3KahoOitqU', NULL, NULL);
/*!40000 ALTER TABLE `HestecElementVideo` ENABLE KEYS */;


-- Structuur van  tabel vagrant.HestecElementVideo_Live wordt geschreven
CREATE TABLE IF NOT EXISTS `HestecElementVideo_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `YoutubeUrl` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `AboveVideoText` mediumtext CHARACTER SET utf8,
  `BelowVideoText` mediumtext CHARACTER SET utf8,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.HestecElementVideo_Live: ~0 rows (ongeveer)
DELETE FROM `HestecElementVideo_Live`;
/*!40000 ALTER TABLE `HestecElementVideo_Live` DISABLE KEYS */;
INSERT INTO `HestecElementVideo_Live` (`ID`, `YoutubeUrl`, `AboveVideoText`, `BelowVideoText`) VALUES
	(5, 'https://www.youtube.com/watch?v=G3KahoOitqU', NULL, NULL);
/*!40000 ALTER TABLE `HestecElementVideo_Live` ENABLE KEYS */;


-- Structuur van  tabel vagrant.HestecElementVideo_Versions wordt geschreven
CREATE TABLE IF NOT EXISTS `HestecElementVideo_Versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `YoutubeUrl` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `AboveVideoText` mediumtext CHARACTER SET utf8,
  `BelowVideoText` mediumtext CHARACTER SET utf8,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.HestecElementVideo_Versions: ~5 rows (ongeveer)
DELETE FROM `HestecElementVideo_Versions`;
/*!40000 ALTER TABLE `HestecElementVideo_Versions` DISABLE KEYS */;
INSERT INTO `HestecElementVideo_Versions` (`ID`, `RecordID`, `Version`, `YoutubeUrl`, `AboveVideoText`, `BelowVideoText`) VALUES
	(1, 5, 1, 'https://www.youtube.com/watch?v=cBd6i2MOCpA', NULL, NULL),
	(2, 5, 2, 'https://www.youtube.com/watch?v=cBd6i2MOCpA', NULL, NULL),
	(3, 5, 3, 'https://www.youtube.com/watch?v=cBd6i2MOCpA', NULL, NULL),
	(4, 5, 4, 'https://www.youtube.com/watch?v=G3KahoOitqU', NULL, NULL),
	(5, 5, 5, 'https://www.youtube.com/watch?v=G3KahoOitqU', NULL, NULL);
/*!40000 ALTER TABLE `HestecElementVideo_Versions` ENABLE KEYS */;


-- Structuur van  tabel vagrant.HestecEvent wordt geschreven
CREATE TABLE IF NOT EXISTS `HestecEvent` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('Hestec\\UpcomingEvents\\Event') CHARACTER SET utf8 DEFAULT 'Hestec\\UpcomingEvents\\Event',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Description` mediumtext CHARACTER SET utf8,
  `DateTime` datetime DEFAULT NULL,
  `EventsPageID` int(11) NOT NULL DEFAULT '0',
  `ImageID` int(11) NOT NULL DEFAULT '0',
  `PriceCurrency` varchar(3) CHARACTER SET utf8 DEFAULT NULL,
  `PriceAmount` decimal(19,4) NOT NULL DEFAULT '0.0000',
  `Location` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ClassName` (`ClassName`),
  KEY `EventsPageID` (`EventsPageID`),
  KEY `ImageID` (`ImageID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.HestecEvent: ~0 rows (ongeveer)
DELETE FROM `HestecEvent`;
/*!40000 ALTER TABLE `HestecEvent` DISABLE KEYS */;
INSERT INTO `HestecEvent` (`ID`, `ClassName`, `LastEdited`, `Created`, `Title`, `Description`, `DateTime`, `EventsPageID`, `ImageID`, `PriceCurrency`, `PriceAmount`, `Location`) VALUES
	(1, 'Hestec\\UpcomingEvents\\Event', '2018-04-04 11:25:08', '2018-04-04 11:25:08', 'Test', '<p>Dit is een test</p>', '2018-04-11 10:30:00', 7, 0, 'EUR', 112.0000, NULL);
/*!40000 ALTER TABLE `HestecEvent` ENABLE KEYS */;


-- Structuur van  tabel vagrant.HestecFaqCategorie wordt geschreven
CREATE TABLE IF NOT EXISTS `HestecFaqCategorie` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('Hestec\\FaqPage\\FaqCategorie','','TheWebmen\\FAQ\\Model\\FAQCategorie') CHARACTER SET utf8 DEFAULT 'Hestec\\FaqPage\\FaqCategorie',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Sort` int(11) NOT NULL DEFAULT '0',
  `PageID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `Sort` (`Sort`),
  KEY `ClassName` (`ClassName`),
  KEY `PageID` (`PageID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.HestecFaqCategorie: ~2 rows (ongeveer)
DELETE FROM `HestecFaqCategorie`;
/*!40000 ALTER TABLE `HestecFaqCategorie` DISABLE KEYS */;
INSERT INTO `HestecFaqCategorie` (`ID`, `ClassName`, `LastEdited`, `Created`, `Title`, `Sort`, `PageID`) VALUES
	(3, 'Hestec\\FaqPage\\FaqCategorie', '2018-09-18 10:35:18', '2018-09-18 10:35:18', 'Algemeen', 0, 8),
	(4, 'Hestec\\FaqPage\\FaqCategorie', '2018-09-18 10:37:33', '2018-09-18 10:37:33', 'Financieel', 0, 8);
/*!40000 ALTER TABLE `HestecFaqCategorie` ENABLE KEYS */;


-- Structuur van  tabel vagrant.HestecFaqQuestion wordt geschreven
CREATE TABLE IF NOT EXISTS `HestecFaqQuestion` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('Hestec\\FaqPage\\FaqQuestion','TheWebmen\\FAQ\\Model\\FAQQuestion') CHARACTER SET utf8 DEFAULT 'Hestec\\FaqPage\\FaqQuestion',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Question` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Answer` mediumtext CHARACTER SET utf8,
  `Sort` int(11) NOT NULL DEFAULT '0',
  `CategoryID` int(11) NOT NULL DEFAULT '0',
  `PageID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `Sort` (`Sort`),
  KEY `ClassName` (`ClassName`),
  KEY `CategoryID` (`CategoryID`),
  KEY `PageID` (`PageID`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.HestecFaqQuestion: ~4 rows (ongeveer)
DELETE FROM `HestecFaqQuestion`;
/*!40000 ALTER TABLE `HestecFaqQuestion` DISABLE KEYS */;
INSERT INTO `HestecFaqQuestion` (`ID`, `ClassName`, `LastEdited`, `Created`, `Question`, `Answer`, `Sort`, `CategoryID`, `PageID`) VALUES
	(6, 'Hestec\\FaqPage\\FaqQuestion', '2018-09-18 10:36:11', '2018-09-18 10:36:11', 'Is dit een faketekst?', '<p>Dit is een faketekst. Alles wat hier staat is slechts om een indruk te geven van het grafische effect van tekst op deze plek. Wat u hier leest is een voorbeeldtekst. Deze wordt later vervangen door de uiteindelijke tekst, die nu nog niet bekend is. De faketekst is dus een tekst die eigenlijk nergens over gaat. Het grappige is, dat mensen deze toch vaak lezen. Zelfs als men weet dat het om een faketekst gaat, lezen ze toch door.</p>', 0, 3, 0),
	(7, 'Hestec\\FaqPage\\FaqQuestion', '2018-09-18 10:36:56', '2018-09-18 10:36:56', 'Worden deze vragen later vervangen door de uiteindelijke tekst?', '<p>Dit is een faketekst. Alles wat hier staat is slechts om een indruk te geven van het grafische effect van tekst op deze plek. Wat u hier leest is een voorbeeldtekst. Deze wordt later vervangen door de uiteindelijke tekst, die nu nog niet bekend is. De faketekst is dus een tekst die eigenlijk nergens over gaat. Het grappige is, dat mensen deze toch vaak lezen. Zelfs als men weet dat het om een faketekst gaat, lezen ze toch door.</p>', 0, 3, 0),
	(8, 'Hestec\\FaqPage\\FaqQuestion', '2018-09-18 10:38:55', '2018-09-18 10:38:11', 'Hoe kan ik mijn factuur betalen?', '<p>Dit is een faketekst. Alles wat hier staat is slechts om een indruk te geven van het grafische effect van tekst op deze plek. Wat u hier leest is een voorbeeldtekst. Deze wordt later vervangen door de uiteindelijke tekst, die nu nog niet bekend is. De faketekst is dus een tekst die eigenlijk nergens over gaat. Het grappige is, dat mensen deze toch vaak lezen. Zelfs als men weet dat het om een faketekst gaat, lezen ze toch door.</p>', 0, 4, 0),
	(9, 'Hestec\\FaqPage\\FaqQuestion', '2018-09-18 10:38:37', '2018-09-18 10:38:37', 'Is de faketekst een tekst die eigenlijk nergens over gaat?', '<p>Dit is een faketekst. Alles wat hier staat is slechts om een indruk te geven van het grafische effect van tekst op deze plek. Wat u hier leest is een voorbeeldtekst. Deze wordt later vervangen door de uiteindelijke tekst, die nu nog niet bekend is. De faketekst is dus een tekst die eigenlijk nergens over gaat. Het grappige is, dat mensen deze toch vaak lezen. Zelfs als men weet dat het om een faketekst gaat, lezen ze toch door.</p>', 0, 4, 0);
/*!40000 ALTER TABLE `HestecFaqQuestion` ENABLE KEYS */;


-- Structuur van  tabel vagrant.HestecSisowMethod wordt geschreven
CREATE TABLE IF NOT EXISTS `HestecSisowMethod` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('Hestec\\SisowMethod\\SisowMethod') CHARACTER SET utf8 DEFAULT 'Hestec\\SisowMethod\\SisowMethod',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `MethodKey` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `MethodTitle` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Enabled` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `SortOrder` int(11) NOT NULL DEFAULT '0',
  `SiteConfigID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `SortOrder` (`SortOrder`),
  KEY `ClassName` (`ClassName`),
  KEY `SiteConfigID` (`SiteConfigID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.HestecSisowMethod: ~0 rows (ongeveer)
DELETE FROM `HestecSisowMethod`;
/*!40000 ALTER TABLE `HestecSisowMethod` DISABLE KEYS */;
/*!40000 ALTER TABLE `HestecSisowMethod` ENABLE KEYS */;


-- Structuur van  tabel vagrant.Hestec_Tools_Elements_ElementVideo wordt geschreven
CREATE TABLE IF NOT EXISTS `Hestec_Tools_Elements_ElementVideo` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `YoutubeUrl` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `AboveVideoText` mediumtext CHARACTER SET utf8,
  `BelowVideoText` mediumtext CHARACTER SET utf8,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.Hestec_Tools_Elements_ElementVideo: ~0 rows (ongeveer)
DELETE FROM `Hestec_Tools_Elements_ElementVideo`;
/*!40000 ALTER TABLE `Hestec_Tools_Elements_ElementVideo` DISABLE KEYS */;
INSERT INTO `Hestec_Tools_Elements_ElementVideo` (`ID`, `YoutubeUrl`, `AboveVideoText`, `BelowVideoText`) VALUES
	(4, 'https://www.youtube.com/watch?v=7LbUETij47s', '<p>Text boven de video</p>', '<p>text onder de video</p>');
/*!40000 ALTER TABLE `Hestec_Tools_Elements_ElementVideo` ENABLE KEYS */;


-- Structuur van  tabel vagrant.Hestec_Tools_Elements_ElementVideo_Live wordt geschreven
CREATE TABLE IF NOT EXISTS `Hestec_Tools_Elements_ElementVideo_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `YoutubeUrl` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `AboveVideoText` mediumtext CHARACTER SET utf8,
  `BelowVideoText` mediumtext CHARACTER SET utf8,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.Hestec_Tools_Elements_ElementVideo_Live: ~0 rows (ongeveer)
DELETE FROM `Hestec_Tools_Elements_ElementVideo_Live`;
/*!40000 ALTER TABLE `Hestec_Tools_Elements_ElementVideo_Live` DISABLE KEYS */;
INSERT INTO `Hestec_Tools_Elements_ElementVideo_Live` (`ID`, `YoutubeUrl`, `AboveVideoText`, `BelowVideoText`) VALUES
	(4, 'https://www.youtube.com/watch?v=7LbUETij47s', '<p>Text boven de video</p>', '<p>text onder de video</p>');
/*!40000 ALTER TABLE `Hestec_Tools_Elements_ElementVideo_Live` ENABLE KEYS */;


-- Structuur van  tabel vagrant.Hestec_Tools_Elements_ElementVideo_Versions wordt geschreven
CREATE TABLE IF NOT EXISTS `Hestec_Tools_Elements_ElementVideo_Versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `YoutubeUrl` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `AboveVideoText` mediumtext CHARACTER SET utf8,
  `BelowVideoText` mediumtext CHARACTER SET utf8,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.Hestec_Tools_Elements_ElementVideo_Versions: ~5 rows (ongeveer)
DELETE FROM `Hestec_Tools_Elements_ElementVideo_Versions`;
/*!40000 ALTER TABLE `Hestec_Tools_Elements_ElementVideo_Versions` DISABLE KEYS */;
INSERT INTO `Hestec_Tools_Elements_ElementVideo_Versions` (`ID`, `RecordID`, `Version`, `YoutubeUrl`, `AboveVideoText`, `BelowVideoText`) VALUES
	(1, 4, 1, 'https://www.youtube.com/watch?v=7LbUETij47s', NULL, NULL),
	(2, 4, 2, 'https://www.youtube.com/watch?v=7LbUETij47s', NULL, NULL),
	(3, 4, 3, 'https://www.youtube.com/watch?v=7LbUETij47s', NULL, NULL),
	(4, 4, 4, 'https://www.youtube.com/watch?v=7LbUETij47s', '<p>Text boven de video</p>', '<p>text onder de video</p>'),
	(5, 4, 5, 'https://www.youtube.com/watch?v=7LbUETij47s', '<p>Text boven de video</p>', '<p>text onder de video</p>');
/*!40000 ALTER TABLE `Hestec_Tools_Elements_ElementVideo_Versions` ENABLE KEYS */;


-- Structuur van  tabel vagrant.Link wordt geschreven
CREATE TABLE IF NOT EXISTS `Link` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('gorriecoe\\Link\\Models\\Link','gorriecoe\\Menu\\Models\\MenuLink') CHARACTER SET utf8 DEFAULT 'gorriecoe\\Link\\Models\\Link',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Type` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `URL` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Email` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Phone` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `OpenInNewWindow` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Template` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Anchor` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `FileID` int(11) NOT NULL DEFAULT '0',
  `SiteTreeID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ClassName` (`ClassName`),
  KEY `FileID` (`FileID`),
  KEY `SiteTreeID` (`SiteTreeID`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.Link: ~8 rows (ongeveer)
DELETE FROM `Link`;
/*!40000 ALTER TABLE `Link` DISABLE KEYS */;
INSERT INTO `Link` (`ID`, `ClassName`, `LastEdited`, `Created`, `Title`, `Type`, `URL`, `Email`, `Phone`, `OpenInNewWindow`, `Template`, `Anchor`, `FileID`, `SiteTreeID`) VALUES
	(1, 'gorriecoe\\Menu\\Models\\MenuLink', '2018-07-06 12:41:09', '2018-07-06 12:41:09', 'Home', 'SiteTree', NULL, NULL, NULL, 0, NULL, NULL, 0, 1),
	(2, 'gorriecoe\\Menu\\Models\\MenuLink', '2018-07-06 12:41:27', '2018-07-06 12:41:27', 'Over ons', 'SiteTree', NULL, NULL, NULL, 0, NULL, NULL, 0, 2),
	(3, 'gorriecoe\\Menu\\Models\\MenuLink', '2018-07-06 12:41:44', '2018-07-06 12:41:44', 'Contact', 'SiteTree', NULL, NULL, NULL, 0, NULL, NULL, 0, 3),
	(4, 'gorriecoe\\Menu\\Models\\MenuLink', '2018-07-06 13:56:58', '2018-07-06 13:56:58', 'Privacyverklaring', 'SiteTree', NULL, NULL, NULL, 0, NULL, NULL, 0, 9),
	(5, 'gorriecoe\\Menu\\Models\\MenuLink', '2018-07-06 13:57:35', '2018-07-06 13:57:35', 'Veelgestelde vragen', 'SiteTree', NULL, NULL, NULL, 0, NULL, NULL, 0, 8),
	(6, 'gorriecoe\\Menu\\Models\\MenuLink', '2018-07-06 13:59:43', '2018-07-06 13:58:14', 'About Us', 'SiteTree', NULL, NULL, NULL, 0, NULL, NULL, 0, 2),
	(7, 'gorriecoe\\Menu\\Models\\MenuLink', '2018-07-06 13:59:43', '2018-07-06 13:58:41', 'Hestec', 'URL', 'https://www.hestec.nl', NULL, NULL, 1, NULL, NULL, 0, 0),
	(8, 'gorriecoe\\Menu\\Models\\MenuLink', '2018-07-06 13:59:43', '2018-07-06 13:59:20', 'info@ss-boilerplate.hst1.nl', 'Email', NULL, 'info@ss-boilerplate.hst1.nl', NULL, 0, NULL, NULL, 0, 0);
/*!40000 ALTER TABLE `Link` ENABLE KEYS */;


-- Structuur van  tabel vagrant.LoginAttempt wordt geschreven
CREATE TABLE IF NOT EXISTS `LoginAttempt` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('SilverStripe\\Security\\LoginAttempt') CHARACTER SET utf8 DEFAULT 'SilverStripe\\Security\\LoginAttempt',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Email` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `EmailHashed` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Status` enum('Success','Failure') CHARACTER SET utf8 DEFAULT 'Success',
  `IP` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `MemberID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ClassName` (`ClassName`),
  KEY `MemberID` (`MemberID`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.LoginAttempt: ~9 rows (ongeveer)
DELETE FROM `LoginAttempt`;
/*!40000 ALTER TABLE `LoginAttempt` DISABLE KEYS */;
INSERT INTO `LoginAttempt` (`ID`, `ClassName`, `LastEdited`, `Created`, `Email`, `EmailHashed`, `Status`, `IP`, `MemberID`) VALUES
	(1, 'SilverStripe\\Security\\LoginAttempt', '2018-07-04 15:23:32', '2018-07-04 15:23:32', NULL, '33f6c23b5056ed16f0df7d07f7db12dd5a97a222', 'Success', '172.27.146.1', 1),
	(2, 'SilverStripe\\Security\\LoginAttempt', '2018-07-06 12:01:35', '2018-07-06 12:01:35', NULL, '33f6c23b5056ed16f0df7d07f7db12dd5a97a222', 'Success', '172.27.146.1', 1),
	(3, 'SilverStripe\\Security\\LoginAttempt', '2018-07-09 14:46:12', '2018-07-09 14:46:12', NULL, '33f6c23b5056ed16f0df7d07f7db12dd5a97a222', 'Success', '172.27.146.1', 1),
	(4, 'SilverStripe\\Security\\LoginAttempt', '2018-07-10 14:12:32', '2018-07-10 14:12:32', NULL, '33f6c23b5056ed16f0df7d07f7db12dd5a97a222', 'Success', '172.27.146.1', 1),
	(5, 'SilverStripe\\Security\\LoginAttempt', '2018-07-12 12:08:53', '2018-07-12 12:08:53', NULL, '33f6c23b5056ed16f0df7d07f7db12dd5a97a222', 'Success', '172.27.146.1', 1),
	(6, 'SilverStripe\\Security\\LoginAttempt', '2018-07-13 15:27:45', '2018-07-13 15:27:45', NULL, '33f6c23b5056ed16f0df7d07f7db12dd5a97a222', 'Success', '172.27.146.1', 1),
	(7, 'SilverStripe\\Security\\LoginAttempt', '2018-07-16 14:43:36', '2018-07-16 14:43:36', NULL, '33f6c23b5056ed16f0df7d07f7db12dd5a97a222', 'Success', '172.27.146.1', 1),
	(8, 'SilverStripe\\Security\\LoginAttempt', '2018-08-03 09:59:36', '2018-08-03 09:59:36', NULL, '33f6c23b5056ed16f0df7d07f7db12dd5a97a222', 'Success', '172.27.146.1', 1),
	(9, 'SilverStripe\\Security\\LoginAttempt', '2018-08-06 11:56:49', '2018-08-06 11:56:49', NULL, '33f6c23b5056ed16f0df7d07f7db12dd5a97a222', 'Success', '172.27.146.1', 1),
	(10, 'SilverStripe\\Security\\LoginAttempt', '2018-08-07 16:39:07', '2018-08-07 16:39:07', NULL, '33f6c23b5056ed16f0df7d07f7db12dd5a97a222', 'Success', '172.27.146.1', 1),
	(11, 'SilverStripe\\Security\\LoginAttempt', '2018-09-18 10:13:24', '2018-09-18 10:13:24', NULL, '33f6c23b5056ed16f0df7d07f7db12dd5a97a222', 'Success', '172.27.146.1', 1);
/*!40000 ALTER TABLE `LoginAttempt` ENABLE KEYS */;


-- Structuur van  tabel vagrant.Member wordt geschreven
CREATE TABLE IF NOT EXISTS `Member` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('SilverStripe\\Security\\Member') CHARACTER SET utf8 DEFAULT 'SilverStripe\\Security\\Member',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `FirstName` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Surname` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Email` varchar(254) CHARACTER SET utf8 DEFAULT NULL,
  `TempIDHash` varchar(160) CHARACTER SET utf8 DEFAULT NULL,
  `TempIDExpired` datetime DEFAULT NULL,
  `Password` varchar(160) CHARACTER SET utf8 DEFAULT NULL,
  `AutoLoginHash` varchar(160) CHARACTER SET utf8 DEFAULT NULL,
  `AutoLoginExpired` datetime DEFAULT NULL,
  `PasswordEncryption` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `Salt` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `PasswordExpiry` date DEFAULT NULL,
  `LockedOutUntil` datetime DEFAULT NULL,
  `Locale` varchar(6) CHARACTER SET utf8 DEFAULT NULL,
  `FailedLoginCount` int(11) NOT NULL DEFAULT '0',
  `URLSegment` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `BlogProfileSummary` mediumtext CHARACTER SET utf8,
  `BlogProfileImageID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `Surname` (`Surname`),
  KEY `FirstName` (`FirstName`),
  KEY `ClassName` (`ClassName`),
  KEY `Email` (`Email`),
  KEY `BlogProfileImageID` (`BlogProfileImageID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.Member: ~0 rows (ongeveer)
DELETE FROM `Member`;
/*!40000 ALTER TABLE `Member` DISABLE KEYS */;
INSERT INTO `Member` (`ID`, `ClassName`, `LastEdited`, `Created`, `FirstName`, `Surname`, `Email`, `TempIDHash`, `TempIDExpired`, `Password`, `AutoLoginHash`, `AutoLoginExpired`, `PasswordEncryption`, `Salt`, `PasswordExpiry`, `LockedOutUntil`, `Locale`, `FailedLoginCount`, `URLSegment`, `BlogProfileSummary`, `BlogProfileImageID`) VALUES
	(1, 'SilverStripe\\Security\\Member', '2018-09-18 10:13:25', '2018-03-22 17:59:28', 'Default Admin', NULL, 'info@hestec.nl', '5426137706ae06d1451fc56fbb034dbe7ed6e5fd', '2018-09-21 10:13:25', NULL, NULL, NULL, 'none', NULL, NULL, NULL, 'nl_NL', 0, 'default-admin', NULL, 0);
/*!40000 ALTER TABLE `Member` ENABLE KEYS */;


-- Structuur van  tabel vagrant.MemberPassword wordt geschreven
CREATE TABLE IF NOT EXISTS `MemberPassword` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('SilverStripe\\Security\\MemberPassword') CHARACTER SET utf8 DEFAULT 'SilverStripe\\Security\\MemberPassword',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Password` varchar(160) CHARACTER SET utf8 DEFAULT NULL,
  `Salt` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `PasswordEncryption` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `MemberID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ClassName` (`ClassName`),
  KEY `MemberID` (`MemberID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.MemberPassword: ~0 rows (ongeveer)
DELETE FROM `MemberPassword`;
/*!40000 ALTER TABLE `MemberPassword` DISABLE KEYS */;
INSERT INTO `MemberPassword` (`ID`, `ClassName`, `LastEdited`, `Created`, `Password`, `Salt`, `PasswordEncryption`, `MemberID`) VALUES
	(1, 'SilverStripe\\Security\\MemberPassword', '2018-03-22 17:59:28', '2018-03-22 17:59:28', NULL, NULL, 'none', 1);
/*!40000 ALTER TABLE `MemberPassword` ENABLE KEYS */;


-- Structuur van  tabel vagrant.MenuLink wordt geschreven
CREATE TABLE IF NOT EXISTS `MenuLink` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Sort` int(11) NOT NULL DEFAULT '0',
  `MenuSetID` int(11) NOT NULL DEFAULT '0',
  `ParentID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `MenuSetID` (`MenuSetID`),
  KEY `ParentID` (`ParentID`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.MenuLink: ~8 rows (ongeveer)
DELETE FROM `MenuLink`;
/*!40000 ALTER TABLE `MenuLink` DISABLE KEYS */;
INSERT INTO `MenuLink` (`ID`, `Sort`, `MenuSetID`, `ParentID`) VALUES
	(1, 0, 1, 0),
	(2, 0, 1, 0),
	(3, 0, 1, 0),
	(4, 0, 2, 0),
	(5, 0, 4, 0),
	(6, 1, 3, 0),
	(7, 2, 3, 0),
	(8, 3, 3, 0);
/*!40000 ALTER TABLE `MenuLink` ENABLE KEYS */;


-- Structuur van  tabel vagrant.MenuSet wordt geschreven
CREATE TABLE IF NOT EXISTS `MenuSet` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('gorriecoe\\Menu\\Models\\MenuSet') CHARACTER SET utf8 DEFAULT 'gorriecoe\\Menu\\Models\\MenuSet',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Slug` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Nested` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.MenuSet: ~4 rows (ongeveer)
DELETE FROM `MenuSet`;
/*!40000 ALTER TABLE `MenuSet` DISABLE KEYS */;
INSERT INTO `MenuSet` (`ID`, `ClassName`, `LastEdited`, `Created`, `Title`, `Slug`, `Nested`) VALUES
	(1, 'gorriecoe\\Menu\\Models\\MenuSet', '2018-09-18 10:31:05', '2018-07-04 15:32:40', 'Top menu', 'top', 0),
	(2, 'gorriecoe\\Menu\\Models\\MenuSet', '2018-09-18 10:31:05', '2018-07-04 15:32:40', 'Footer column 1', 'footer1', 0),
	(3, 'gorriecoe\\Menu\\Models\\MenuSet', '2018-09-18 10:31:05', '2018-07-04 15:32:40', 'Footer column 2', 'footer2', 0),
	(4, 'gorriecoe\\Menu\\Models\\MenuSet', '2018-09-18 10:31:05', '2018-07-04 15:32:40', 'Footer column 3', 'footer3', 0);
/*!40000 ALTER TABLE `MenuSet` ENABLE KEYS */;


-- Structuur van  tabel vagrant.OwnIp wordt geschreven
CREATE TABLE IF NOT EXISTS `OwnIp` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('Hestec\\Tools\\OwnIp','OwnIp') CHARACTER SET utf8 DEFAULT 'Hestec\\Tools\\OwnIp',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `IpAddress` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `Description` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `SiteConfigID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ClassName` (`ClassName`),
  KEY `SiteConfigID` (`SiteConfigID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.OwnIp: ~0 rows (ongeveer)
DELETE FROM `OwnIp`;
/*!40000 ALTER TABLE `OwnIp` DISABLE KEYS */;
INSERT INTO `OwnIp` (`ID`, `ClassName`, `LastEdited`, `Created`, `IpAddress`, `Description`, `SiteConfigID`) VALUES
	(1, 'OwnIp', '2018-03-26 20:15:00', '2018-03-26 20:15:00', '172.27.146.1', 'Test', 1);
/*!40000 ALTER TABLE `OwnIp` ENABLE KEYS */;


-- Structuur van  tabel vagrant.Permission wordt geschreven
CREATE TABLE IF NOT EXISTS `Permission` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('SilverStripe\\Security\\Permission') CHARACTER SET utf8 DEFAULT 'SilverStripe\\Security\\Permission',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Code` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Arg` int(11) NOT NULL DEFAULT '0',
  `Type` int(11) NOT NULL DEFAULT '1',
  `GroupID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ClassName` (`ClassName`),
  KEY `GroupID` (`GroupID`),
  KEY `Code` (`Code`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.Permission: ~6 rows (ongeveer)
DELETE FROM `Permission`;
/*!40000 ALTER TABLE `Permission` DISABLE KEYS */;
INSERT INTO `Permission` (`ID`, `ClassName`, `LastEdited`, `Created`, `Code`, `Arg`, `Type`, `GroupID`) VALUES
	(1, 'SilverStripe\\Security\\Permission', '2018-03-22 17:59:28', '2018-03-22 17:59:28', 'CMS_ACCESS_CMSMain', 0, 1, 1),
	(2, 'SilverStripe\\Security\\Permission', '2018-03-22 17:59:28', '2018-03-22 17:59:28', 'CMS_ACCESS_AssetAdmin', 0, 1, 1),
	(3, 'SilverStripe\\Security\\Permission', '2018-03-22 17:59:28', '2018-03-22 17:59:28', 'CMS_ACCESS_ReportAdmin', 0, 1, 1),
	(4, 'SilverStripe\\Security\\Permission', '2018-03-22 17:59:28', '2018-03-22 17:59:28', 'SITETREE_REORGANISE', 0, 1, 1),
	(5, 'SilverStripe\\Security\\Permission', '2018-03-22 17:59:28', '2018-03-22 17:59:28', 'ADMIN', 0, 1, 2),
	(6, 'SilverStripe\\Security\\Permission', '2018-04-05 11:04:10', '2018-04-05 11:04:10', 'CMS_ACCESS_CMSMain', 0, 1, 3);
/*!40000 ALTER TABLE `Permission` ENABLE KEYS */;


-- Structuur van  tabel vagrant.PermissionRole wordt geschreven
CREATE TABLE IF NOT EXISTS `PermissionRole` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('SilverStripe\\Security\\PermissionRole') CHARACTER SET utf8 DEFAULT 'SilverStripe\\Security\\PermissionRole',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `OnlyAdminCanApply` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `Title` (`Title`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.PermissionRole: ~0 rows (ongeveer)
DELETE FROM `PermissionRole`;
/*!40000 ALTER TABLE `PermissionRole` DISABLE KEYS */;
/*!40000 ALTER TABLE `PermissionRole` ENABLE KEYS */;


-- Structuur van  tabel vagrant.PermissionRoleCode wordt geschreven
CREATE TABLE IF NOT EXISTS `PermissionRoleCode` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('SilverStripe\\Security\\PermissionRoleCode') CHARACTER SET utf8 DEFAULT 'SilverStripe\\Security\\PermissionRoleCode',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Code` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `RoleID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ClassName` (`ClassName`),
  KEY `RoleID` (`RoleID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.PermissionRoleCode: ~0 rows (ongeveer)
DELETE FROM `PermissionRoleCode`;
/*!40000 ALTER TABLE `PermissionRoleCode` DISABLE KEYS */;
/*!40000 ALTER TABLE `PermissionRoleCode` ENABLE KEYS */;


-- Structuur van  tabel vagrant.RedirectorPage wordt geschreven
CREATE TABLE IF NOT EXISTS `RedirectorPage` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RedirectionType` enum('Internal','External') CHARACTER SET utf8 DEFAULT 'Internal',
  `ExternalURL` varchar(2083) CHARACTER SET utf8 DEFAULT NULL,
  `LinkToID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `LinkToID` (`LinkToID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.RedirectorPage: ~0 rows (ongeveer)
DELETE FROM `RedirectorPage`;
/*!40000 ALTER TABLE `RedirectorPage` DISABLE KEYS */;
/*!40000 ALTER TABLE `RedirectorPage` ENABLE KEYS */;


-- Structuur van  tabel vagrant.RedirectorPage_Live wordt geschreven
CREATE TABLE IF NOT EXISTS `RedirectorPage_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RedirectionType` enum('Internal','External') CHARACTER SET utf8 DEFAULT 'Internal',
  `ExternalURL` varchar(2083) CHARACTER SET utf8 DEFAULT NULL,
  `LinkToID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `LinkToID` (`LinkToID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.RedirectorPage_Live: ~0 rows (ongeveer)
DELETE FROM `RedirectorPage_Live`;
/*!40000 ALTER TABLE `RedirectorPage_Live` DISABLE KEYS */;
/*!40000 ALTER TABLE `RedirectorPage_Live` ENABLE KEYS */;


-- Structuur van  tabel vagrant.RedirectorPage_Versions wordt geschreven
CREATE TABLE IF NOT EXISTS `RedirectorPage_Versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `RedirectionType` enum('Internal','External') CHARACTER SET utf8 DEFAULT 'Internal',
  `ExternalURL` varchar(2083) CHARACTER SET utf8 DEFAULT NULL,
  `LinkToID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`),
  KEY `LinkToID` (`LinkToID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.RedirectorPage_Versions: ~0 rows (ongeveer)
DELETE FROM `RedirectorPage_Versions`;
/*!40000 ALTER TABLE `RedirectorPage_Versions` DISABLE KEYS */;
/*!40000 ALTER TABLE `RedirectorPage_Versions` ENABLE KEYS */;


-- Structuur van  tabel vagrant.RememberLoginHash wordt geschreven
CREATE TABLE IF NOT EXISTS `RememberLoginHash` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('SilverStripe\\Security\\RememberLoginHash') CHARACTER SET utf8 DEFAULT 'SilverStripe\\Security\\RememberLoginHash',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `DeviceID` varchar(40) CHARACTER SET utf8 DEFAULT NULL,
  `Hash` varchar(160) CHARACTER SET utf8 DEFAULT NULL,
  `ExpiryDate` datetime DEFAULT NULL,
  `MemberID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ClassName` (`ClassName`),
  KEY `MemberID` (`MemberID`),
  KEY `DeviceID` (`DeviceID`),
  KEY `Hash` (`Hash`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.RememberLoginHash: ~0 rows (ongeveer)
DELETE FROM `RememberLoginHash`;
/*!40000 ALTER TABLE `RememberLoginHash` DISABLE KEYS */;
/*!40000 ALTER TABLE `RememberLoginHash` ENABLE KEYS */;


-- Structuur van  tabel vagrant.SiteConfig wordt geschreven
CREATE TABLE IF NOT EXISTS `SiteConfig` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('SilverStripe\\SiteConfig\\SiteConfig') CHARACTER SET utf8 DEFAULT 'SilverStripe\\SiteConfig\\SiteConfig',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Tagline` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `CanViewType` enum('Anyone','LoggedInUsers','OnlyTheseUsers') CHARACTER SET utf8 DEFAULT 'Anyone',
  `CanEditType` enum('LoggedInUsers','OnlyTheseUsers') CHARACTER SET utf8 DEFAULT 'LoggedInUsers',
  `CanCreateTopLevelType` enum('LoggedInUsers','OnlyTheseUsers') CHARACTER SET utf8 DEFAULT 'LoggedInUsers',
  `FooterContent` mediumtext CHARACTER SET utf8,
  `GATrackingCode` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `GlobalFromEmail` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `GlobalFromName` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `FacebookUrl` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `TwitterUrl` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `InstagramUrl` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `YoutubeUrl` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `LinkedinUrl` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `PinterestUrl` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `GoogleplusUrl` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `SnapchatUrl` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `SoundcloudUrl` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `FacebookAppId` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `TawkSiteId` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `FooterColumnTitle1` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `FooterColumnTitle2` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `FooterColumnTitle3` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `FooterColumnTitle4` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `FooterContent2` mediumtext CHARACTER SET utf8,
  `Tracking` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ThirdParty` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Always` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `NoGeoIp` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Scrolling` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `RefreshPage` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Top` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ShowNoConsent` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `HideDetailsBtn` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Blocking` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Language` varchar(2) CHARACTER SET utf8 DEFAULT NULL,
  `Theme` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `RememberChoice` int(11) NOT NULL DEFAULT '0',
  `CustomPrivacyUrlID` int(11) NOT NULL DEFAULT '0',
  `ForceLang` varchar(2) CHARACTER SET utf8 DEFAULT NULL,
  `Remember` int(11) NOT NULL DEFAULT '0',
  `PrivacyPageID` int(11) NOT NULL DEFAULT '0',
  `CbTracking` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `CbThirdParty` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `CbAlways` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `CbNoGeoIp` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `CbScrolling` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `CbRefreshPage` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `CbTop` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `CbShowNoConsent` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `CbHideDetailsBtn` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `CbBlocking` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `CbForceLang` varchar(2) CHARACTER SET utf8 DEFAULT NULL,
  `CbTheme` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `CbRemember` int(11) NOT NULL DEFAULT '0',
  `CbEnable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `CbPrivacyPageID` int(11) NOT NULL DEFAULT '0',
  `FooterContent3` mediumtext CHARACTER SET utf8,
  `FooterContent4` mediumtext CHARACTER SET utf8,
  `CcEnable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `CcTimeOut` int(11) NOT NULL DEFAULT '0',
  `CcExpiration` int(11) NOT NULL DEFAULT '0',
  `CcImplicit` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `CcPerformance` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `CcAnalytics` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `CcMarketing` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `CcStatementID` int(11) NOT NULL DEFAULT '0',
  `FooterColumnTitle5` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `FooterColumnTitle6` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `GTMTrackingCode` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `AdCrowdPixel` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ClassName` (`ClassName`),
  KEY `CustomPrivacyUrlID` (`CustomPrivacyUrlID`),
  KEY `PrivacyPageID` (`PrivacyPageID`),
  KEY `CbPrivacyPageID` (`CbPrivacyPageID`),
  KEY `CcStatementID` (`CcStatementID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.SiteConfig: ~1 rows (ongeveer)
DELETE FROM `SiteConfig`;
/*!40000 ALTER TABLE `SiteConfig` DISABLE KEYS */;
INSERT INTO `SiteConfig` (`ID`, `ClassName`, `LastEdited`, `Created`, `Title`, `Tagline`, `CanViewType`, `CanEditType`, `CanCreateTopLevelType`, `FooterContent`, `GATrackingCode`, `GlobalFromEmail`, `GlobalFromName`, `FacebookUrl`, `TwitterUrl`, `InstagramUrl`, `YoutubeUrl`, `LinkedinUrl`, `PinterestUrl`, `GoogleplusUrl`, `SnapchatUrl`, `SoundcloudUrl`, `FacebookAppId`, `TawkSiteId`, `FooterColumnTitle1`, `FooterColumnTitle2`, `FooterColumnTitle3`, `FooterColumnTitle4`, `FooterContent2`, `Tracking`, `ThirdParty`, `Always`, `NoGeoIp`, `Scrolling`, `RefreshPage`, `Top`, `ShowNoConsent`, `HideDetailsBtn`, `Blocking`, `Language`, `Theme`, `RememberChoice`, `CustomPrivacyUrlID`, `ForceLang`, `Remember`, `PrivacyPageID`, `CbTracking`, `CbThirdParty`, `CbAlways`, `CbNoGeoIp`, `CbScrolling`, `CbRefreshPage`, `CbTop`, `CbShowNoConsent`, `CbHideDetailsBtn`, `CbBlocking`, `CbForceLang`, `CbTheme`, `CbRemember`, `CbEnable`, `CbPrivacyPageID`, `FooterContent3`, `FooterContent4`, `CcEnable`, `CcTimeOut`, `CcExpiration`, `CcImplicit`, `CcPerformance`, `CcAnalytics`, `CcMarketing`, `CcStatementID`, `FooterColumnTitle5`, `FooterColumnTitle6`, `GTMTrackingCode`, `AdCrowdPixel`) VALUES
	(1, 'SilverStripe\\SiteConfig\\SiteConfig', '2018-07-10 15:00:22', '2018-03-22 17:59:27', 'Your Site Name', 'your tagline here', 'Anyone', 'LoggedInUsers', 'LoggedInUsers', NULL, NULL, NULL, NULL, 'https://www.facebook.com', 'https://www.twtter.com', 'https://www.instagram.com', 'https://www.youtube.com', 'https://www.linkedin.com', 'https://www.pinterest.com', 'https://plus.google.com', 'https://www.snapchat.com', 'https://soundcloud.com', NULL, NULL, 'Menu', 'Over ons', 'Privacy & Voorwaarden', 'Volg ons', NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 30, 0, NULL, 30, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, 7, NULL, NULL, 1, 0, 0, 0, 1, 1, 1, 0, 'Beveiliging en privacy', NULL, NULL, NULL);
/*!40000 ALTER TABLE `SiteConfig` ENABLE KEYS */;


-- Structuur van  tabel vagrant.SiteConfig_CreateTopLevelGroups wordt geschreven
CREATE TABLE IF NOT EXISTS `SiteConfig_CreateTopLevelGroups` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SiteConfigID` int(11) NOT NULL DEFAULT '0',
  `GroupID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `SiteConfigID` (`SiteConfigID`),
  KEY `GroupID` (`GroupID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.SiteConfig_CreateTopLevelGroups: ~0 rows (ongeveer)
DELETE FROM `SiteConfig_CreateTopLevelGroups`;
/*!40000 ALTER TABLE `SiteConfig_CreateTopLevelGroups` DISABLE KEYS */;
/*!40000 ALTER TABLE `SiteConfig_CreateTopLevelGroups` ENABLE KEYS */;


-- Structuur van  tabel vagrant.SiteConfig_EditorGroups wordt geschreven
CREATE TABLE IF NOT EXISTS `SiteConfig_EditorGroups` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SiteConfigID` int(11) NOT NULL DEFAULT '0',
  `GroupID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `SiteConfigID` (`SiteConfigID`),
  KEY `GroupID` (`GroupID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.SiteConfig_EditorGroups: ~0 rows (ongeveer)
DELETE FROM `SiteConfig_EditorGroups`;
/*!40000 ALTER TABLE `SiteConfig_EditorGroups` DISABLE KEYS */;
/*!40000 ALTER TABLE `SiteConfig_EditorGroups` ENABLE KEYS */;


-- Structuur van  tabel vagrant.SiteConfig_ViewerGroups wordt geschreven
CREATE TABLE IF NOT EXISTS `SiteConfig_ViewerGroups` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SiteConfigID` int(11) NOT NULL DEFAULT '0',
  `GroupID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `SiteConfigID` (`SiteConfigID`),
  KEY `GroupID` (`GroupID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.SiteConfig_ViewerGroups: ~0 rows (ongeveer)
DELETE FROM `SiteConfig_ViewerGroups`;
/*!40000 ALTER TABLE `SiteConfig_ViewerGroups` DISABLE KEYS */;
/*!40000 ALTER TABLE `SiteConfig_ViewerGroups` ENABLE KEYS */;


-- Structuur van  tabel vagrant.SiteTree wordt geschreven
CREATE TABLE IF NOT EXISTS `SiteTree` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('SilverStripe\\CMS\\Model\\SiteTree','Page','ElementPage','HomePage','Hestec\\Faqpage\\FaqPage','FirstChildRedirect','SilverStripe\\ErrorPage\\ErrorPage','SilverStripe\\Blog\\Model\\Blog','SilverStripe\\Blog\\Model\\BlogPost','SilverStripe\\CMS\\Model\\RedirectorPage','SilverStripe\\CMS\\Model\\VirtualPage','SilverStripe\\UserForms\\Model\\UserDefinedForm','Hestec\\UpcomingEvents\\EventsPage') CHARACTER SET utf8 DEFAULT 'Page',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `URLSegment` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `MenuTitle` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `Content` mediumtext CHARACTER SET utf8,
  `MetaDescription` mediumtext CHARACTER SET utf8,
  `ExtraMeta` mediumtext CHARACTER SET utf8,
  `ShowInMenus` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ShowInSearch` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Sort` int(11) NOT NULL DEFAULT '0',
  `HasBrokenFile` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `HasBrokenLink` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ReportClass` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Version` int(11) NOT NULL DEFAULT '0',
  `CanViewType` enum('Anyone','LoggedInUsers','OnlyTheseUsers','Inherit') CHARACTER SET utf8 DEFAULT 'Inherit',
  `CanEditType` enum('LoggedInUsers','OnlyTheseUsers','Inherit') CHARACTER SET utf8 DEFAULT 'Inherit',
  `Priority` varchar(5) CHARACTER SET utf8 DEFAULT NULL,
  `ParentID` int(11) NOT NULL DEFAULT '0',
  `MetaTitle` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `Sort` (`Sort`),
  KEY `ClassName` (`ClassName`),
  KEY `ParentID` (`ParentID`),
  KEY `URLSegment` (`URLSegment`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.SiteTree: ~12 rows (ongeveer)
DELETE FROM `SiteTree`;
/*!40000 ALTER TABLE `SiteTree` DISABLE KEYS */;
INSERT INTO `SiteTree` (`ID`, `ClassName`, `LastEdited`, `Created`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `Version`, `CanViewType`, `CanEditType`, `Priority`, `ParentID`, `MetaTitle`) VALUES
	(1, 'HomePage', '2018-08-07 16:52:47', '2018-03-22 17:59:27', 'home', 'Home', NULL, '<p>Welcome to SilverStripe! This is the default homepage. You can edit this page by opening <a href="admin/">the CMS</a>.</p><p>You can now access the <a href="http://docs.silverstripe.org">developer documentation</a>, or begin the <a href="http://www.silverstripe.org/learn/lessons">SilverStripe lessons</a>.</p>', NULL, NULL, 1, 1, 1, 0, 0, NULL, 4, 'Inherit', 'Inherit', NULL, 0, NULL),
	(2, 'Page', '2018-07-06 12:35:57', '2018-03-22 17:59:27', 'about-us', 'About Us', NULL, '<p>Dit is een faketekst. Alles wat hier staat is slechts om een indruk te geven van het grafische effect van tekst op deze plek. Wat u hier leest is een voorbeeldtekst. Deze wordt later vervangen door de uiteindelijke tekst, die nu nog niet bekend is. De faketekst is dus een tekst die eigenlijk nergens over gaat. Het grappige is, dat mensen deze toch vaak lezen. Zelfs als men weet dat het om een faketekst gaat, lezen ze toch door.</p><p>Dit is een faketekst. Alles wat hier staat is slechts om een indruk te geven van het grafische effect van tekst op deze plek. Wat u hier leest is een voorbeeldtekst. Deze wordt later vervangen door de uiteindelijke tekst, die nu nog niet bekend is. De faketekst is dus een tekst die eigenlijk nergens over gaat. Het grappige is, dat mensen deze toch vaak lezen. Zelfs als men weet dat het om een faketekst gaat, lezen ze toch door.</p><p>Dit is een faketekst. Alles wat hier staat is slechts om een indruk te geven van het grafische effect van tekst op deze plek. Wat u hier leest is een voorbeeldtekst. Deze wordt later vervangen door de uiteindelijke tekst, die nu nog niet bekend is. De faketekst is dus een tekst die eigenlijk nergens over gaat. Het grappige is, dat mensen deze toch vaak lezen. Zelfs als men weet dat het om een faketekst gaat, lezen ze toch door.</p><p>Dit is een faketekst. Alles wat hier staat is slechts om een indruk te geven van het grafische effect van tekst op deze plek. Wat u hier leest is een voorbeeldtekst. Deze wordt later vervangen door de uiteindelijke tekst, die nu nog niet bekend is. De faketekst is dus een tekst die eigenlijk nergens over gaat. Het grappige is, dat mensen deze toch vaak lezen. Zelfs als men weet dat het om een faketekst gaat, lezen ze toch door.</p><p>Dit is een faketekst. Alles wat hier staat is slechts om een indruk te geven van het grafische effect van tekst op deze plek. Wat u hier leest is een voorbeeldtekst. Deze wordt later vervangen door de uiteindelijke tekst, die nu nog niet bekend is. De faketekst is dus een tekst die eigenlijk nergens over gaat. Het grappige is, dat mensen deze toch vaak lezen. Zelfs als men weet dat het om een faketekst gaat, lezen ze toch door.</p><p>Dit is een faketekst. Alles wat hier staat is slechts om een indruk te geven van het grafische effect van tekst op deze plek. Wat u hier leest is een voorbeeldtekst. Deze wordt later vervangen door de uiteindelijke tekst, die nu nog niet bekend is. De faketekst is dus een tekst die eigenlijk nergens over gaat. Het grappige is, dat mensen deze toch vaak lezen. Zelfs als men weet dat het om een faketekst gaat, lezen ze toch door.</p><p>Dit is een faketekst. Alles wat hier staat is slechts om een indruk te geven van het grafische effect van tekst op deze plek. Wat u hier leest is een voorbeeldtekst. Deze wordt later vervangen door de uiteindelijke tekst, die nu nog niet bekend is. De faketekst is dus een tekst die eigenlijk nergens over gaat. Het grappige is, dat mensen deze toch vaak lezen. Zelfs als men weet dat het om een faketekst gaat, lezen ze toch door.</p><p>Dit is een faketekst. Alles wat hier staat is slechts om een indruk te geven van het grafische effect van tekst op deze plek. Wat u hier leest is een voorbeeldtekst. Deze wordt later vervangen door de uiteindelijke tekst, die nu nog niet bekend is. De faketekst is dus een tekst die eigenlijk nergens over gaat. Het grappige is, dat mensen deze toch vaak lezen. Zelfs als men weet dat het om een faketekst gaat, lezen ze toch door.</p><p>Dit is een faketekst. Alles wat hier staat is slechts om een indruk te geven van het grafische effect van tekst op deze plek. Wat u hier leest is een voorbeeldtekst. Deze wordt later vervangen door de uiteindelijke tekst, die nu nog niet bekend is. De faketekst is dus een tekst die eigenlijk nergens over gaat. Het grappige is, dat mensen deze toch vaak lezen. Zelfs als men weet dat het om een faketekst gaat, lezen ze toch door.</p>', NULL, NULL, 1, 1, 2, 0, 0, NULL, 3, 'Inherit', 'Inherit', NULL, 0, NULL),
	(3, 'Page', '2018-03-22 17:59:28', '2018-03-22 17:59:28', 'contact-us', 'Contact Us', NULL, '<p>You can fill this page out with your own content, or delete it and create your own pages.</p>', NULL, NULL, 1, 1, 3, 0, 0, NULL, 1, 'Inherit', 'Inherit', NULL, 0, NULL),
	(4, 'SilverStripe\\ErrorPage\\ErrorPage', '2018-03-22 17:59:28', '2018-03-22 17:59:28', 'page-not-found', 'Page not found', NULL, '<p>Sorry, it seems you were trying to access a page that doesn\'t exist.</p><p>Please check the spelling of the URL you were trying to access and try again.</p>', NULL, NULL, 0, 0, 4, 0, 0, NULL, 1, 'Inherit', 'Inherit', NULL, 0, NULL),
	(5, 'SilverStripe\\ErrorPage\\ErrorPage', '2018-03-22 17:59:28', '2018-03-22 17:59:28', 'server-error', 'Server error', NULL, '<p>Sorry, there was a problem with handling your request.</p>', NULL, NULL, 0, 0, 5, 0, 0, NULL, 1, 'Inherit', 'Inherit', NULL, 0, NULL),
	(6, 'ElementPage', '2018-07-12 12:42:38', '2018-03-29 20:45:22', 'elements', 'Elements', NULL, NULL, NULL, NULL, 1, 1, 3, 0, 0, NULL, 5, 'Inherit', 'Inherit', NULL, 12, NULL),
	(7, 'Hestec\\UpcomingEvents\\EventsPage', '2018-04-04 10:48:57', '2018-04-04 10:48:44', 'evenementen', 'Evenementen', NULL, NULL, NULL, NULL, 1, 1, 8, 0, 0, NULL, 2, 'Inherit', 'Inherit', NULL, 0, NULL),
	(8, 'Hestec\\Faqpage\\FaqPage', '2018-09-18 10:22:54', '2018-07-06 12:22:53', 'veelgestelde-vragen', 'Veelgestelde vragen', NULL, NULL, NULL, NULL, 1, 1, 9, 0, 0, NULL, 8, 'Inherit', 'Inherit', NULL, 0, NULL),
	(9, 'Page', '2018-07-06 13:56:30', '2018-07-06 13:54:51', 'privacyverklaring', 'Privacyverklaring', NULL, '<h2>Handelsmerken</h2><p>De op deze internetpagina (â€˜websiteâ€™) genoemde handelsmerken of bedrijfsnamen kunnen een handelsmerk zijn. De rechten van deze handelmerken berusten bij de betreffende rechthebbende.</p><h2>Copyright</h2><p>Het copyright op de inhoud van deze website berust bij Hestec internetdiensten. Het is verboden om de inhoud van deze website in zijn geheel of gedeeltelijk te gebruiken, te kopiÃ«ren of over te nemen in enig ander werk, behalve voor:<br>het afdrukken of downloaden van delen van de inhoud van deze website voor niet-commercieel, informatief en persoonlijk gebruik; en&nbsp;het kopiÃ«ren van de inhoud van deze website om geÃ¯nteresseerde derde partijen persoonlijk te informeren op voorwaarde dat u Hestec internetdiensten noemt als de bron van de inhoud en dat u deze derden ervan op de hoogte stelt dat deze voorwaarden ook voor hen gelden en dat ook zij verplicht zijn de voorwaarden na te leven.</p><h2>Disclaimer: aansprakelijkheidsbeperkingen</h2><p>Hestec internetdiensten kan en wil de juistheid en de volledigheid van de informatie, de inhoud en de afbeeldingen op deze website niet garanderen. Hestec internetdiensten tracht u met deze website slechts â€œvrijblijvendâ€ te informeren over zijn producten en diensten. Voor zover wettelijk toegestaan stelt Hestec internetdiensten zich uitdrukkelijk niet aansprakelijk voor de juistheid en volledigheid van de informatie, inhoud en afbeeldingen op de website voor zover de website hiermee expliciet of impliciet verwachtingen zou scheppen over commercieel gebruik of andere toepassingen en over bepaalde productkenmerken of gebruikseigenschappen, ongeacht de rechtsgrond en ongeacht of Hestec internetdiensten, diens werknemers of vertegenwoordigers hierbij in gebreke zijn gebleven.<br>Hestec internetdiensten is in geen enkel opzicht aansprakelijk voor enige specifieke schade, indirecte schade, gevolgschade of schade vanwege uitval, gegevensverlies of winstderving, voortkomende of verband houdende met het gebruik of de juistheid van de informatie, inhoud of afbeeldingen op deze website, ongeacht of deze schade is ontstaan onder uitvoering van een overeenkomst, door nalatigheid of door een onrechtmatige daad.</p><h2>Privacyverklaring</h2><p>Hestec internetdiensten kan persoonlijke gegevens, zoals het Internet Protocol (IP) adres, opslaan wanneer u onze website bezoekt. Hestec internetdiensten zal deze informatie in lijn met de Wet Bescherming Persoonsgegevens, vertrouwelijk behandelen. Hestec internetdiensten&nbsp;gebruikt deze gegevens enkel en alleen om te reageren op vragen van u, om het bezoekersgedrag op de website te analyseren en voor marketingdoeleinden.</p><h2>Cookies</h2><p>De website van Hestec internetdiensten gebruikt cookies.</p><p><strong>Wat is een cookie?</strong><br>Een cookie is een klein tekstbestand dat tijdens je bezoek aan een website op bijvoorbeeld je computer, tablet of smartphone wordt geplaatst. In dit tekstbestand wordt informatie opgeslagen. Deze informatie kan bij een later bezoek weer worden herkend door deze website. De belangrijkste functie van cookies is om de ene gebruiker van de andere te onderscheiden. Je komt cookies dan ook veel tegen bij websites waarbij je moet inloggen. Een cookie zorgt dat je ingelogd blijft terwijl je de site gebruikt.</p><p>Hestec internetdiensten gebruikt de volgende cookies:</p><ul><li>Marketing (tracking en remarketing) cookies:<br>Met behulp van tracking cookies wordt het surfgedrag van een bezoeker van onze website gevolgd. Deze gegevens worden gebruikt om jou relevante advertenties te kunnen bieden op andere websites.</li>\n<li>Analytische cookies:<br>Wij gebruiken analytische cookies om bijvoorbeeld te zien hoe je onze website gebruikt, zodat de kwaliteit en/of de effectiviteit van de website kan worden verbeterd. Zo krijgen wij beter inzicht in het functioneren van de website en kunnen wij de site blijven verbeteren.</li>\n<li>Functionele cookies:<br>Deze cookies zijn nodig om deze site te laten functioneren. Je kan hierbij denken aan bestanden die je inloggegevens opslaan. Zo hoeft je niet iedere keer opnieuw je gegevens in te vullen. Functionele cookies maken weinig inbreuk op de privacy.</li>\n</ul><p><strong>Toelichting specifieke cookies</strong></p><ul><li><strong>Google Analytics</strong><br>Hestec internetdiensten maakt gebruik van analytische cookies via Google Analytics. Hierbij worden statistieken over aantallen bezoekers, de bezochte paginaâ€™s en de verkeersbronnen anoniem via het Google Analytics account van Hestec internetdiensten verzameld.\n<p>Analytische Cookies blijven maximaal 2 jaar geldig.</p>\n<p>Hestec internetdiensten heeft het Google Analytics account zo ingesteld dat:</p>\n<ul><li>Er een verwerkersovereenkomst tussen Hestec internetdiensten en Google is over de Analytics gegevens.</li>\n<li>IP-adressen anoniem worden gemaakt zodat gegevens nooit naar jou of jouw IP-adres te herleiden zijn.</li>\n<li>De informatie niet met anderen gedeeld wordt.</li>\n<li>Er geen profielen worden gemaakt n.a.v. interesses, leeftijd, enz.</li>\n<li>Hestec internetdiensten geeft je de mogelijkheid voor een â€˜opt-outâ€™. Hiermee telt jouw bezoek niet meer mee in de statistieken van alle websites die Google Analytics gebruiken. Klik hier om dit in te stellen.</li>\n</ul></li>\n<li><strong>Tawk.to</strong><br>Hestec internetdiensten maakt gebruik van Tawk.to. Deze dienst maakt het voor jou mogelijk om contact met ons op te nemen via een chatfunctie. Deze dienst is essentieel als je via de chat contact wilt opnemen met de klantenservice. Met deze cookies kunnen we jouw bezoek op de pagina blijven volgen. Tawk.to plaatst hiervoor een tracking cookie. Tawk.to slaat geen persoonlijke informatie op tenzij het is vereist voor de werking van de dienst. Tawk.to verwerkt ook jouw IP-adres.<br><br><a rel="noopener" href="https://www.tawk.to/data-protection/gdpr/" target="_blank">Klik hier voor de GDPR verklaring van Tawk.to</a></li>\n</ul><p><strong>Hoe kun je cookies verwijderen?</strong><br>Je cookies kun je verwijderen via je browser. klik hieronder op door jou gebruikte browser voor instructies:</p><ul><li><a rel="noopener" href="http://support.google.com/chrome/bin/answer.py?hl=nl&amp;answer=95647" target="_blank">Google Chrome</a></li>\n<li><a rel="noopener" href="http://windows.microsoft.com/nl-NL/windows-vista/Block-or-allow-cookies" target="_blank">Internet Explorer</a></li>\n<li><a rel="noopener" href="http://support.mozilla.org/nl/kb/Cookies%20in-%20en%20uitschakelen" target="_blank">Firefox</a></li>\n<li><a rel="noopener" href="http://support.apple.com/kb/PH5042" target="_blank">Safari</a></li>\n<li><a rel="noopener" href="https://support.microsoft.com/en-us/help/4027947/windows-delete-cookies" target="_blank">Microsoft Edge</a></li>\n<li><a rel="noopener" href="https://www.opera.com/help/tutorials/security/privacy/" target="_blank">Opera</a></li>\n</ul><p>&nbsp;</p>', NULL, NULL, 0, 1, 10, 0, 0, NULL, 8, 'Inherit', 'Inherit', NULL, 0, NULL),
	(10, 'Page', '2018-07-10 14:40:11', '2018-07-09 15:50:49', 'pagina-als-dropdown', 'Pagina als dropdown', NULL, '<p>Dit is een faketekst. Alles wat hier staat is slechts om een indruk te geven van het grafische effect van tekst op deze plek. Wat u hier leest is een voorbeeldtekst. Deze wordt later vervangen door de uiteindelijke tekst, die nu nog niet bekend is. De faketekst is dus een tekst die eigenlijk nergens over gaat. Het grappige is, dat mensen deze toch vaak lezen. Zelfs als men weet dat het om een faketekst gaat, lezen ze toch door.</p><p>Dit is een faketekst. Alles wat hier staat is slechts om een indruk te geven van het grafische effect van tekst op deze plek. Wat u hier leest is een voorbeeldtekst. Deze wordt later vervangen door de uiteindelijke tekst, die nu nog niet bekend is. De faketekst is dus een tekst die eigenlijk nergens over gaat. Het grappige is, dat mensen deze toch vaak lezen. Zelfs als men weet dat het om een faketekst gaat, lezen ze toch door.</p>', NULL, NULL, 1, 1, 1, 0, 0, NULL, 5, 'Inherit', 'Inherit', NULL, 12, NULL),
	(11, 'Page', '2018-07-10 14:40:18', '2018-07-09 15:51:46', 'nog-een-pagina', 'Nog een pagina', NULL, '<p>Dit is een faketekst. Alles wat hier staat is slechts om een indruk te geven van het grafische effect van tekst op deze plek. Wat u hier leest is een voorbeeldtekst. Deze wordt later vervangen door de uiteindelijke tekst, die nu nog niet bekend is. De faketekst is dus een tekst die eigenlijk nergens over gaat. Het grappige is, dat mensen deze toch vaak lezen. Zelfs als men weet dat het om een faketekst gaat, lezen ze toch door.</p><p>Dit is een faketekst. Alles wat hier staat is slechts om een indruk te geven van het grafische effect van tekst op deze plek. Wat u hier leest is een voorbeeldtekst. Deze wordt later vervangen door de uiteindelijke tekst, die nu nog niet bekend is. De faketekst is dus een tekst die eigenlijk nergens over gaat. Het grappige is, dat mensen deze toch vaak lezen. Zelfs als men weet dat het om een faketekst gaat, lezen ze toch door.</p>', NULL, NULL, 1, 1, 2, 0, 0, NULL, 5, 'Inherit', 'Inherit', NULL, 12, NULL),
	(12, 'FirstChildRedirect', '2018-07-10 14:39:51', '2018-07-10 14:39:17', 'dropdown', 'Dropdown', NULL, NULL, NULL, NULL, 1, 1, 6, 0, 0, NULL, 3, 'Inherit', 'Inherit', NULL, 0, NULL);
/*!40000 ALTER TABLE `SiteTree` ENABLE KEYS */;


-- Structuur van  tabel vagrant.SiteTreeLink wordt geschreven
CREATE TABLE IF NOT EXISTS `SiteTreeLink` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('SilverStripe\\CMS\\Model\\SiteTreeLink') CHARACTER SET utf8 DEFAULT 'SilverStripe\\CMS\\Model\\SiteTreeLink',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `LinkedID` int(11) NOT NULL DEFAULT '0',
  `ParentID` int(11) NOT NULL DEFAULT '0',
  `ParentClass` enum('Hestec\\SisowMethod\\SisowMethod','Hestec\\ElementalExtensions\\Dataobjects\\FaqQuestion','Hestec\\ExactOnline\\ExactOnlineConnection','Hestec\\FaqPage\\FaqCategorie','Hestec\\FaqPage\\FaqQuestion','BrandColor','BrandColorType','ExtraNavigationItem','Hestec\\Tools\\OwnIp','SilverStripe\\Assets\\File','SilverStripe\\SiteConfig\\SiteConfig','SilverStripe\\Versioned\\ChangeSetItem','SilverStripe\\Versioned\\ChangeSet','DNADesign\\Elemental\\Models\\BaseElement','DNADesign\\Elemental\\Models\\ElementalArea','gorriecoe\\Link\\Models\\Link','gorriecoe\\Menu\\Models\\MenuSet','SilverStripe\\Assets\\Shortcodes\\FileLink','SilverStripe\\Blog\\Model\\BlogCategory','SilverStripe\\Blog\\Model\\BlogTag','SilverStripe\\CMS\\Model\\SiteTreeLink','SilverStripe\\CMS\\Model\\SiteTree','SilverStripe\\Security\\Group','SilverStripe\\Security\\LoginAttempt','SilverStripe\\Security\\MemberPassword','SilverStripe\\Security\\Member','SilverStripe\\Security\\Permission','SilverStripe\\Security\\PermissionRoleCode','SilverStripe\\Security\\PermissionRole','SilverStripe\\Security\\RememberLoginHash','SilverStripe\\UserForms\\Model\\EditableCustomRule','SilverStripe\\UserForms\\Model\\EditableFormField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableOption','SilverStripe\\UserForms\\Model\\Recipient\\EmailRecipientCondition','SilverStripe\\UserForms\\Model\\Recipient\\EmailRecipient','SilverStripe\\UserForms\\Model\\Submission\\SubmittedFormField','SilverStripe\\UserForms\\Model\\Submission\\SubmittedForm','SilverStripe\\Assets\\Folder','SilverStripe\\Assets\\Image','Hestec\\ElementalExtensions\\Elements\\ElementContentImage','Hestec\\ElementalExtensions\\Elements\\ElementFaq','Hestec\\ElementalExtensions\\Elements\\ElementVideo','DNADesign\\Elemental\\Models\\ElementContent','gorriecoe\\Menu\\Models\\MenuLink','Page','ElementPage','HomePage','Hestec\\Faqpage\\FaqPage','FirstChildRedirect','SilverStripe\\ErrorPage\\ErrorPage','SilverStripe\\Blog\\Model\\Blog','SilverStripe\\Blog\\Model\\BlogPost','SilverStripe\\CMS\\Model\\RedirectorPage','SilverStripe\\CMS\\Model\\VirtualPage','SilverStripe\\UserForms\\Model\\UserDefinedForm','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableCheckbox','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableCountryDropdownField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableDateField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableEmailField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableFieldGroupEnd','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableFieldGroup','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableFileField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableFormHeading','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableFormStep','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableLiteralField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableMemberListField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableMultipleOptionField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableNumericField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableTextField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableCheckboxGroupField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableDropdown','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableRadioField','SilverStripe\\UserForms\\Model\\Submission\\SubmittedFileField') CHARACTER SET utf8 DEFAULT 'Hestec\\SisowMethod\\SisowMethod',
  PRIMARY KEY (`ID`),
  KEY `ClassName` (`ClassName`),
  KEY `LinkedID` (`LinkedID`),
  KEY `Parent` (`ParentID`,`ParentClass`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.SiteTreeLink: ~0 rows (ongeveer)
DELETE FROM `SiteTreeLink`;
/*!40000 ALTER TABLE `SiteTreeLink` DISABLE KEYS */;
/*!40000 ALTER TABLE `SiteTreeLink` ENABLE KEYS */;


-- Structuur van  tabel vagrant.SiteTree_EditorGroups wordt geschreven
CREATE TABLE IF NOT EXISTS `SiteTree_EditorGroups` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SiteTreeID` int(11) NOT NULL DEFAULT '0',
  `GroupID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `SiteTreeID` (`SiteTreeID`),
  KEY `GroupID` (`GroupID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.SiteTree_EditorGroups: ~0 rows (ongeveer)
DELETE FROM `SiteTree_EditorGroups`;
/*!40000 ALTER TABLE `SiteTree_EditorGroups` DISABLE KEYS */;
/*!40000 ALTER TABLE `SiteTree_EditorGroups` ENABLE KEYS */;


-- Structuur van  tabel vagrant.SiteTree_ImageTracking wordt geschreven
CREATE TABLE IF NOT EXISTS `SiteTree_ImageTracking` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SiteTreeID` int(11) NOT NULL DEFAULT '0',
  `FileID` int(11) NOT NULL DEFAULT '0',
  `FieldName` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `SiteTreeID` (`SiteTreeID`),
  KEY `FileID` (`FileID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.SiteTree_ImageTracking: ~0 rows (ongeveer)
DELETE FROM `SiteTree_ImageTracking`;
/*!40000 ALTER TABLE `SiteTree_ImageTracking` DISABLE KEYS */;
/*!40000 ALTER TABLE `SiteTree_ImageTracking` ENABLE KEYS */;


-- Structuur van  tabel vagrant.SiteTree_LinkTracking wordt geschreven
CREATE TABLE IF NOT EXISTS `SiteTree_LinkTracking` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SiteTreeID` int(11) NOT NULL DEFAULT '0',
  `ChildID` int(11) NOT NULL DEFAULT '0',
  `FieldName` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `SiteTreeID` (`SiteTreeID`),
  KEY `ChildID` (`ChildID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.SiteTree_LinkTracking: ~0 rows (ongeveer)
DELETE FROM `SiteTree_LinkTracking`;
/*!40000 ALTER TABLE `SiteTree_LinkTracking` DISABLE KEYS */;
/*!40000 ALTER TABLE `SiteTree_LinkTracking` ENABLE KEYS */;


-- Structuur van  tabel vagrant.SiteTree_Live wordt geschreven
CREATE TABLE IF NOT EXISTS `SiteTree_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('SilverStripe\\CMS\\Model\\SiteTree','Page','ElementPage','HomePage','Hestec\\Faqpage\\FaqPage','FirstChildRedirect','SilverStripe\\ErrorPage\\ErrorPage','SilverStripe\\Blog\\Model\\Blog','SilverStripe\\Blog\\Model\\BlogPost','SilverStripe\\CMS\\Model\\RedirectorPage','SilverStripe\\CMS\\Model\\VirtualPage','SilverStripe\\UserForms\\Model\\UserDefinedForm','Hestec\\UpcomingEvents\\EventsPage') CHARACTER SET utf8 DEFAULT 'Page',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `URLSegment` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `MenuTitle` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `Content` mediumtext CHARACTER SET utf8,
  `MetaDescription` mediumtext CHARACTER SET utf8,
  `ExtraMeta` mediumtext CHARACTER SET utf8,
  `ShowInMenus` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ShowInSearch` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Sort` int(11) NOT NULL DEFAULT '0',
  `HasBrokenFile` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `HasBrokenLink` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ReportClass` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Version` int(11) NOT NULL DEFAULT '0',
  `CanViewType` enum('Anyone','LoggedInUsers','OnlyTheseUsers','Inherit') CHARACTER SET utf8 DEFAULT 'Inherit',
  `CanEditType` enum('LoggedInUsers','OnlyTheseUsers','Inherit') CHARACTER SET utf8 DEFAULT 'Inherit',
  `Priority` varchar(5) CHARACTER SET utf8 DEFAULT NULL,
  `ParentID` int(11) NOT NULL DEFAULT '0',
  `MetaTitle` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `Sort` (`Sort`),
  KEY `ClassName` (`ClassName`),
  KEY `ParentID` (`ParentID`),
  KEY `URLSegment` (`URLSegment`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.SiteTree_Live: ~12 rows (ongeveer)
DELETE FROM `SiteTree_Live`;
/*!40000 ALTER TABLE `SiteTree_Live` DISABLE KEYS */;
INSERT INTO `SiteTree_Live` (`ID`, `ClassName`, `LastEdited`, `Created`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `Version`, `CanViewType`, `CanEditType`, `Priority`, `ParentID`, `MetaTitle`) VALUES
	(1, 'HomePage', '2018-08-07 16:52:47', '2018-03-22 17:59:27', 'home', 'Home', NULL, '<p>Welcome to SilverStripe! This is the default homepage. You can edit this page by opening <a href="admin/">the CMS</a>.</p><p>You can now access the <a href="http://docs.silverstripe.org">developer documentation</a>, or begin the <a href="http://www.silverstripe.org/learn/lessons">SilverStripe lessons</a>.</p>', NULL, NULL, 1, 1, 1, 0, 0, NULL, 4, 'Inherit', 'Inherit', NULL, 0, NULL),
	(2, 'Page', '2018-07-06 12:35:57', '2018-03-22 17:59:27', 'about-us', 'About Us', NULL, '<p>Dit is een faketekst. Alles wat hier staat is slechts om een indruk te geven van het grafische effect van tekst op deze plek. Wat u hier leest is een voorbeeldtekst. Deze wordt later vervangen door de uiteindelijke tekst, die nu nog niet bekend is. De faketekst is dus een tekst die eigenlijk nergens over gaat. Het grappige is, dat mensen deze toch vaak lezen. Zelfs als men weet dat het om een faketekst gaat, lezen ze toch door.</p><p>Dit is een faketekst. Alles wat hier staat is slechts om een indruk te geven van het grafische effect van tekst op deze plek. Wat u hier leest is een voorbeeldtekst. Deze wordt later vervangen door de uiteindelijke tekst, die nu nog niet bekend is. De faketekst is dus een tekst die eigenlijk nergens over gaat. Het grappige is, dat mensen deze toch vaak lezen. Zelfs als men weet dat het om een faketekst gaat, lezen ze toch door.</p><p>Dit is een faketekst. Alles wat hier staat is slechts om een indruk te geven van het grafische effect van tekst op deze plek. Wat u hier leest is een voorbeeldtekst. Deze wordt later vervangen door de uiteindelijke tekst, die nu nog niet bekend is. De faketekst is dus een tekst die eigenlijk nergens over gaat. Het grappige is, dat mensen deze toch vaak lezen. Zelfs als men weet dat het om een faketekst gaat, lezen ze toch door.</p><p>Dit is een faketekst. Alles wat hier staat is slechts om een indruk te geven van het grafische effect van tekst op deze plek. Wat u hier leest is een voorbeeldtekst. Deze wordt later vervangen door de uiteindelijke tekst, die nu nog niet bekend is. De faketekst is dus een tekst die eigenlijk nergens over gaat. Het grappige is, dat mensen deze toch vaak lezen. Zelfs als men weet dat het om een faketekst gaat, lezen ze toch door.</p><p>Dit is een faketekst. Alles wat hier staat is slechts om een indruk te geven van het grafische effect van tekst op deze plek. Wat u hier leest is een voorbeeldtekst. Deze wordt later vervangen door de uiteindelijke tekst, die nu nog niet bekend is. De faketekst is dus een tekst die eigenlijk nergens over gaat. Het grappige is, dat mensen deze toch vaak lezen. Zelfs als men weet dat het om een faketekst gaat, lezen ze toch door.</p><p>Dit is een faketekst. Alles wat hier staat is slechts om een indruk te geven van het grafische effect van tekst op deze plek. Wat u hier leest is een voorbeeldtekst. Deze wordt later vervangen door de uiteindelijke tekst, die nu nog niet bekend is. De faketekst is dus een tekst die eigenlijk nergens over gaat. Het grappige is, dat mensen deze toch vaak lezen. Zelfs als men weet dat het om een faketekst gaat, lezen ze toch door.</p><p>Dit is een faketekst. Alles wat hier staat is slechts om een indruk te geven van het grafische effect van tekst op deze plek. Wat u hier leest is een voorbeeldtekst. Deze wordt later vervangen door de uiteindelijke tekst, die nu nog niet bekend is. De faketekst is dus een tekst die eigenlijk nergens over gaat. Het grappige is, dat mensen deze toch vaak lezen. Zelfs als men weet dat het om een faketekst gaat, lezen ze toch door.</p><p>Dit is een faketekst. Alles wat hier staat is slechts om een indruk te geven van het grafische effect van tekst op deze plek. Wat u hier leest is een voorbeeldtekst. Deze wordt later vervangen door de uiteindelijke tekst, die nu nog niet bekend is. De faketekst is dus een tekst die eigenlijk nergens over gaat. Het grappige is, dat mensen deze toch vaak lezen. Zelfs als men weet dat het om een faketekst gaat, lezen ze toch door.</p><p>Dit is een faketekst. Alles wat hier staat is slechts om een indruk te geven van het grafische effect van tekst op deze plek. Wat u hier leest is een voorbeeldtekst. Deze wordt later vervangen door de uiteindelijke tekst, die nu nog niet bekend is. De faketekst is dus een tekst die eigenlijk nergens over gaat. Het grappige is, dat mensen deze toch vaak lezen. Zelfs als men weet dat het om een faketekst gaat, lezen ze toch door.</p>', NULL, NULL, 1, 1, 2, 0, 0, NULL, 3, 'Inherit', 'Inherit', NULL, 0, NULL),
	(3, 'Page', '2018-03-22 17:59:28', '2018-03-22 17:59:28', 'contact-us', 'Contact Us', NULL, '<p>You can fill this page out with your own content, or delete it and create your own pages.</p>', NULL, NULL, 1, 1, 3, 0, 0, NULL, 1, 'Inherit', 'Inherit', NULL, 0, NULL),
	(4, 'SilverStripe\\ErrorPage\\ErrorPage', '2018-03-22 17:59:28', '2018-03-22 17:59:28', 'page-not-found', 'Page not found', NULL, '<p>Sorry, it seems you were trying to access a page that doesn\'t exist.</p><p>Please check the spelling of the URL you were trying to access and try again.</p>', NULL, NULL, 0, 0, 4, 0, 0, NULL, 1, 'Inherit', 'Inherit', NULL, 0, NULL),
	(5, 'SilverStripe\\ErrorPage\\ErrorPage', '2018-03-22 17:59:28', '2018-03-22 17:59:28', 'server-error', 'Server error', NULL, '<p>Sorry, there was a problem with handling your request.</p>', NULL, NULL, 0, 0, 5, 0, 0, NULL, 1, 'Inherit', 'Inherit', NULL, 0, NULL),
	(6, 'ElementPage', '2018-07-10 14:40:40', '2018-03-29 20:45:22', 'elements', 'Elements', NULL, NULL, NULL, NULL, 1, 1, 3, 0, 0, NULL, 5, 'Inherit', 'Inherit', NULL, 12, NULL),
	(7, 'Hestec\\UpcomingEvents\\EventsPage', '2018-04-04 10:48:57', '2018-04-04 10:48:44', 'evenementen', 'Evenementen', NULL, NULL, NULL, NULL, 1, 1, 8, 0, 0, NULL, 2, 'Inherit', 'Inherit', NULL, 0, NULL),
	(8, 'Hestec\\Faqpage\\FaqPage', '2018-09-18 10:22:54', '2018-07-06 12:22:53', 'veelgestelde-vragen', 'Veelgestelde vragen', NULL, NULL, NULL, NULL, 1, 1, 9, 0, 0, NULL, 8, 'Inherit', 'Inherit', NULL, 0, NULL),
	(9, 'Page', '2018-07-06 13:56:30', '2018-07-06 13:54:51', 'privacyverklaring', 'Privacyverklaring', NULL, '<h2>Handelsmerken</h2><p>De op deze internetpagina (â€˜websiteâ€™) genoemde handelsmerken of bedrijfsnamen kunnen een handelsmerk zijn. De rechten van deze handelmerken berusten bij de betreffende rechthebbende.</p><h2>Copyright</h2><p>Het copyright op de inhoud van deze website berust bij Hestec internetdiensten. Het is verboden om de inhoud van deze website in zijn geheel of gedeeltelijk te gebruiken, te kopiÃ«ren of over te nemen in enig ander werk, behalve voor:<br>het afdrukken of downloaden van delen van de inhoud van deze website voor niet-commercieel, informatief en persoonlijk gebruik; en&nbsp;het kopiÃ«ren van de inhoud van deze website om geÃ¯nteresseerde derde partijen persoonlijk te informeren op voorwaarde dat u Hestec internetdiensten noemt als de bron van de inhoud en dat u deze derden ervan op de hoogte stelt dat deze voorwaarden ook voor hen gelden en dat ook zij verplicht zijn de voorwaarden na te leven.</p><h2>Disclaimer: aansprakelijkheidsbeperkingen</h2><p>Hestec internetdiensten kan en wil de juistheid en de volledigheid van de informatie, de inhoud en de afbeeldingen op deze website niet garanderen. Hestec internetdiensten tracht u met deze website slechts â€œvrijblijvendâ€ te informeren over zijn producten en diensten. Voor zover wettelijk toegestaan stelt Hestec internetdiensten zich uitdrukkelijk niet aansprakelijk voor de juistheid en volledigheid van de informatie, inhoud en afbeeldingen op de website voor zover de website hiermee expliciet of impliciet verwachtingen zou scheppen over commercieel gebruik of andere toepassingen en over bepaalde productkenmerken of gebruikseigenschappen, ongeacht de rechtsgrond en ongeacht of Hestec internetdiensten, diens werknemers of vertegenwoordigers hierbij in gebreke zijn gebleven.<br>Hestec internetdiensten is in geen enkel opzicht aansprakelijk voor enige specifieke schade, indirecte schade, gevolgschade of schade vanwege uitval, gegevensverlies of winstderving, voortkomende of verband houdende met het gebruik of de juistheid van de informatie, inhoud of afbeeldingen op deze website, ongeacht of deze schade is ontstaan onder uitvoering van een overeenkomst, door nalatigheid of door een onrechtmatige daad.</p><h2>Privacyverklaring</h2><p>Hestec internetdiensten kan persoonlijke gegevens, zoals het Internet Protocol (IP) adres, opslaan wanneer u onze website bezoekt. Hestec internetdiensten zal deze informatie in lijn met de Wet Bescherming Persoonsgegevens, vertrouwelijk behandelen. Hestec internetdiensten&nbsp;gebruikt deze gegevens enkel en alleen om te reageren op vragen van u, om het bezoekersgedrag op de website te analyseren en voor marketingdoeleinden.</p><h2>Cookies</h2><p>De website van Hestec internetdiensten gebruikt cookies.</p><p><strong>Wat is een cookie?</strong><br>Een cookie is een klein tekstbestand dat tijdens je bezoek aan een website op bijvoorbeeld je computer, tablet of smartphone wordt geplaatst. In dit tekstbestand wordt informatie opgeslagen. Deze informatie kan bij een later bezoek weer worden herkend door deze website. De belangrijkste functie van cookies is om de ene gebruiker van de andere te onderscheiden. Je komt cookies dan ook veel tegen bij websites waarbij je moet inloggen. Een cookie zorgt dat je ingelogd blijft terwijl je de site gebruikt.</p><p>Hestec internetdiensten gebruikt de volgende cookies:</p><ul><li>Marketing (tracking en remarketing) cookies:<br>Met behulp van tracking cookies wordt het surfgedrag van een bezoeker van onze website gevolgd. Deze gegevens worden gebruikt om jou relevante advertenties te kunnen bieden op andere websites.</li>\n<li>Analytische cookies:<br>Wij gebruiken analytische cookies om bijvoorbeeld te zien hoe je onze website gebruikt, zodat de kwaliteit en/of de effectiviteit van de website kan worden verbeterd. Zo krijgen wij beter inzicht in het functioneren van de website en kunnen wij de site blijven verbeteren.</li>\n<li>Functionele cookies:<br>Deze cookies zijn nodig om deze site te laten functioneren. Je kan hierbij denken aan bestanden die je inloggegevens opslaan. Zo hoeft je niet iedere keer opnieuw je gegevens in te vullen. Functionele cookies maken weinig inbreuk op de privacy.</li>\n</ul><p><strong>Toelichting specifieke cookies</strong></p><ul><li><strong>Google Analytics</strong><br>Hestec internetdiensten maakt gebruik van analytische cookies via Google Analytics. Hierbij worden statistieken over aantallen bezoekers, de bezochte paginaâ€™s en de verkeersbronnen anoniem via het Google Analytics account van Hestec internetdiensten verzameld.\n<p>Analytische Cookies blijven maximaal 2 jaar geldig.</p>\n<p>Hestec internetdiensten heeft het Google Analytics account zo ingesteld dat:</p>\n<ul><li>Er een verwerkersovereenkomst tussen Hestec internetdiensten en Google is over de Analytics gegevens.</li>\n<li>IP-adressen anoniem worden gemaakt zodat gegevens nooit naar jou of jouw IP-adres te herleiden zijn.</li>\n<li>De informatie niet met anderen gedeeld wordt.</li>\n<li>Er geen profielen worden gemaakt n.a.v. interesses, leeftijd, enz.</li>\n<li>Hestec internetdiensten geeft je de mogelijkheid voor een â€˜opt-outâ€™. Hiermee telt jouw bezoek niet meer mee in de statistieken van alle websites die Google Analytics gebruiken. Klik hier om dit in te stellen.</li>\n</ul></li>\n<li><strong>Tawk.to</strong><br>Hestec internetdiensten maakt gebruik van Tawk.to. Deze dienst maakt het voor jou mogelijk om contact met ons op te nemen via een chatfunctie. Deze dienst is essentieel als je via de chat contact wilt opnemen met de klantenservice. Met deze cookies kunnen we jouw bezoek op de pagina blijven volgen. Tawk.to plaatst hiervoor een tracking cookie. Tawk.to slaat geen persoonlijke informatie op tenzij het is vereist voor de werking van de dienst. Tawk.to verwerkt ook jouw IP-adres.<br><br><a rel="noopener" href="https://www.tawk.to/data-protection/gdpr/" target="_blank">Klik hier voor de GDPR verklaring van Tawk.to</a></li>\n</ul><p><strong>Hoe kun je cookies verwijderen?</strong><br>Je cookies kun je verwijderen via je browser. klik hieronder op door jou gebruikte browser voor instructies:</p><ul><li><a rel="noopener" href="http://support.google.com/chrome/bin/answer.py?hl=nl&amp;answer=95647" target="_blank">Google Chrome</a></li>\n<li><a rel="noopener" href="http://windows.microsoft.com/nl-NL/windows-vista/Block-or-allow-cookies" target="_blank">Internet Explorer</a></li>\n<li><a rel="noopener" href="http://support.mozilla.org/nl/kb/Cookies%20in-%20en%20uitschakelen" target="_blank">Firefox</a></li>\n<li><a rel="noopener" href="http://support.apple.com/kb/PH5042" target="_blank">Safari</a></li>\n<li><a rel="noopener" href="https://support.microsoft.com/en-us/help/4027947/windows-delete-cookies" target="_blank">Microsoft Edge</a></li>\n<li><a rel="noopener" href="https://www.opera.com/help/tutorials/security/privacy/" target="_blank">Opera</a></li>\n</ul><p>&nbsp;</p>', NULL, NULL, 0, 1, 10, 0, 0, NULL, 8, 'Inherit', 'Inherit', NULL, 0, NULL),
	(10, 'Page', '2018-07-10 14:40:11', '2018-07-09 15:50:49', 'pagina-als-dropdown', 'Pagina als dropdown', NULL, '<p>Dit is een faketekst. Alles wat hier staat is slechts om een indruk te geven van het grafische effect van tekst op deze plek. Wat u hier leest is een voorbeeldtekst. Deze wordt later vervangen door de uiteindelijke tekst, die nu nog niet bekend is. De faketekst is dus een tekst die eigenlijk nergens over gaat. Het grappige is, dat mensen deze toch vaak lezen. Zelfs als men weet dat het om een faketekst gaat, lezen ze toch door.</p><p>Dit is een faketekst. Alles wat hier staat is slechts om een indruk te geven van het grafische effect van tekst op deze plek. Wat u hier leest is een voorbeeldtekst. Deze wordt later vervangen door de uiteindelijke tekst, die nu nog niet bekend is. De faketekst is dus een tekst die eigenlijk nergens over gaat. Het grappige is, dat mensen deze toch vaak lezen. Zelfs als men weet dat het om een faketekst gaat, lezen ze toch door.</p>', NULL, NULL, 1, 1, 1, 0, 0, NULL, 5, 'Inherit', 'Inherit', NULL, 12, NULL),
	(11, 'Page', '2018-07-10 14:40:18', '2018-07-09 15:51:46', 'nog-een-pagina', 'Nog een pagina', NULL, '<p>Dit is een faketekst. Alles wat hier staat is slechts om een indruk te geven van het grafische effect van tekst op deze plek. Wat u hier leest is een voorbeeldtekst. Deze wordt later vervangen door de uiteindelijke tekst, die nu nog niet bekend is. De faketekst is dus een tekst die eigenlijk nergens over gaat. Het grappige is, dat mensen deze toch vaak lezen. Zelfs als men weet dat het om een faketekst gaat, lezen ze toch door.</p><p>Dit is een faketekst. Alles wat hier staat is slechts om een indruk te geven van het grafische effect van tekst op deze plek. Wat u hier leest is een voorbeeldtekst. Deze wordt later vervangen door de uiteindelijke tekst, die nu nog niet bekend is. De faketekst is dus een tekst die eigenlijk nergens over gaat. Het grappige is, dat mensen deze toch vaak lezen. Zelfs als men weet dat het om een faketekst gaat, lezen ze toch door.</p>', NULL, NULL, 1, 1, 2, 0, 0, NULL, 5, 'Inherit', 'Inherit', NULL, 12, NULL),
	(12, 'FirstChildRedirect', '2018-07-10 14:39:51', '2018-07-10 14:39:17', 'dropdown', 'Dropdown', NULL, NULL, NULL, NULL, 1, 1, 6, 0, 0, NULL, 3, 'Inherit', 'Inherit', NULL, 0, NULL);
/*!40000 ALTER TABLE `SiteTree_Live` ENABLE KEYS */;


-- Structuur van  tabel vagrant.SiteTree_Versions wordt geschreven
CREATE TABLE IF NOT EXISTS `SiteTree_Versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `WasPublished` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `AuthorID` int(11) NOT NULL DEFAULT '0',
  `PublisherID` int(11) NOT NULL DEFAULT '0',
  `ClassName` enum('SilverStripe\\CMS\\Model\\SiteTree','Page','ElementPage','HomePage','Hestec\\Faqpage\\FaqPage','FirstChildRedirect','SilverStripe\\ErrorPage\\ErrorPage','SilverStripe\\Blog\\Model\\Blog','SilverStripe\\Blog\\Model\\BlogPost','SilverStripe\\CMS\\Model\\RedirectorPage','SilverStripe\\CMS\\Model\\VirtualPage','SilverStripe\\UserForms\\Model\\UserDefinedForm','TheWebmen\\FAQ\\Pages\\FAQPage','Hestec\\UpcomingEvents\\EventsPage','Axllent\\EnquiryPage\\EnquiryPage','Suilven\\HomeLandingPage\\Model\\HomePage','Suilven\\HomeLandingPage\\Model\\LandingPage') CHARACTER SET utf8 DEFAULT 'Page',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `URLSegment` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `MenuTitle` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `Content` mediumtext CHARACTER SET utf8,
  `MetaDescription` mediumtext CHARACTER SET utf8,
  `ExtraMeta` mediumtext CHARACTER SET utf8,
  `ShowInMenus` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ShowInSearch` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Sort` int(11) NOT NULL DEFAULT '0',
  `HasBrokenFile` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `HasBrokenLink` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ReportClass` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `CanViewType` enum('Anyone','LoggedInUsers','OnlyTheseUsers','Inherit') CHARACTER SET utf8 DEFAULT 'Inherit',
  `CanEditType` enum('LoggedInUsers','OnlyTheseUsers','Inherit') CHARACTER SET utf8 DEFAULT 'Inherit',
  `Priority` varchar(5) CHARACTER SET utf8 DEFAULT NULL,
  `ParentID` int(11) NOT NULL DEFAULT '0',
  `MetaTitle` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `WasDeleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `WasDraft` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`),
  KEY `AuthorID` (`AuthorID`),
  KEY `PublisherID` (`PublisherID`),
  KEY `Sort` (`Sort`),
  KEY `ClassName` (`ClassName`),
  KEY `ParentID` (`ParentID`),
  KEY `URLSegment` (`URLSegment`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.SiteTree_Versions: ~44 rows (ongeveer)
DELETE FROM `SiteTree_Versions`;
/*!40000 ALTER TABLE `SiteTree_Versions` DISABLE KEYS */;
INSERT INTO `SiteTree_Versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `LastEdited`, `Created`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `Priority`, `ParentID`, `MetaTitle`, `WasDeleted`, `WasDraft`) VALUES
	(1, 1, 1, 1, 0, 0, 'Page', '2018-03-22 17:59:27', '2018-03-22 17:59:27', 'home', 'Home', NULL, '<p>Welcome to SilverStripe! This is the default homepage. You can edit this page by opening <a href="admin/">the CMS</a>.</p><p>You can now access the <a href="http://docs.silverstripe.org">developer documentation</a>, or begin the <a href="http://www.silverstripe.org/learn/lessons">SilverStripe lessons</a>.</p>', NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 0, NULL, 0, 0),
	(2, 2, 1, 1, 0, 0, 'Page', '2018-03-22 17:59:27', '2018-03-22 17:59:27', 'about-us', 'About Us', NULL, '<p>You can fill this page out with your own content, or delete it and create your own pages.</p>', NULL, NULL, 1, 1, 2, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 0, NULL, 0, 0),
	(3, 3, 1, 1, 0, 0, 'Page', '2018-03-22 17:59:28', '2018-03-22 17:59:28', 'contact-us', 'Contact Us', NULL, '<p>You can fill this page out with your own content, or delete it and create your own pages.</p>', NULL, NULL, 1, 1, 3, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 0, NULL, 0, 0),
	(4, 4, 1, 1, 0, 0, 'SilverStripe\\ErrorPage\\ErrorPage', '2018-03-22 17:59:28', '2018-03-22 17:59:28', 'page-not-found', 'Page not found', NULL, '<p>Sorry, it seems you were trying to access a page that doesn\'t exist.</p><p>Please check the spelling of the URL you were trying to access and try again.</p>', NULL, NULL, 0, 0, 4, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 0, NULL, 0, 0),
	(5, 5, 1, 1, 0, 0, 'SilverStripe\\ErrorPage\\ErrorPage', '2018-03-22 17:59:28', '2018-03-22 17:59:28', 'server-error', 'Server error', NULL, '<p>Sorry, there was a problem with handling your request.</p>', NULL, NULL, 0, 0, 5, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 0, NULL, 0, 0),
	(6, 1, 2, 1, 1, 1, 'Page', '2018-03-26 22:27:42', '2018-03-22 17:59:27', 'home', 'Home', NULL, '<p>Welcome to SilverStripe! This is the default homepage. You can edit this page by opening <a href="admin/">the CMS</a>.</p><p>You can now access the <a href="http://docs.silverstripe.org">developer documentation</a>, or begin the <a href="http://www.silverstripe.org/learn/lessons">SilverStripe lessons</a>.</p>', NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 0, NULL, 0, 0),
	(7, 6, 1, 0, 1, 0, 'ElementPage', '2018-03-29 20:45:22', '2018-03-29 20:45:22', 'new-element-page', 'New Element Page', NULL, NULL, NULL, NULL, 1, 1, 6, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 0, NULL, 0, 0),
	(8, 6, 2, 1, 1, 1, 'ElementPage', '2018-03-29 20:45:36', '2018-03-29 20:45:22', 'elements', 'Elements', NULL, NULL, NULL, NULL, 1, 1, 6, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 0, NULL, 0, 0),
	(9, 6, 3, 1, 1, 1, 'ElementPage', '2018-03-31 14:10:15', '2018-03-29 20:45:22', 'elements', 'Elements', NULL, NULL, NULL, NULL, 1, 1, 6, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 0, NULL, 0, 0),
	(10, 7, 1, 0, 1, 0, 'Hestec\\UpcomingEvents\\EventsPage', '2018-04-04 10:48:44', '2018-04-04 10:48:44', 'nieuwe-events-page', 'Nieuwe Events Page', NULL, NULL, NULL, NULL, 1, 1, 7, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 0, NULL, 0, 0),
	(11, 7, 2, 1, 1, 1, 'Hestec\\UpcomingEvents\\EventsPage', '2018-04-04 10:48:57', '2018-04-04 10:48:44', 'evenementen', 'Evenementen', NULL, NULL, NULL, NULL, 1, 1, 7, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 0, NULL, 0, 0),
	(12, 8, 1, 0, 1, 0, 'Suilven\\HomeLandingPage\\Model\\LandingPage', '2018-04-05 10:16:37', '2018-04-05 10:16:37', 'nieuwe-landing-page', 'Nieuwe Landing Page', NULL, NULL, NULL, NULL, 1, 1, 8, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 0, NULL, 0, 0),
	(13, 8, 2, 1, 1, 1, 'Suilven\\HomeLandingPage\\Model\\LandingPage', '2018-04-05 10:16:58', '2018-04-05 10:16:37', 'dit-is-een-test', 'Dit is een test', NULL, NULL, NULL, NULL, 1, 1, 8, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 0, NULL, 0, 0),
	(14, 8, 3, 1, 1, 1, 'Suilven\\HomeLandingPage\\Model\\LandingPage', '2018-04-05 10:17:21', '2018-04-05 10:16:37', 'landing', 'Landing', NULL, '<p>dit is een test</p>', NULL, NULL, 1, 1, 8, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 0, NULL, 0, 0),
	(15, 9, 1, 0, 1, 0, 'Suilven\\HomeLandingPage\\Model\\HomePage', '2018-04-05 10:23:09', '2018-04-05 10:23:09', 'nieuwe-home-page', 'Nieuwe Home Page', NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 8, NULL, 0, 0),
	(16, 9, 2, 1, 1, 1, 'Suilven\\HomeLandingPage\\Model\\HomePage', '2018-04-05 10:23:29', '2018-04-05 10:23:09', 'homepage', 'Homepage', NULL, '<p>dit is ook een test</p>', NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 8, NULL, 0, 0),
	(17, 9, 3, 0, 1, 0, 'Suilven\\HomeLandingPage\\Model\\HomePage', '2018-04-05 10:23:33', '2018-04-05 10:23:09', 'homepage', 'Homepage', NULL, '<p>dit is ook een test</p>', NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 0, NULL, 0, 0),
	(18, 9, 4, 1, 1, 1, 'Suilven\\HomeLandingPage\\Model\\HomePage', '2018-04-05 10:23:33', '2018-04-05 10:23:09', 'homepage', 'Homepage', NULL, '<p>dit is ook een test</p>', NULL, NULL, 1, 1, 8, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 0, NULL, 0, 0),
	(19, 10, 1, 0, 1, 0, 'Axllent\\EnquiryPage\\EnquiryPage', '2018-04-05 10:53:09', '2018-04-05 10:53:09', 'nieuwe-enquiry-page', 'Nieuwe Enquiry Page', NULL, NULL, NULL, NULL, 1, 1, 8, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 0, NULL, 0, 0),
	(20, 10, 2, 1, 1, 1, 'Axllent\\EnquiryPage\\EnquiryPage', '2018-04-05 10:53:24', '2018-04-05 10:53:09', 'enquiry', 'Enquiry', NULL, '<p>dit is een test</p>', NULL, NULL, 1, 1, 8, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 0, NULL, 0, 0),
	(21, 11, 1, 0, 1, 0, 'SilverStripe\\Blog\\Model\\Blog', '2018-04-05 11:04:10', '2018-04-05 11:04:10', 'nieuwe-blog', 'Nieuwe Blog', NULL, NULL, NULL, NULL, 1, 1, 9, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 0, NULL, 0, 0),
	(22, 11, 2, 1, 1, 1, 'SilverStripe\\Blog\\Model\\Blog', '2018-04-05 11:04:26', '2018-04-05 11:04:10', 'blog', 'Blog', NULL, '<p>hallo</p>', NULL, NULL, 1, 1, 9, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 0, NULL, 0, 0),
	(23, 8, 4, 0, 1, 0, 'TheWebmen\\FAQ\\Pages\\FAQPage', '2018-07-06 12:22:53', '2018-07-06 12:22:53', 'nieuwe-faq-pagina', 'Nieuwe FAQ pagina', NULL, NULL, NULL, NULL, 1, 1, 8, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 0, NULL, 0, 0),
	(24, 8, 5, 1, 1, 1, 'TheWebmen\\FAQ\\Pages\\FAQPage', '2018-07-06 12:23:59', '2018-07-06 12:22:53', 'frequently-asked-questions', 'Frequently asked questions', NULL, NULL, NULL, NULL, 1, 1, 8, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 0, NULL, 0, 0),
	(25, 8, 6, 1, 1, 1, 'TheWebmen\\FAQ\\Pages\\FAQPage', '2018-07-06 12:25:04', '2018-07-06 12:22:53', 'veelgestelde-vragen', 'Veelgestelde vragen', NULL, NULL, NULL, NULL, 1, 1, 8, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 0, NULL, 0, 0),
	(26, 2, 2, 1, 1, 1, 'Page', '2018-07-06 12:35:26', '2018-03-22 17:59:27', 'about-us', 'About Us', NULL, '<p>Dit is een faketekst. Alles wat hier staat is slechts om een indruk te geven van het grafische effect van tekst op deze plek. Wat u hier leest is een voorbeeldtekst. Deze wordt later vervangen door de uiteindelijke tekst, die nu nog niet bekend is. De faketekst is dus een tekst die eigenlijk nergens over gaat. Het grappige is, dat mensen deze toch vaak lezen. Zelfs als men weet dat het om een faketekst gaat, lezen ze toch door.</p><p>Dit is een faketekst. Alles wat hier staat is slechts om een indruk te geven van het grafische effect van tekst op deze plek. Wat u hier leest is een voorbeeldtekst. Deze wordt later vervangen door de uiteindelijke tekst, die nu nog niet bekend is. De faketekst is dus een tekst die eigenlijk nergens over gaat. Het grappige is, dat mensen deze toch vaak lezen. Zelfs als men weet dat het om een faketekst gaat, lezen ze toch door.</p><p>Dit is een faketekst. Alles wat hier staat is slechts om een indruk te geven van het grafische effect van tekst op deze plek. Wat u hier leest is een voorbeeldtekst. Deze wordt later vervangen door de uiteindelijke tekst, die nu nog niet bekend is. De faketekst is dus een tekst die eigenlijk nergens over gaat. Het grappige is, dat mensen deze toch vaak lezen. Zelfs als men weet dat het om een faketekst gaat, lezen ze toch door.</p><p>Dit is een faketekst. Alles wat hier staat is slechts om een indruk te geven van het grafische effect van tekst op deze plek. Wat u hier leest is een voorbeeldtekst. Deze wordt later vervangen door de uiteindelijke tekst, die nu nog niet bekend is. De faketekst is dus een tekst die eigenlijk nergens over gaat. Het grappige is, dat mensen deze toch vaak lezen. Zelfs als men weet dat het om een faketekst gaat, lezen ze toch door.</p><p>Dit is een faketekst. Alles wat hier staat is slechts om een indruk te geven van het grafische effect van tekst op deze plek. Wat u hier leest is een voorbeeldtekst. Deze wordt later vervangen door de uiteindelijke tekst, die nu nog niet bekend is. De faketekst is dus een tekst die eigenlijk nergens over gaat. Het grappige is, dat mensen deze toch vaak lezen. Zelfs als men weet dat het om een faketekst gaat, lezen ze toch door.</p><p>Dit is een faketekst. Alles wat hier staat is slechts om een indruk te geven van het grafische effect van tekst op deze plek. Wat u hier leest is een voorbeeldtekst. Deze wordt later vervangen door de uiteindelijke tekst, die nu nog niet bekend is. De faketekst is dus een tekst die eigenlijk nergens over gaat. Het grappige is, dat mensen deze toch vaak lezen. Zelfs als men weet dat het om een faketekst gaat, lezen ze toch door.</p>', NULL, NULL, 1, 1, 2, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 0, NULL, 0, 0),
	(27, 2, 3, 1, 1, 1, 'Page', '2018-07-06 12:35:57', '2018-03-22 17:59:27', 'about-us', 'About Us', NULL, '<p>Dit is een faketekst. Alles wat hier staat is slechts om een indruk te geven van het grafische effect van tekst op deze plek. Wat u hier leest is een voorbeeldtekst. Deze wordt later vervangen door de uiteindelijke tekst, die nu nog niet bekend is. De faketekst is dus een tekst die eigenlijk nergens over gaat. Het grappige is, dat mensen deze toch vaak lezen. Zelfs als men weet dat het om een faketekst gaat, lezen ze toch door.</p><p>Dit is een faketekst. Alles wat hier staat is slechts om een indruk te geven van het grafische effect van tekst op deze plek. Wat u hier leest is een voorbeeldtekst. Deze wordt later vervangen door de uiteindelijke tekst, die nu nog niet bekend is. De faketekst is dus een tekst die eigenlijk nergens over gaat. Het grappige is, dat mensen deze toch vaak lezen. Zelfs als men weet dat het om een faketekst gaat, lezen ze toch door.</p><p>Dit is een faketekst. Alles wat hier staat is slechts om een indruk te geven van het grafische effect van tekst op deze plek. Wat u hier leest is een voorbeeldtekst. Deze wordt later vervangen door de uiteindelijke tekst, die nu nog niet bekend is. De faketekst is dus een tekst die eigenlijk nergens over gaat. Het grappige is, dat mensen deze toch vaak lezen. Zelfs als men weet dat het om een faketekst gaat, lezen ze toch door.</p><p>Dit is een faketekst. Alles wat hier staat is slechts om een indruk te geven van het grafische effect van tekst op deze plek. Wat u hier leest is een voorbeeldtekst. Deze wordt later vervangen door de uiteindelijke tekst, die nu nog niet bekend is. De faketekst is dus een tekst die eigenlijk nergens over gaat. Het grappige is, dat mensen deze toch vaak lezen. Zelfs als men weet dat het om een faketekst gaat, lezen ze toch door.</p><p>Dit is een faketekst. Alles wat hier staat is slechts om een indruk te geven van het grafische effect van tekst op deze plek. Wat u hier leest is een voorbeeldtekst. Deze wordt later vervangen door de uiteindelijke tekst, die nu nog niet bekend is. De faketekst is dus een tekst die eigenlijk nergens over gaat. Het grappige is, dat mensen deze toch vaak lezen. Zelfs als men weet dat het om een faketekst gaat, lezen ze toch door.</p><p>Dit is een faketekst. Alles wat hier staat is slechts om een indruk te geven van het grafische effect van tekst op deze plek. Wat u hier leest is een voorbeeldtekst. Deze wordt later vervangen door de uiteindelijke tekst, die nu nog niet bekend is. De faketekst is dus een tekst die eigenlijk nergens over gaat. Het grappige is, dat mensen deze toch vaak lezen. Zelfs als men weet dat het om een faketekst gaat, lezen ze toch door.</p><p>Dit is een faketekst. Alles wat hier staat is slechts om een indruk te geven van het grafische effect van tekst op deze plek. Wat u hier leest is een voorbeeldtekst. Deze wordt later vervangen door de uiteindelijke tekst, die nu nog niet bekend is. De faketekst is dus een tekst die eigenlijk nergens over gaat. Het grappige is, dat mensen deze toch vaak lezen. Zelfs als men weet dat het om een faketekst gaat, lezen ze toch door.</p><p>Dit is een faketekst. Alles wat hier staat is slechts om een indruk te geven van het grafische effect van tekst op deze plek. Wat u hier leest is een voorbeeldtekst. Deze wordt later vervangen door de uiteindelijke tekst, die nu nog niet bekend is. De faketekst is dus een tekst die eigenlijk nergens over gaat. Het grappige is, dat mensen deze toch vaak lezen. Zelfs als men weet dat het om een faketekst gaat, lezen ze toch door.</p><p>Dit is een faketekst. Alles wat hier staat is slechts om een indruk te geven van het grafische effect van tekst op deze plek. Wat u hier leest is een voorbeeldtekst. Deze wordt later vervangen door de uiteindelijke tekst, die nu nog niet bekend is. De faketekst is dus een tekst die eigenlijk nergens over gaat. Het grappige is, dat mensen deze toch vaak lezen. Zelfs als men weet dat het om een faketekst gaat, lezen ze toch door.</p>', NULL, NULL, 1, 1, 2, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 0, NULL, 0, 0),
	(28, 9, 5, 0, 1, 0, 'Page', '2018-07-06 13:54:51', '2018-07-06 13:54:51', 'nieuwe-page', 'Nieuwe Page', NULL, NULL, NULL, NULL, 1, 1, 9, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 0, NULL, 0, 0),
	(29, 9, 6, 1, 1, 1, 'Page', '2018-07-06 13:55:13', '2018-07-06 13:54:51', 'privacyverklaring', 'Privacyverklaring', NULL, NULL, NULL, NULL, 1, 1, 9, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 0, NULL, 0, 0),
	(30, 9, 7, 1, 1, 1, 'Page', '2018-07-06 13:55:26', '2018-07-06 13:54:51', 'privacyverklaring', 'Privacyverklaring', NULL, NULL, NULL, NULL, 0, 1, 9, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 0, NULL, 0, 0),
	(31, 9, 8, 1, 1, 1, 'Page', '2018-07-06 13:56:30', '2018-07-06 13:54:51', 'privacyverklaring', 'Privacyverklaring', NULL, '<h2>Handelsmerken</h2><p>De op deze internetpagina (â€˜websiteâ€™) genoemde handelsmerken of bedrijfsnamen kunnen een handelsmerk zijn. De rechten van deze handelmerken berusten bij de betreffende rechthebbende.</p><h2>Copyright</h2><p>Het copyright op de inhoud van deze website berust bij Hestec internetdiensten. Het is verboden om de inhoud van deze website in zijn geheel of gedeeltelijk te gebruiken, te kopiÃ«ren of over te nemen in enig ander werk, behalve voor:<br>het afdrukken of downloaden van delen van de inhoud van deze website voor niet-commercieel, informatief en persoonlijk gebruik; en&nbsp;het kopiÃ«ren van de inhoud van deze website om geÃ¯nteresseerde derde partijen persoonlijk te informeren op voorwaarde dat u Hestec internetdiensten noemt als de bron van de inhoud en dat u deze derden ervan op de hoogte stelt dat deze voorwaarden ook voor hen gelden en dat ook zij verplicht zijn de voorwaarden na te leven.</p><h2>Disclaimer: aansprakelijkheidsbeperkingen</h2><p>Hestec internetdiensten kan en wil de juistheid en de volledigheid van de informatie, de inhoud en de afbeeldingen op deze website niet garanderen. Hestec internetdiensten tracht u met deze website slechts â€œvrijblijvendâ€ te informeren over zijn producten en diensten. Voor zover wettelijk toegestaan stelt Hestec internetdiensten zich uitdrukkelijk niet aansprakelijk voor de juistheid en volledigheid van de informatie, inhoud en afbeeldingen op de website voor zover de website hiermee expliciet of impliciet verwachtingen zou scheppen over commercieel gebruik of andere toepassingen en over bepaalde productkenmerken of gebruikseigenschappen, ongeacht de rechtsgrond en ongeacht of Hestec internetdiensten, diens werknemers of vertegenwoordigers hierbij in gebreke zijn gebleven.<br>Hestec internetdiensten is in geen enkel opzicht aansprakelijk voor enige specifieke schade, indirecte schade, gevolgschade of schade vanwege uitval, gegevensverlies of winstderving, voortkomende of verband houdende met het gebruik of de juistheid van de informatie, inhoud of afbeeldingen op deze website, ongeacht of deze schade is ontstaan onder uitvoering van een overeenkomst, door nalatigheid of door een onrechtmatige daad.</p><h2>Privacyverklaring</h2><p>Hestec internetdiensten kan persoonlijke gegevens, zoals het Internet Protocol (IP) adres, opslaan wanneer u onze website bezoekt. Hestec internetdiensten zal deze informatie in lijn met de Wet Bescherming Persoonsgegevens, vertrouwelijk behandelen. Hestec internetdiensten&nbsp;gebruikt deze gegevens enkel en alleen om te reageren op vragen van u, om het bezoekersgedrag op de website te analyseren en voor marketingdoeleinden.</p><h2>Cookies</h2><p>De website van Hestec internetdiensten gebruikt cookies.</p><p><strong>Wat is een cookie?</strong><br>Een cookie is een klein tekstbestand dat tijdens je bezoek aan een website op bijvoorbeeld je computer, tablet of smartphone wordt geplaatst. In dit tekstbestand wordt informatie opgeslagen. Deze informatie kan bij een later bezoek weer worden herkend door deze website. De belangrijkste functie van cookies is om de ene gebruiker van de andere te onderscheiden. Je komt cookies dan ook veel tegen bij websites waarbij je moet inloggen. Een cookie zorgt dat je ingelogd blijft terwijl je de site gebruikt.</p><p>Hestec internetdiensten gebruikt de volgende cookies:</p><ul><li>Marketing (tracking en remarketing) cookies:<br>Met behulp van tracking cookies wordt het surfgedrag van een bezoeker van onze website gevolgd. Deze gegevens worden gebruikt om jou relevante advertenties te kunnen bieden op andere websites.</li>\n<li>Analytische cookies:<br>Wij gebruiken analytische cookies om bijvoorbeeld te zien hoe je onze website gebruikt, zodat de kwaliteit en/of de effectiviteit van de website kan worden verbeterd. Zo krijgen wij beter inzicht in het functioneren van de website en kunnen wij de site blijven verbeteren.</li>\n<li>Functionele cookies:<br>Deze cookies zijn nodig om deze site te laten functioneren. Je kan hierbij denken aan bestanden die je inloggegevens opslaan. Zo hoeft je niet iedere keer opnieuw je gegevens in te vullen. Functionele cookies maken weinig inbreuk op de privacy.</li>\n</ul><p><strong>Toelichting specifieke cookies</strong></p><ul><li><strong>Google Analytics</strong><br>Hestec internetdiensten maakt gebruik van analytische cookies via Google Analytics. Hierbij worden statistieken over aantallen bezoekers, de bezochte paginaâ€™s en de verkeersbronnen anoniem via het Google Analytics account van Hestec internetdiensten verzameld.\n<p>Analytische Cookies blijven maximaal 2 jaar geldig.</p>\n<p>Hestec internetdiensten heeft het Google Analytics account zo ingesteld dat:</p>\n<ul><li>Er een verwerkersovereenkomst tussen Hestec internetdiensten en Google is over de Analytics gegevens.</li>\n<li>IP-adressen anoniem worden gemaakt zodat gegevens nooit naar jou of jouw IP-adres te herleiden zijn.</li>\n<li>De informatie niet met anderen gedeeld wordt.</li>\n<li>Er geen profielen worden gemaakt n.a.v. interesses, leeftijd, enz.</li>\n<li>Hestec internetdiensten geeft je de mogelijkheid voor een â€˜opt-outâ€™. Hiermee telt jouw bezoek niet meer mee in de statistieken van alle websites die Google Analytics gebruiken. Klik hier om dit in te stellen.</li>\n</ul></li>\n<li><strong>Tawk.to</strong><br>Hestec internetdiensten maakt gebruik van Tawk.to. Deze dienst maakt het voor jou mogelijk om contact met ons op te nemen via een chatfunctie. Deze dienst is essentieel als je via de chat contact wilt opnemen met de klantenservice. Met deze cookies kunnen we jouw bezoek op de pagina blijven volgen. Tawk.to plaatst hiervoor een tracking cookie. Tawk.to slaat geen persoonlijke informatie op tenzij het is vereist voor de werking van de dienst. Tawk.to verwerkt ook jouw IP-adres.<br><br><a rel="noopener" href="https://www.tawk.to/data-protection/gdpr/" target="_blank">Klik hier voor de GDPR verklaring van Tawk.to</a></li>\n</ul><p><strong>Hoe kun je cookies verwijderen?</strong><br>Je cookies kun je verwijderen via je browser. klik hieronder op door jou gebruikte browser voor instructies:</p><ul><li><a rel="noopener" href="http://support.google.com/chrome/bin/answer.py?hl=nl&amp;answer=95647" target="_blank">Google Chrome</a></li>\n<li><a rel="noopener" href="http://windows.microsoft.com/nl-NL/windows-vista/Block-or-allow-cookies" target="_blank">Internet Explorer</a></li>\n<li><a rel="noopener" href="http://support.mozilla.org/nl/kb/Cookies%20in-%20en%20uitschakelen" target="_blank">Firefox</a></li>\n<li><a rel="noopener" href="http://support.apple.com/kb/PH5042" target="_blank">Safari</a></li>\n<li><a rel="noopener" href="https://support.microsoft.com/en-us/help/4027947/windows-delete-cookies" target="_blank">Microsoft Edge</a></li>\n<li><a rel="noopener" href="https://www.opera.com/help/tutorials/security/privacy/" target="_blank">Opera</a></li>\n</ul><p>&nbsp;</p>', NULL, NULL, 0, 1, 9, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 0, NULL, 0, 0),
	(32, 10, 3, 0, 1, 0, 'Page', '2018-07-09 15:50:49', '2018-07-09 15:50:49', 'nieuwe-page', 'Nieuwe Page', NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 6, NULL, 0, 0),
	(33, 10, 4, 1, 1, 1, 'Page', '2018-07-09 15:51:28', '2018-07-09 15:50:49', 'pagina-als-dropdown', 'Pagina als dropdown', NULL, '<p>Dit is een faketekst. Alles wat hier staat is slechts om een indruk te geven van het grafische effect van tekst op deze plek. Wat u hier leest is een voorbeeldtekst. Deze wordt later vervangen door de uiteindelijke tekst, die nu nog niet bekend is. De faketekst is dus een tekst die eigenlijk nergens over gaat. Het grappige is, dat mensen deze toch vaak lezen. Zelfs als men weet dat het om een faketekst gaat, lezen ze toch door.</p><p>Dit is een faketekst. Alles wat hier staat is slechts om een indruk te geven van het grafische effect van tekst op deze plek. Wat u hier leest is een voorbeeldtekst. Deze wordt later vervangen door de uiteindelijke tekst, die nu nog niet bekend is. De faketekst is dus een tekst die eigenlijk nergens over gaat. Het grappige is, dat mensen deze toch vaak lezen. Zelfs als men weet dat het om een faketekst gaat, lezen ze toch door.</p>', NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 6, NULL, 0, 0),
	(34, 11, 3, 0, 1, 0, 'Page', '2018-07-09 15:51:46', '2018-07-09 15:51:46', 'nieuwe-page', 'Nieuwe Page', NULL, NULL, NULL, NULL, 1, 1, 2, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 6, NULL, 0, 0),
	(35, 11, 4, 1, 1, 1, 'Page', '2018-07-09 15:52:06', '2018-07-09 15:51:46', 'nog-een-pagina', 'Nog een pagina', NULL, '<p>Dit is een faketekst. Alles wat hier staat is slechts om een indruk te geven van het grafische effect van tekst op deze plek. Wat u hier leest is een voorbeeldtekst. Deze wordt later vervangen door de uiteindelijke tekst, die nu nog niet bekend is. De faketekst is dus een tekst die eigenlijk nergens over gaat. Het grappige is, dat mensen deze toch vaak lezen. Zelfs als men weet dat het om een faketekst gaat, lezen ze toch door.</p><p>Dit is een faketekst. Alles wat hier staat is slechts om een indruk te geven van het grafische effect van tekst op deze plek. Wat u hier leest is een voorbeeldtekst. Deze wordt later vervangen door de uiteindelijke tekst, die nu nog niet bekend is. De faketekst is dus een tekst die eigenlijk nergens over gaat. Het grappige is, dat mensen deze toch vaak lezen. Zelfs als men weet dat het om een faketekst gaat, lezen ze toch door.</p>', NULL, NULL, 1, 1, 2, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 6, NULL, 0, 0),
	(36, 12, 1, 0, 1, 0, 'FirstChildRedirect', '2018-07-10 14:39:17', '2018-07-10 14:39:17', 'nieuwe-first-child-redirect', 'Nieuwe First Child Redirect', NULL, NULL, NULL, NULL, 1, 1, 10, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 0, NULL, 0, 0),
	(37, 12, 2, 1, 1, 1, 'FirstChildRedirect', '2018-07-10 14:39:42', '2018-07-10 14:39:17', 'dropdown', 'Dropdown', NULL, NULL, NULL, NULL, 1, 1, 10, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 0, NULL, 0, 0),
	(38, 12, 3, 1, 1, 1, 'FirstChildRedirect', '2018-07-10 14:39:47', '2018-07-10 14:39:17', 'dropdown', 'Dropdown', NULL, NULL, NULL, NULL, 1, 1, 6, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 0, NULL, 0, 0),
	(39, 10, 5, 1, 1, 1, 'Page', '2018-07-10 14:39:57', '2018-07-09 15:50:49', 'pagina-als-dropdown', 'Pagina als dropdown', NULL, '<p>Dit is een faketekst. Alles wat hier staat is slechts om een indruk te geven van het grafische effect van tekst op deze plek. Wat u hier leest is een voorbeeldtekst. Deze wordt later vervangen door de uiteindelijke tekst, die nu nog niet bekend is. De faketekst is dus een tekst die eigenlijk nergens over gaat. Het grappige is, dat mensen deze toch vaak lezen. Zelfs als men weet dat het om een faketekst gaat, lezen ze toch door.</p><p>Dit is een faketekst. Alles wat hier staat is slechts om een indruk te geven van het grafische effect van tekst op deze plek. Wat u hier leest is een voorbeeldtekst. Deze wordt later vervangen door de uiteindelijke tekst, die nu nog niet bekend is. De faketekst is dus een tekst die eigenlijk nergens over gaat. Het grappige is, dat mensen deze toch vaak lezen. Zelfs als men weet dat het om een faketekst gaat, lezen ze toch door.</p>', NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 12, NULL, 0, 0),
	(40, 11, 5, 1, 1, 1, 'Page', '2018-07-10 14:40:01', '2018-07-09 15:51:46', 'nog-een-pagina', 'Nog een pagina', NULL, '<p>Dit is een faketekst. Alles wat hier staat is slechts om een indruk te geven van het grafische effect van tekst op deze plek. Wat u hier leest is een voorbeeldtekst. Deze wordt later vervangen door de uiteindelijke tekst, die nu nog niet bekend is. De faketekst is dus een tekst die eigenlijk nergens over gaat. Het grappige is, dat mensen deze toch vaak lezen. Zelfs als men weet dat het om een faketekst gaat, lezen ze toch door.</p><p>Dit is een faketekst. Alles wat hier staat is slechts om een indruk te geven van het grafische effect van tekst op deze plek. Wat u hier leest is een voorbeeldtekst. Deze wordt later vervangen door de uiteindelijke tekst, die nu nog niet bekend is. De faketekst is dus een tekst die eigenlijk nergens over gaat. Het grappige is, dat mensen deze toch vaak lezen. Zelfs als men weet dat het om een faketekst gaat, lezen ze toch door.</p>', NULL, NULL, 1, 1, 2, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 12, NULL, 0, 0),
	(41, 6, 4, 0, 1, 0, 'ElementPage', '2018-07-10 14:40:24', '2018-03-29 20:45:22', 'elements', 'Elements', NULL, NULL, NULL, NULL, 1, 1, 7, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 12, NULL, 0, 0),
	(42, 6, 5, 1, 1, 1, 'ElementPage', '2018-07-10 14:40:24', '2018-03-29 20:45:22', 'elements', 'Elements', NULL, NULL, NULL, NULL, 1, 1, 3, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 12, NULL, 0, 0),
	(43, 1, 3, 0, 1, 0, 'HomePage', '2018-08-07 16:52:47', '2018-03-22 17:59:27', 'home', 'Home', NULL, '<p>Welcome to SilverStripe! This is the default homepage. You can edit this page by opening <a href="admin/">the CMS</a>.</p><p>You can now access the <a href="http://docs.silverstripe.org">developer documentation</a>, or begin the <a href="http://www.silverstripe.org/learn/lessons">SilverStripe lessons</a>.</p>', NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 0, NULL, 0, 1),
	(44, 1, 4, 1, 1, 1, 'HomePage', '2018-08-07 16:52:47', '2018-03-22 17:59:27', 'home', 'Home', NULL, '<p>Welcome to SilverStripe! This is the default homepage. You can edit this page by opening <a href="admin/">the CMS</a>.</p><p>You can now access the <a href="http://docs.silverstripe.org">developer documentation</a>, or begin the <a href="http://www.silverstripe.org/learn/lessons">SilverStripe lessons</a>.</p>', NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 0, NULL, 0, 1),
	(45, 8, 7, 0, 1, 0, 'Hestec\\Faqpage\\FaqPage', '2018-09-18 10:22:54', '2018-07-06 12:22:53', 'veelgestelde-vragen', 'Veelgestelde vragen', NULL, NULL, NULL, NULL, 1, 1, 9, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 0, NULL, 0, 1),
	(46, 8, 8, 1, 1, 1, 'Hestec\\Faqpage\\FaqPage', '2018-09-18 10:22:54', '2018-07-06 12:22:53', 'veelgestelde-vragen', 'Veelgestelde vragen', NULL, NULL, NULL, NULL, 1, 1, 9, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 0, NULL, 0, 1);
/*!40000 ALTER TABLE `SiteTree_Versions` ENABLE KEYS */;


-- Structuur van  tabel vagrant.SiteTree_ViewerGroups wordt geschreven
CREATE TABLE IF NOT EXISTS `SiteTree_ViewerGroups` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SiteTreeID` int(11) NOT NULL DEFAULT '0',
  `GroupID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `SiteTreeID` (`SiteTreeID`),
  KEY `GroupID` (`GroupID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.SiteTree_ViewerGroups: ~0 rows (ongeveer)
DELETE FROM `SiteTree_ViewerGroups`;
/*!40000 ALTER TABLE `SiteTree_ViewerGroups` DISABLE KEYS */;
/*!40000 ALTER TABLE `SiteTree_ViewerGroups` ENABLE KEYS */;


-- Structuur van  tabel vagrant.SubmittedFileField wordt geschreven
CREATE TABLE IF NOT EXISTS `SubmittedFileField` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `UploadedFileID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `UploadedFileID` (`UploadedFileID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.SubmittedFileField: ~0 rows (ongeveer)
DELETE FROM `SubmittedFileField`;
/*!40000 ALTER TABLE `SubmittedFileField` DISABLE KEYS */;
/*!40000 ALTER TABLE `SubmittedFileField` ENABLE KEYS */;


-- Structuur van  tabel vagrant.SubmittedForm wordt geschreven
CREATE TABLE IF NOT EXISTS `SubmittedForm` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('SilverStripe\\UserForms\\Model\\Submission\\SubmittedForm') CHARACTER SET utf8 DEFAULT 'SilverStripe\\UserForms\\Model\\Submission\\SubmittedForm',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `SubmittedByID` int(11) NOT NULL DEFAULT '0',
  `ParentID` int(11) NOT NULL DEFAULT '0',
  `ParentClass` enum('Hestec\\SisowMethod\\SisowMethod','Hestec\\ElementalExtensions\\Dataobjects\\FaqQuestion','Hestec\\ExactOnline\\ExactOnlineConnection','Hestec\\FaqPage\\FaqCategorie','Hestec\\FaqPage\\FaqQuestion','BrandColor','BrandColorType','ExtraNavigationItem','Hestec\\Tools\\OwnIp','SilverStripe\\Assets\\File','SilverStripe\\SiteConfig\\SiteConfig','SilverStripe\\Versioned\\ChangeSetItem','SilverStripe\\Versioned\\ChangeSet','DNADesign\\Elemental\\Models\\BaseElement','DNADesign\\Elemental\\Models\\ElementalArea','gorriecoe\\Link\\Models\\Link','gorriecoe\\Menu\\Models\\MenuSet','SilverStripe\\Assets\\Shortcodes\\FileLink','SilverStripe\\Blog\\Model\\BlogCategory','SilverStripe\\Blog\\Model\\BlogTag','SilverStripe\\CMS\\Model\\SiteTreeLink','SilverStripe\\CMS\\Model\\SiteTree','SilverStripe\\Security\\Group','SilverStripe\\Security\\LoginAttempt','SilverStripe\\Security\\MemberPassword','SilverStripe\\Security\\Member','SilverStripe\\Security\\Permission','SilverStripe\\Security\\PermissionRoleCode','SilverStripe\\Security\\PermissionRole','SilverStripe\\Security\\RememberLoginHash','SilverStripe\\UserForms\\Model\\EditableCustomRule','SilverStripe\\UserForms\\Model\\EditableFormField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableOption','SilverStripe\\UserForms\\Model\\Recipient\\EmailRecipientCondition','SilverStripe\\UserForms\\Model\\Recipient\\EmailRecipient','SilverStripe\\UserForms\\Model\\Submission\\SubmittedFormField','SilverStripe\\UserForms\\Model\\Submission\\SubmittedForm','SilverStripe\\Assets\\Folder','SilverStripe\\Assets\\Image','Hestec\\ElementalExtensions\\Elements\\ElementContentImage','Hestec\\ElementalExtensions\\Elements\\ElementFaq','Hestec\\ElementalExtensions\\Elements\\ElementVideo','DNADesign\\Elemental\\Models\\ElementContent','gorriecoe\\Menu\\Models\\MenuLink','Page','ElementPage','HomePage','Hestec\\Faqpage\\FaqPage','FirstChildRedirect','SilverStripe\\ErrorPage\\ErrorPage','SilverStripe\\Blog\\Model\\Blog','SilverStripe\\Blog\\Model\\BlogPost','SilverStripe\\CMS\\Model\\RedirectorPage','SilverStripe\\CMS\\Model\\VirtualPage','SilverStripe\\UserForms\\Model\\UserDefinedForm','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableCheckbox','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableCountryDropdownField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableDateField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableEmailField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableFieldGroupEnd','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableFieldGroup','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableFileField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableFormHeading','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableFormStep','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableLiteralField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableMemberListField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableMultipleOptionField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableNumericField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableTextField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableCheckboxGroupField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableDropdown','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableRadioField','SilverStripe\\UserForms\\Model\\Submission\\SubmittedFileField') CHARACTER SET utf8 DEFAULT 'Hestec\\SisowMethod\\SisowMethod',
  PRIMARY KEY (`ID`),
  KEY `ClassName` (`ClassName`),
  KEY `SubmittedByID` (`SubmittedByID`),
  KEY `Parent` (`ParentID`,`ParentClass`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.SubmittedForm: ~0 rows (ongeveer)
DELETE FROM `SubmittedForm`;
/*!40000 ALTER TABLE `SubmittedForm` DISABLE KEYS */;
/*!40000 ALTER TABLE `SubmittedForm` ENABLE KEYS */;


-- Structuur van  tabel vagrant.SubmittedFormField wordt geschreven
CREATE TABLE IF NOT EXISTS `SubmittedFormField` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('SilverStripe\\UserForms\\Model\\Submission\\SubmittedFormField','SilverStripe\\UserForms\\Model\\Submission\\SubmittedFileField') CHARACTER SET utf8 DEFAULT 'SilverStripe\\UserForms\\Model\\Submission\\SubmittedFormField',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Value` mediumtext CHARACTER SET utf8,
  `Title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `ParentID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ClassName` (`ClassName`),
  KEY `ParentID` (`ParentID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.SubmittedFormField: ~0 rows (ongeveer)
DELETE FROM `SubmittedFormField`;
/*!40000 ALTER TABLE `SubmittedFormField` DISABLE KEYS */;
/*!40000 ALTER TABLE `SubmittedFormField` ENABLE KEYS */;


-- Structuur van  tabel vagrant.UserDefinedForm wordt geschreven
CREATE TABLE IF NOT EXISTS `UserDefinedForm` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SubmitButtonText` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `ClearButtonText` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `OnCompleteMessage` mediumtext CHARACTER SET utf8,
  `ShowClearButton` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `DisableSaveSubmissions` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `EnableLiveValidation` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `DisplayErrorMessagesAtTop` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `DisableAuthenicatedFinishAction` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `DisableCsrfSecurityToken` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.UserDefinedForm: ~0 rows (ongeveer)
DELETE FROM `UserDefinedForm`;
/*!40000 ALTER TABLE `UserDefinedForm` DISABLE KEYS */;
/*!40000 ALTER TABLE `UserDefinedForm` ENABLE KEYS */;


-- Structuur van  tabel vagrant.UserDefinedForm_EmailRecipient wordt geschreven
CREATE TABLE IF NOT EXISTS `UserDefinedForm_EmailRecipient` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('SilverStripe\\UserForms\\Model\\Recipient\\EmailRecipient') CHARACTER SET utf8 DEFAULT 'SilverStripe\\UserForms\\Model\\Recipient\\EmailRecipient',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `EmailAddress` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `EmailSubject` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `EmailFrom` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `EmailReplyTo` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `EmailBody` mediumtext CHARACTER SET utf8,
  `EmailBodyHtml` mediumtext CHARACTER SET utf8,
  `EmailTemplate` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `SendPlain` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `HideFormData` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `CustomRulesCondition` enum('And','Or') CHARACTER SET utf8 DEFAULT 'And',
  `SendEmailFromFieldID` int(11) NOT NULL DEFAULT '0',
  `SendEmailToFieldID` int(11) NOT NULL DEFAULT '0',
  `SendEmailSubjectFieldID` int(11) NOT NULL DEFAULT '0',
  `FormID` int(11) NOT NULL DEFAULT '0',
  `FormClass` enum('Hestec\\SisowMethod\\SisowMethod','Hestec\\ElementalExtensions\\Dataobjects\\FaqQuestion','Hestec\\ExactOnline\\ExactOnlineConnection','Hestec\\FaqPage\\FaqCategorie','Hestec\\FaqPage\\FaqQuestion','BrandColor','BrandColorType','ExtraNavigationItem','Hestec\\Tools\\OwnIp','SilverStripe\\Assets\\File','SilverStripe\\SiteConfig\\SiteConfig','SilverStripe\\Versioned\\ChangeSetItem','SilverStripe\\Versioned\\ChangeSet','DNADesign\\Elemental\\Models\\BaseElement','DNADesign\\Elemental\\Models\\ElementalArea','gorriecoe\\Link\\Models\\Link','gorriecoe\\Menu\\Models\\MenuSet','SilverStripe\\Assets\\Shortcodes\\FileLink','SilverStripe\\Blog\\Model\\BlogCategory','SilverStripe\\Blog\\Model\\BlogTag','SilverStripe\\CMS\\Model\\SiteTreeLink','SilverStripe\\CMS\\Model\\SiteTree','SilverStripe\\Security\\Group','SilverStripe\\Security\\LoginAttempt','SilverStripe\\Security\\MemberPassword','SilverStripe\\Security\\Member','SilverStripe\\Security\\Permission','SilverStripe\\Security\\PermissionRoleCode','SilverStripe\\Security\\PermissionRole','SilverStripe\\Security\\RememberLoginHash','SilverStripe\\UserForms\\Model\\EditableCustomRule','SilverStripe\\UserForms\\Model\\EditableFormField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableOption','SilverStripe\\UserForms\\Model\\Recipient\\EmailRecipientCondition','SilverStripe\\UserForms\\Model\\Recipient\\EmailRecipient','SilverStripe\\UserForms\\Model\\Submission\\SubmittedFormField','SilverStripe\\UserForms\\Model\\Submission\\SubmittedForm','SilverStripe\\Assets\\Folder','SilverStripe\\Assets\\Image','Hestec\\ElementalExtensions\\Elements\\ElementContentImage','Hestec\\ElementalExtensions\\Elements\\ElementFaq','Hestec\\ElementalExtensions\\Elements\\ElementVideo','DNADesign\\Elemental\\Models\\ElementContent','gorriecoe\\Menu\\Models\\MenuLink','Page','ElementPage','HomePage','Hestec\\Faqpage\\FaqPage','FirstChildRedirect','SilverStripe\\ErrorPage\\ErrorPage','SilverStripe\\Blog\\Model\\Blog','SilverStripe\\Blog\\Model\\BlogPost','SilverStripe\\CMS\\Model\\RedirectorPage','SilverStripe\\CMS\\Model\\VirtualPage','SilverStripe\\UserForms\\Model\\UserDefinedForm','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableCheckbox','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableCountryDropdownField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableDateField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableEmailField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableFieldGroupEnd','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableFieldGroup','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableFileField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableFormHeading','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableFormStep','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableLiteralField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableMemberListField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableMultipleOptionField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableNumericField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableTextField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableCheckboxGroupField','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableDropdown','SilverStripe\\UserForms\\Model\\EditableFormField\\EditableRadioField','SilverStripe\\UserForms\\Model\\Submission\\SubmittedFileField') CHARACTER SET utf8 DEFAULT 'Hestec\\SisowMethod\\SisowMethod',
  PRIMARY KEY (`ID`),
  KEY `ClassName` (`ClassName`),
  KEY `SendEmailFromFieldID` (`SendEmailFromFieldID`),
  KEY `SendEmailToFieldID` (`SendEmailToFieldID`),
  KEY `SendEmailSubjectFieldID` (`SendEmailSubjectFieldID`),
  KEY `Form` (`FormID`,`FormClass`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.UserDefinedForm_EmailRecipient: ~0 rows (ongeveer)
DELETE FROM `UserDefinedForm_EmailRecipient`;
/*!40000 ALTER TABLE `UserDefinedForm_EmailRecipient` DISABLE KEYS */;
/*!40000 ALTER TABLE `UserDefinedForm_EmailRecipient` ENABLE KEYS */;


-- Structuur van  tabel vagrant.UserDefinedForm_EmailRecipientCondition wordt geschreven
CREATE TABLE IF NOT EXISTS `UserDefinedForm_EmailRecipientCondition` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('SilverStripe\\UserForms\\Model\\Recipient\\EmailRecipientCondition') CHARACTER SET utf8 DEFAULT 'SilverStripe\\UserForms\\Model\\Recipient\\EmailRecipientCondition',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `ConditionOption` enum('IsBlank','IsNotBlank','Equals','NotEquals','ValueLessThan','ValueLessThanEqual','ValueGreaterThan','ValueGreaterThanEqual') CHARACTER SET utf8 DEFAULT 'IsBlank',
  `ConditionValue` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `ParentID` int(11) NOT NULL DEFAULT '0',
  `ConditionFieldID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ClassName` (`ClassName`),
  KEY `ParentID` (`ParentID`),
  KEY `ConditionFieldID` (`ConditionFieldID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.UserDefinedForm_EmailRecipientCondition: ~0 rows (ongeveer)
DELETE FROM `UserDefinedForm_EmailRecipientCondition`;
/*!40000 ALTER TABLE `UserDefinedForm_EmailRecipientCondition` DISABLE KEYS */;
/*!40000 ALTER TABLE `UserDefinedForm_EmailRecipientCondition` ENABLE KEYS */;


-- Structuur van  tabel vagrant.UserDefinedForm_Live wordt geschreven
CREATE TABLE IF NOT EXISTS `UserDefinedForm_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SubmitButtonText` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `ClearButtonText` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `OnCompleteMessage` mediumtext CHARACTER SET utf8,
  `ShowClearButton` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `DisableSaveSubmissions` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `EnableLiveValidation` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `DisplayErrorMessagesAtTop` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `DisableAuthenicatedFinishAction` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `DisableCsrfSecurityToken` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.UserDefinedForm_Live: ~0 rows (ongeveer)
DELETE FROM `UserDefinedForm_Live`;
/*!40000 ALTER TABLE `UserDefinedForm_Live` DISABLE KEYS */;
/*!40000 ALTER TABLE `UserDefinedForm_Live` ENABLE KEYS */;


-- Structuur van  tabel vagrant.UserDefinedForm_Versions wordt geschreven
CREATE TABLE IF NOT EXISTS `UserDefinedForm_Versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `SubmitButtonText` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `ClearButtonText` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `OnCompleteMessage` mediumtext CHARACTER SET utf8,
  `ShowClearButton` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `DisableSaveSubmissions` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `EnableLiveValidation` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `DisplayErrorMessagesAtTop` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `DisableAuthenicatedFinishAction` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `DisableCsrfSecurityToken` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.UserDefinedForm_Versions: ~0 rows (ongeveer)
DELETE FROM `UserDefinedForm_Versions`;
/*!40000 ALTER TABLE `UserDefinedForm_Versions` DISABLE KEYS */;
/*!40000 ALTER TABLE `UserDefinedForm_Versions` ENABLE KEYS */;


-- Structuur van  tabel vagrant.VirtualPage wordt geschreven
CREATE TABLE IF NOT EXISTS `VirtualPage` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `VersionID` int(11) NOT NULL DEFAULT '0',
  `CopyContentFromID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `CopyContentFromID` (`CopyContentFromID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.VirtualPage: ~0 rows (ongeveer)
DELETE FROM `VirtualPage`;
/*!40000 ALTER TABLE `VirtualPage` DISABLE KEYS */;
/*!40000 ALTER TABLE `VirtualPage` ENABLE KEYS */;


-- Structuur van  tabel vagrant.VirtualPage_Live wordt geschreven
CREATE TABLE IF NOT EXISTS `VirtualPage_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `VersionID` int(11) NOT NULL DEFAULT '0',
  `CopyContentFromID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `CopyContentFromID` (`CopyContentFromID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.VirtualPage_Live: ~0 rows (ongeveer)
DELETE FROM `VirtualPage_Live`;
/*!40000 ALTER TABLE `VirtualPage_Live` DISABLE KEYS */;
/*!40000 ALTER TABLE `VirtualPage_Live` ENABLE KEYS */;


-- Structuur van  tabel vagrant.VirtualPage_Versions wordt geschreven
CREATE TABLE IF NOT EXISTS `VirtualPage_Versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `VersionID` int(11) NOT NULL DEFAULT '0',
  `CopyContentFromID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`),
  KEY `CopyContentFromID` (`CopyContentFromID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant.VirtualPage_Versions: ~0 rows (ongeveer)
DELETE FROM `VirtualPage_Versions`;
/*!40000 ALTER TABLE `VirtualPage_Versions` DISABLE KEYS */;
/*!40000 ALTER TABLE `VirtualPage_Versions` ENABLE KEYS */;


-- Structuur van  tabel vagrant._obsolete_Page wordt geschreven
CREATE TABLE IF NOT EXISTS `_obsolete_Page` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ElementalAreaID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ElementalAreaID` (`ElementalAreaID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant._obsolete_Page: ~0 rows (ongeveer)
DELETE FROM `_obsolete_Page`;
/*!40000 ALTER TABLE `_obsolete_Page` DISABLE KEYS */;
INSERT INTO `_obsolete_Page` (`ID`, `ElementalAreaID`) VALUES
	(1, 1);
/*!40000 ALTER TABLE `_obsolete_Page` ENABLE KEYS */;


-- Structuur van  tabel vagrant._obsolete_Page_Live wordt geschreven
CREATE TABLE IF NOT EXISTS `_obsolete_Page_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ElementalAreaID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ElementalAreaID` (`ElementalAreaID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant._obsolete_Page_Live: ~0 rows (ongeveer)
DELETE FROM `_obsolete_Page_Live`;
/*!40000 ALTER TABLE `_obsolete_Page_Live` DISABLE KEYS */;
INSERT INTO `_obsolete_Page_Live` (`ID`, `ElementalAreaID`) VALUES
	(1, 1);
/*!40000 ALTER TABLE `_obsolete_Page_Live` ENABLE KEYS */;


-- Structuur van  tabel vagrant._obsolete_Page_Versions wordt geschreven
CREATE TABLE IF NOT EXISTS `_obsolete_Page_Versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `ElementalAreaID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`),
  KEY `ElementalAreaID` (`ElementalAreaID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel vagrant._obsolete_Page_Versions: ~0 rows (ongeveer)
DELETE FROM `_obsolete_Page_Versions`;
/*!40000 ALTER TABLE `_obsolete_Page_Versions` DISABLE KEYS */;
INSERT INTO `_obsolete_Page_Versions` (`ID`, `RecordID`, `Version`, `ElementalAreaID`) VALUES
	(1, 1, 2, 1);
/*!40000 ALTER TABLE `_obsolete_Page_Versions` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
