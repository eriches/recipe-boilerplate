<?php

use SilverStripe\CMS\Controllers\ContentController;
use SilverStripe\Control\Director;
use SilverStripe\View\Requirements;
use Hestec\Tools\Forms\BootstrapCheckboxField;
use SilverStripe\Forms\FormAction;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\Form;

class PageController extends ContentController
{
    /**
     * An array of actions that can be accessed via a request. Each array element should be an action name, and the
     * permissions or conditions required to allow the user to access it.
     *
     * <code>
     * array (
     *     'action', // anyone can access this action
     *     'action' => true, // same as above
     *     'action' => 'ADMIN', // you must have ADMIN permissions to access this action
     *     'action' => '->checkAction' // you can only access this action if $this->checkAction() returns true
     * );
     * </code>
     *
     * @var array
     */
    private static $allowed_actions = array(
        'TestForm'
    );

    protected function init() {
        parent::init();

        if (class_exists('Translatable')) {
            i18n::set_locale($this->Locale);
        }

        Requirements::block("framework/thirdparty/jquery/jquery.js");

        if ( Director::isLive() ){
            // the  compressed main.css file (for the live environment)
            Requirements::themedCSS('main');
            Requirements::javascript("themes/main/js/main.min.js");
        }else{
            Requirements::themedCSS('layout');
            Requirements::javascript("themes/main/js/main.js");
            Requirements::javascript("themes/main/js/mailchimp.js");
            Requirements::javascript("themes/main/js/tp/holder.min.js");
        }

    }

    public function TestForm(){

        $NoGasField = BootstrapCheckboxField::create('NoGas', "Alleen stroom vergelijken (zonder gas)");
        //$NoGasField->addExtraClass("custom-control-input");


        $Action = FormAction::create('SubmitTestForm', "Submit >");
        $Action->addExtraClass("orange");

        $fields = FieldList::create(array(
            $NoGasField
        ));

        $actions = FieldList::create(
            $Action
        );


        //$validator = Validator::create();
        /*$validator->addRequiredFields(array(
            'Name'
        ));*/
        /*$validator = new RequiredFields([
            'Name'
        ]);*/

        /*$validator->addRequiredFields([
            'Zip' => "Postcode is verplicht.",
            'HouseNumber' => "Huisnummer is verplicht."
        ]);*/

        $form = Form::create($this, __FUNCTION__, $fields, $actions);
        //$form->setTemplate('ZipCheckEnergyForm');

        //if (isset($Params['OtherID']) && is_numeric($Params['OtherID'])) {

        //$form->loadDataFrom(PlanMiddleItem::get()->byID($Params['OtherID']));
        //$form->loadDataFrom(array('Arrival' => "01/09/2018"));


        //}

        return $form;

        //}

    }

    public function SubmitTestForm($data,$form)
    {

        switch($data['Persons']){
            case 1:
                $electricity = 1500;
                $gas = 900;
                break;
            case 3:
                $electricity = 3500;
                $gas = 1500;
                break;
            case 4:
                $electricity = 4500;
                $gas = 1700;
                break;
            case 5:
                $electricity = 5000;
                $gas = 1800;
                break;
            default:
                $electricity = 2500;
                $gas = 1000;
                break;
        }

        $output = "?zip=".$data['Zip'];
        $output .= "&nr=".$data['HouseNumber'];
        $output .= "&electricity=".$electricity;
        if (isset($data['NoGas']) && $data['NoGas'] == true){
            $output .= "&hideGas=true";
        }else{
            $output .= "&gas=".$gas;
        }

        return $this->redirect($this->LinkToComparisonEnergyPage().$output);

    }

}
