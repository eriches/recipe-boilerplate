<?php

use SilverStripe\CMS\Model\SiteTree;
use Spatie\SchemaOrg\Organization;

class Page extends SiteTree
{
    private static $db = array(
    );

    private static $has_one = array(
    );

    public function getStructuredSchemaData() {
        $organization = new Organization();
        $organization
            ->name('Swift DevLabs');

        return $organization;
    }

}
